CS425 - Final Project Readme

IMPORTANT:
Before you can use this project, you may have to install Oracle.ManagedDataAccess using the NuGet Package Manager.

Instructions to installing Oracle.ManagedDataAccess:
1. Open the solution file in Visual Studio (*.sln)
2. Tools > NuGet Package Manager > Package Manager Console
3. Type in "Install-Package Oracle.ManagedDataAccess"
4. Click Start
5. Follow the on-screen instructions

--

Features:

- Registration and Login
- Password modification
- Rewards and ranking
- Updateable personal information
- Live query viewer (shows results of common queries such as finding the user with the most comments!)*
- Live query viewer (shows the SQL database tables in-app)*
- Reviews!
- Upvote/downvote reviews
- Comment on existing reviews
- Schedule management*
- Employee management*
- Buy tickets
- View tickets bought

* - requires special privileges