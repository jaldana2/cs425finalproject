﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheatreExplorer
{
    class Stringy
    {
        public static string NormalizeDaysOfTheWeek(string input)
        {
            bool sun = false;
            bool mon = false;
            bool tue = false;
            bool wed = false;
            bool thu = false;
            bool fri = false;
            bool sat = false;

            for (int i = 0; i <= input.Length - 1; i++)
            {
                string schar = input[i].ToString().ToUpper();
                switch (schar)
                {
                    case "U": sun = true; break;
                    case "M": mon = true; break;
                    case "T": tue = true; break;
                    case "W": wed = true; break;
                    case "R": thu = true; break;
                    case "F": fri = true; break;
                    case "S": sat = true; break;
                }
            }

            string r_days = "";
            if (sun) r_days = r_days + "U";
            if (mon) r_days = r_days + "M";
            if (tue) r_days = r_days + "T";
            if (wed) r_days = r_days + "W";
            if (thu) r_days = r_days + "R";
            if (fri) r_days = r_days + "F";
            if (sat) r_days = r_days + "S";
            return r_days;
        }
        public static string NormalizeHoursOfTheWeek(string input)
        {
            if (input.LastIndexOf("-") > 0)
            {
                string r_pre = input.Substring(0, input.LastIndexOf("-"));
                string r_post = input.Substring(input.LastIndexOf("-") + 1);

                // 8-14 => 08-14
                if (r_pre.Length == 1) r_pre = "0" + r_pre;
                if (r_post.Length == 1) r_post = "0" + r_post;
                return r_pre + "-" + r_post;
            }

            return "00-24"; // idk
        }
    }
}
