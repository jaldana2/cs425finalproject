﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using Oracle.ManagedDataAccess.Client;
using System.Data;

namespace TheatreExplorer
{
    class fDB
    {
        // Reuse these variables.
        private OracleConnection con;
        public static int SCHEDULE_NOT_EXIST = -1;


        public fDB(OracleConnection con)
        {
            this.con = con;
        }

        // ---------------------------------------------------------------------------------------------------
        // **
        // Employee
        // **
        // ---------------------------------------------------------------------------------------------------

        private string GetEmployeeStrAttrByID(string attr, int id)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "SELECT * FROM employees WHERE employee_id = :employee_id";
                cmd.Parameters.Add(new OracleParameter("employee_id", id));

                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    return dr[attr].ToString();
                }

                return "Unknown";
            }
        }

        public string GetEmployeeNameByID(int id)
        {
            return GetEmployeeStrAttrByID("name", id);
        }
        public string GetEmployeeAddressByID(int id)
        {
            return GetEmployeeStrAttrByID("address", id);
        }

        // add other GetEmployee...ByID as needed

        public ArrayList GetAllEmployeeIDs()
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                ArrayList id_list = new ArrayList();

                cmd.CommandText = "SELECT * FROM employees";

                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    id_list.Add(Convert.ToInt32(dr["employee_id"]));
                }

                return id_list;
            }
        }

        public EmployeeInfo GetEmployeeInfo(int user_id)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                EmployeeInfo employeeInfo = new EmployeeInfo();
                cmd.Connection = con;

                cmd.CommandText = "SELECT * FROM Employees WHERE user_id = :user_id";
                cmd.Parameters.Add(new OracleParameter("user_id", user_id));

                OracleDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    employeeInfo.employee_id = Convert.ToInt32(dr["employee_id"]);
                    employeeInfo.ssn = Convert.ToInt32(dr["ssn"]);

                    return employeeInfo;
                }

                return null;
            }
        }

        public TheatreInfo GetEmployeeWorkLocation(int employee_id)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                TheatreInfo theaterInfo = new TheatreInfo();
                cmd.Connection = con;

                cmd.CommandText = "SELECT * FROM Theatres WHERE theatre_id = (SELECT theatre_id FROM Employees WHERE employee_id = :employee_id)";
                cmd.Parameters.Add(new OracleParameter("employee_id", employee_id));

                OracleDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    theaterInfo.name = Convert.ToString(dr["name"]);
                    theaterInfo.theatre_id = Convert.ToInt32(dr["theatre_id"]);
                    theaterInfo.address = Convert.ToString(dr["address"]);

                    return theaterInfo;
                }

                return null;
            }
        }

        public void CheckSecurityStatus()
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "SELECT count(*) FROM Schedules WHERE job_type = 'security' AND day = :day";

                switch (DateTime.Now.DayOfWeek)
                {
                    case DayOfWeek.Sunday:
                        cmd.Parameters.Add(new OracleParameter("day", "u"));
                        break;
                    case DayOfWeek.Monday:
                        cmd.Parameters.Add(new OracleParameter("day", "m"));
                        break;
                    case DayOfWeek.Tuesday:
                        cmd.Parameters.Add(new OracleParameter("day", "t"));
                        break;
                    case DayOfWeek.Wednesday:
                        cmd.Parameters.Add(new OracleParameter("day", "w"));
                        break;
                    case DayOfWeek.Thursday:
                        cmd.Parameters.Add(new OracleParameter("day", "r"));
                        break;
                    case DayOfWeek.Friday:
                        cmd.Parameters.Add(new OracleParameter("day", "f"));
                        break;
                    case DayOfWeek.Saturday:
                        cmd.Parameters.Add(new OracleParameter("day", "s"));
                        break;
                }

                OracleDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    int count = Convert.ToInt32(dr["count(*)"]);
                    if (count == 0)
                    {
                        System.Windows.Forms.MessageBox.Show("OH NO! Today is " + DateTime.Now.DayOfWeek.ToString() + " and there are only " + count + " security officers on duty. You should fix that.");
                    }
                }
            }
        }

        // ---------------------------------------------------------------------------------------------------
        // **
        // Schedule
        // **
        // ---------------------------------------------------------------------------------------------------

        private string GetScheduleStrAttrByID(string attr, int id)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "SELECT * FROM schedules WHERE schedule_id = :schedule_id";
                cmd.Parameters.Add(new OracleParameter("schedule_id", id));

                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    return dr[attr].ToString();
                }

                return "Unknown";
            }
        }

        public string GetScheduleDaysByID(int id)
        {
            return GetScheduleStrAttrByID("days", id);
        }
        public string GetScheduleTimeByID(int id)
        {
            return GetScheduleStrAttrByID("time", id);
        }

        public ArrayList GetScheduleForID(int id)
        {
            ArrayList s_list = new ArrayList();

            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "SELECT * FROM schedules WHERE employee_id = :employee_id";
                cmd.Parameters.Add(new OracleParameter("employee_id", id));

                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    s_list.Add(Convert.ToInt32(dr["schedule_id"]));
                }

                return s_list;
            }
        }
        public void AddScheduleData(int employee_id, int theatre_id, int job_id, string days, string time)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "INSERT INTO schedules (schedule_id, employee_id, theatre_id, job_id, days, time) VALUES (:schedule_id, :employee_id, :theatre_id, :job_id, :days, :time)";
                cmd.Parameters.Add(new OracleParameter("schedule_id", GetLastScheduleID()));
                cmd.Parameters.Add(new OracleParameter("employee_id", employee_id));
                cmd.Parameters.Add(new OracleParameter("theatre_id", theatre_id));
                cmd.Parameters.Add(new OracleParameter("job_id", job_id));
                cmd.Parameters.Add(new OracleParameter("days", days));
                cmd.Parameters.Add(new OracleParameter("time", time));
                cmd.ExecuteNonQuery();
            }
        }
        public int GetLastScheduleID()
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "SELECT * FROM schedules";

                OracleDataReader dr = cmd.ExecuteReader();
                int num = 0;

                while (dr.Read())
                {
                    num = Convert.ToInt32(dr["schedule_id"]);
                }

                return num + 1;
            }
        }
        public void RemScheduleByID(int id)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "DELETE FROM schedules WHERE schedule_id = :schedule_id";
                cmd.Parameters.Add(new OracleParameter("schedule_id", id));
                cmd.ExecuteNonQuery();
            }
        }

        public ScheduleInfo GetEmployeeSchedule(int employee_id)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                ScheduleInfo schedule = new ScheduleInfo();

                List<string> vals = GetEmployeeScheduleByDay(employee_id, "m");
                schedule.times["m"] = vals[0];
                schedule.jobs["m"] = vals[1];
                schedule.theatres["m"] = GetTheatreNameFromId(Convert.ToInt32(vals[2]));
                vals = GetEmployeeScheduleByDay(employee_id, "t");
                schedule.times["t"] = vals[0];
                schedule.jobs["t"] = vals[1];
                schedule.theatres["t"] = GetTheatreNameFromId(Convert.ToInt32(vals[2]));
                vals = GetEmployeeScheduleByDay(employee_id, "w");
                schedule.times["w"] = vals[0];
                schedule.jobs["w"] = vals[1];
                schedule.theatres["w"] = GetTheatreNameFromId(Convert.ToInt32(vals[2]));
                vals = GetEmployeeScheduleByDay(employee_id, "r");
                schedule.times["r"] = vals[0];
                schedule.jobs["r"] = vals[1];
                schedule.theatres["r"] = GetTheatreNameFromId(Convert.ToInt32(vals[2]));
                vals = GetEmployeeScheduleByDay(employee_id, "f");
                schedule.times["f"] = vals[0];
                schedule.jobs["f"] = vals[1];
                schedule.theatres["f"] = GetTheatreNameFromId(Convert.ToInt32(vals[2]));
                vals = GetEmployeeScheduleByDay(employee_id, "s");
                schedule.times["s"] = vals[0];
                schedule.jobs["s"] = vals[1];
                schedule.theatres["s"] = GetTheatreNameFromId(Convert.ToInt32(vals[2]));
                vals = GetEmployeeScheduleByDay(employee_id, "u");
                schedule.times["u"] = vals[0];
                schedule.jobs["u"] = vals[1];
                schedule.theatres["u"] = GetTheatreNameFromId(Convert.ToInt32(vals[2]));

                return schedule;
            }
        }

        public List<string> GetEmployeeScheduleByDay(int employee_id, string day)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;
                cmd.CommandText = "SELECT time, job_type, theatre_id FROM schedules WHERE employee_id = :employee_id AND day = :day";
                cmd.Parameters.Add(new OracleParameter("employee_id", employee_id));
                cmd.Parameters.Add(new OracleParameter("day", day));

                OracleDataReader dr = cmd.ExecuteReader();

                List<string> val = new List<string>(3);
                if (dr.Read())
                {

                    val.Add(Convert.ToString(dr["time"]));
                    val.Add(Convert.ToString(dr["job_type"]));
                    val.Add(Convert.ToString(dr["theatre_id"]));

                    return val;
                }

                val.Add(null);
                val.Add(null);
                val.Add(null);
                return val;
            }
        }

        public bool CheckIfEmployeeCanDoJob(int employee_id, string job_type)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;
                cmd.CommandText = "SELECT * FROM employees WHERE employee_id = :employee_id AND job_type = :job_type";
                cmd.Parameters.Add(new OracleParameter("employee_id", employee_id));
                cmd.Parameters.Add(new OracleParameter("job_type", job_type));

                OracleDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    return true;
                }

                return false;
            }
        }

        public int GetNextScheduleID()
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "SELECT * FROM schedules ORDER BY schedule_id";

                OracleDataReader dr = cmd.ExecuteReader();
                int num = 0;

                while (dr.Read())
                {
                    num = Convert.ToInt32(dr["schedule_id"]);
                }


                return num + 1;
            }
        }

        public void InsertNewSchedule(int employee_id, int theatre_id, string job_type, string day, string time)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "INSERT INTO schedules (schedule_id, employee_id, theatre_id, job_type, day, time) VALUES (:schedule_id, :employee_id, :theatre_id, :job_type, :day, :time)";
                cmd.Parameters.Add(new OracleParameter("schedule_id", GetNextScheduleID()));
                cmd.Parameters.Add(new OracleParameter("employee_id", employee_id));
                cmd.Parameters.Add(new OracleParameter("theatre_id", theatre_id));
                cmd.Parameters.Add(new OracleParameter("job_type", job_type));
                cmd.Parameters.Add(new OracleParameter("day", day));
                cmd.Parameters.Add(new OracleParameter("time", time));
                cmd.ExecuteNonQuery();
            }
        }
        public void UpdateSchedule(int employee_id, int theatre_id, string job_type, string day, string time)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "UPDATE schedules SET theatre_id = :theatre_id, job_type = :job_type,  time = :time WHERE employee_id = :employee_id AND day = :day";
                cmd.Parameters.Add(new OracleParameter("theatre_id", theatre_id));
                cmd.Parameters.Add(new OracleParameter("job_type", job_type));
                cmd.Parameters.Add(new OracleParameter("time", time));
                cmd.Parameters.Add(new OracleParameter("employee_id", employee_id));
                cmd.Parameters.Add(new OracleParameter("day", day));

                cmd.ExecuteNonQuery();
            }
        }

        public List<string> GetEmployeeJobs(int employee_id)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;
                cmd.CommandText = "SELECT job_type FROM employees WHERE employee_id = :employee_id";
                cmd.Parameters.Add(new OracleParameter("employee_id", employee_id));

                OracleDataReader dr = cmd.ExecuteReader();

                List<string> val = new List<string>(3);
                while (dr.Read())
                {
                    val.Add(Convert.ToString(dr["job_type"]));
                }

                return val;
            }
        }

        // ---------------------------------------------------------------------------------------------------
        // **
        // Credits
        // **
        // ---------------------------------------------------------------------------------------------------

        public ArrayList GetDefinedTheatreIDsWithReviewCredits()
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                ArrayList id_list = new ArrayList();

                cmd.CommandText = "SELECT * FROM Review_Credits";

                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    id_list.Add(Convert.ToInt32(dr["theatre_id"]));
                }

                return id_list;
            }
        }
        public string GetCreditByID(int id)
        {
            string c_str = "";

            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.CommandText = "SELECT * FROM Review_Credits WHERE theatre_id = :theatre_id";
                cmd.Parameters.Add(new OracleParameter("theatre_id", id));

                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    c_str = dr["silver"] + "," + dr["gold"] + "," + dr["platinum"];
                }
            }
            return c_str;
        }
        public void UpdateReviewCreditByID(int id, int silver, int gold, int plat)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "UPDATE Review_Credits SET silver = :silver, gold = :gold, platinum = :platinum WHERE theatre_id = :theatre_id";
                cmd.Parameters.Add(new OracleParameter("silver", silver));
                cmd.Parameters.Add(new OracleParameter("gold", gold));
                cmd.Parameters.Add(new OracleParameter("platinum", plat));
                cmd.Parameters.Add(new OracleParameter("theatre_id", id));
                cmd.ExecuteNonQuery();
            }
        }
        public void InsertReviewCreditByID(int id, int silver, int gold, int plat)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "INSERT INTO Review_Credits (theatre_id, silver, gold, platinum) VALUES (:theatre_id, :silver, :gold, :platinum)";
                cmd.Parameters.Add(new OracleParameter("silver", silver));
                cmd.Parameters.Add(new OracleParameter("gold", gold));
                cmd.Parameters.Add(new OracleParameter("platinum", plat));
                cmd.Parameters.Add(new OracleParameter("theatre_id", id));
                cmd.ExecuteNonQuery();
            }
        }

        // ---------------------------------------------------------------------------------------------------
        // **
        // Registration
        // **
        // ---------------------------------------------------------------------------------------------------

        public int NewGuestRegistration(string username, string email, string password, string address, string phone_num, string name)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;
                int u_id = GetLastUserID();
                int accesslevel = 1;

                cmd.CommandText = "INSERT INTO Users (user_id, username, name, email, password, credits, rank, address, phone_number, access_level) " +
                    "VALUES (:user_id, :username, :name, :email, :password, :credits, :rank, :address, :phone_num, :access_level )";
                cmd.Parameters.Add(new OracleParameter("user_id", u_id));
                cmd.Parameters.Add(new OracleParameter("username", username));
                cmd.Parameters.Add(new OracleParameter("name", name));
                cmd.Parameters.Add(new OracleParameter("email", email));
                cmd.Parameters.Add(new OracleParameter("password", password));
                cmd.Parameters.Add(new OracleParameter("credits", 0.ToString()));
                cmd.Parameters.Add(new OracleParameter("rank", "silver"));
                cmd.Parameters.Add(new OracleParameter("address", address));
                cmd.Parameters.Add(new OracleParameter("phone_num", phone_num));
                cmd.Parameters.Add(new OracleParameter("access_level", accesslevel));

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    // some error
                    System.Windows.Forms.MessageBox.Show(e.Message);
                    return -1;
                }
                return u_id;
            }
        }
        public void NewCreditCardRegistration(int user_id, string cc_num, string cc_type, string cc_exp)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "INSERT INTO Credit_Card (user_id, cnumber, ctype, expiration_date) " +
                    "VALUES (:user_id, :cnumber, :ctype, :expiration_date)";
                cmd.Parameters.Add(new OracleParameter("user_id", user_id));
                cmd.Parameters.Add(new OracleParameter("cnumber", cc_num));
                cmd.Parameters.Add(new OracleParameter("ctype", cc_type));
                cmd.Parameters.Add(new OracleParameter("expiration_date", cc_exp));
                cmd.ExecuteNonQuery();
            }
        }

        public CreditCard GetUserCreditCard(int user_id)
        {
            CreditCard cc = new CreditCard();

            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;
                cmd.CommandText = "SELECT * FROM Credit_Card WHERE user_id = :user_id";
                cmd.Parameters.Add(new OracleParameter("user_id", user_id));

                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    cc.cc = dr["cnumber"].ToString();
                    cc.cc_type = dr["ctype"].ToString();
                    cc.cc_exp = dr["expiration_date"].ToString();
                }

                return cc;
            }
        }

        public int GetLastUserID()
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "SELECT * FROM users ORDER BY user_id";

                OracleDataReader dr = cmd.ExecuteReader();
                int num = 0;

                while (dr.Read())
                {
                    num = Convert.ToInt32(dr["user_id"]);
                }


                return num + 1;
            }
        }

        // ---------------------------------------------------------------------------------------------------
        // **
        // Login
        // **
        // ---------------------------------------------------------------------------------------------------

        public bool VerifyLogin(string username, string password)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "SELECT * FROM users WHERE username = :username AND password = :password";
                cmd.Parameters.Add(new OracleParameter("username", username));
                cmd.Parameters.Add(new OracleParameter("password", password));

                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public AccessLevel LoginGetUserAccessLevel(string username)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "SELECT * FROM users WHERE username = :username";
                cmd.Parameters.Add(new OracleParameter("username", username));

                OracleDataReader dr = cmd.ExecuteReader();

                int id = -1;
                while (dr.Read())
                {
                    id = Convert.ToInt32(dr["user_id"]);
                }
                if (id > -1)
                {
                    //cmd.CommandText = "SELECT * FROM Employees WHERE employee_id = :employee_id";
                    //cmd.Parameters.Add(new OracleParameter("employee_id", id));
                    //dr = cmd.ExecuteReader();

                    //while (dr.Read())
                    //{
                    //    return USER_LEVEL_EMPLOYEE;
                    //}
                }
                return AccessLevel.normal;
            }
        }
        public string GetUsernameFromId(int id)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "SELECT * FROM users WHERE user_id = :u_id";
                cmd.Parameters.Add(new OracleParameter("u_id", id));

                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    return dr["username"].ToString();
                }

                return "(INVALID ID)";
            }
        }


        // ---------------------------------------------------------------------------------------------------
        // **
        // User Info
        // **
        // ---------------------------------------------------------------------------------------------------
        public void ModUserPointsById(UserInfo ui, int points)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                ui.credits += points;
                if (ui.credits > 99 && ui.credits - 100 < 10 && points > 0)
                {
                    System.Windows.Forms.MessageBox.Show("You are now a gold member!");
                    updateUserRank(ui.user_id, "gold");
                }
                else if (ui.credits > 199 && ui.credits - 200 < 10 && points > 0)
                {
                    System.Windows.Forms.MessageBox.Show("You are now a platinum member!");
                    updateUserRank(ui.user_id, "platinum");
                }

                cmd.CommandText = "UPDATE Users SET credits = :c WHERE user_id = :u_id";
                cmd.Parameters.Add(new OracleParameter("credits", ui.credits));
                cmd.Parameters.Add(new OracleParameter("user_id", ui.user_id));
                cmd.ExecuteNonQuery();
            }
        }

        public UserInfo GetUserInfo(string username)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                UserInfo userInfo = new UserInfo();
                cmd.Connection = con;

                cmd.CommandText = "SELECT * FROM users WHERE username = :username";
                cmd.Parameters.Add(new OracleParameter("username", username));

                OracleDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    userInfo.username = username;
                    userInfo.name = Convert.ToString(dr["name"]);
                    userInfo.phoneNum = Convert.ToString(dr["phone_number"]);
                    userInfo.address = Convert.ToString(dr["address"]);
                    userInfo.email = Convert.ToString(dr["email"]);
                    userInfo.user_id = Convert.ToInt32(dr["user_id"]);
                    userInfo.credits = Convert.ToInt32(dr["credits"]);
                    userInfo.rank = Convert.ToString(dr["rank"]);
                    userInfo.isEmployee = false;
                    userInfo.accessLevel = (AccessLevel)Convert.ToInt32(dr["access_level"]);
                    userInfo.creditCard = GetUserCreditCard(userInfo.user_id);

                    if ((userInfo.employeeInfo = GetEmployeeInfo(userInfo.user_id)) != null)
                    {
                        userInfo.isEmployee = true;
                    }

                }
                return userInfo;
            }
        }

        public UserInfo GetUserInfoFromName(string name)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                UserInfo userInfo = new UserInfo();
                cmd.Connection = con;

                cmd.CommandText = "SELECT * FROM users WHERE name = :name";
                cmd.Parameters.Add(new OracleParameter("name", name));

                OracleDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    userInfo.username = Convert.ToString(dr["username"]);
                    userInfo.name = name;
                    userInfo.phoneNum = Convert.ToString(dr["phone_number"]);
                    userInfo.address = Convert.ToString(dr["address"]);
                    userInfo.email = Convert.ToString(dr["email"]);
                    userInfo.user_id = Convert.ToInt32(dr["user_id"]);
                    userInfo.credits = Convert.ToInt32(dr["credits"]);
                    userInfo.rank = Convert.ToString(dr["rank"]);
                    userInfo.isEmployee = false;
                    userInfo.accessLevel = (AccessLevel)Convert.ToInt32(dr["access_level"]);
                    userInfo.creditCard = GetUserCreditCard(userInfo.user_id);

                    if ((userInfo.employeeInfo = GetEmployeeInfo(userInfo.user_id)) != null)
                    {
                        userInfo.isEmployee = true;
                    }

                }
                return userInfo;
            }
        }

        public void updatePassword(string username, string newPass)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "UPDATE users SET password= :newPass WHERE username = :username";
                cmd.Parameters.Add(new OracleParameter("newPass", newPass));
                cmd.Parameters.Add(new OracleParameter("username", username));
                cmd.ExecuteNonQuery();

            }
        }

        public bool verifyPassword(string username, string password)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "SELECT * FROM users WHERE username = :username AND password = :password";
                cmd.Parameters.Add(new OracleParameter("username", username));
                cmd.Parameters.Add(new OracleParameter("password", password));

                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    return true;
                }
                return false;
            }
        }

        public void updateUserInfo(int user_id, string name, string phoneNum, string address)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "UPDATE users SET name = :name, phone_number = :phone_number, address = :address WHERE user_id = :user_id";
                cmd.Parameters.Add(new OracleParameter("name", name));
                cmd.Parameters.Add(new OracleParameter("phone_number", phoneNum));
                cmd.Parameters.Add(new OracleParameter("address", address));
                cmd.Parameters.Add(new OracleParameter("user_id", user_id));
                cmd.ExecuteNonQuery();

            }
        }

        public void updateUserRank(int user_id, string rank)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "UPDATE users SET rank = :rank WHERE user_id = :user_id";
                cmd.Parameters.Add(new OracleParameter("rank", rank));
                cmd.Parameters.Add(new OracleParameter("user_id", user_id));
                cmd.ExecuteNonQuery();

            }
        }

        // ---------------------------------------------------------------------------------------------------
        // **
        // Threads, Forums and more
        // **
        // ---------------------------------------------------------------------------------------------------

        // woops fn
        public List<MovieRoomPlaying> GetAllMoviesInTheatre(int theatre_id)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                List<MovieRoomPlaying> movies = new List<MovieRoomPlaying>();

                cmd.Connection = con;

                cmd.CommandText = "SELECT * FROM Currently_Playing WHERE room_id IN (SELECT room_id FROM Screening_Rooms WHERE theatre_id = :t_id)";
                cmd.Parameters.Add(new OracleParameter("t_id", theatre_id));

                OracleDataReader dr = cmd.ExecuteReader();

                // @@ You can probably do a better SQL syntax here but here's the basic code
                while (dr.Read())
                {
                    // room_id, movie_id, time
                    MovieRoomPlaying movie = new MovieRoomPlaying();
                    movie.room_id = Convert.ToInt32(dr["room_id"]);
                    movie.movie_id = Convert.ToInt32(dr["movie_id"]);
                    movie.time = dr["time"].ToString();
                    movie.name = GetMovieNameFromId(Convert.ToInt32(dr["movie_id"]));

                    movies.Add(movie);
                }

                return movies;
            }
        }

        public List<ThreadPost> GetThread(int id, ThreadType t_type)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                List<ThreadPost> thread = new List<ThreadPost>();

                cmd.Connection = con;

                if (t_type == ThreadType.movie)
                {
                    cmd.CommandText = "SELECT * FROM Movie_Posts WHERE thread_id = :t_id ORDER BY submit";
                }
                else if (t_type == ThreadType.theatre)
                {
                    cmd.CommandText = "SELECT * FROM Theatre_Posts WHERE thread_id = :thread_id ORDER BY submit";
                }
                cmd.Parameters.Add(new OracleParameter("t_id", id));

                OracleDataReader dr = cmd.ExecuteReader();

                // @@ You can probably do a better SQL syntax here but here's the basic code
                while (dr.Read())
                {
                    // post_id, content, submit, user_id, thread_id
                    ThreadPost tp = new ThreadPost();

                    tp.post_id = Convert.ToInt32(dr["post_id"]);
                    tp.user_id = Convert.ToInt32(dr["user_id"]);
                    tp.thread_id = Convert.ToInt32(dr["thread_id"]);
                    tp.content = dr["content"].ToString();
                    tp.date = dr["submit"].ToString();
                    thread.Add(tp);
                }

                return thread;
            }
        }
        public ThreadPost GetThreadPost(int id, ThreadType t_type)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                ThreadPost tp = new ThreadPost();

                cmd.Connection = con;

                if (t_type == ThreadType.movie)
                {
                    cmd.CommandText = "SELECT * FROM Movie_Posts WHERE post_id = :post_id";
                }
                else if (t_type == ThreadType.theatre)
                {
                    cmd.CommandText = "SELECT * FROM Theatre_Posts WHERE post_id = :post_id";
                }
                cmd.Parameters.Add(new OracleParameter("post_id", id));

                OracleDataReader dr = cmd.ExecuteReader();

                // @@ You can probably do a better SQL syntax here but here's the basic code
                while (dr.Read())
                {
                    // post_id, content, rating, submit, user_id, thread_id
                    tp.post_id = Convert.ToInt32(dr["post_id"]);
                    tp.user_id = Convert.ToInt32(dr["user_id"]);
                    tp.thread_id = Convert.ToInt32(dr["thread_id"]);
                    tp.content = dr["content"].ToString();
                    tp.date = dr["submit"].ToString();
                }

                return tp;
            }
        }

        // V2!
        public List<string> GetAllTheatreThreads()
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "SELECT * FROM Theatre_Threads";

                OracleDataReader dr = cmd.ExecuteReader();

                List<string> threads = new List<string>();

                while (dr.Read())
                {
                    // thread_id, theatre_id, thread_name
                    threads.Add(dr["thread_name"].ToString());
                }

                return threads;
            }
        }
        public List<string> GetAllMovieThreads()
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "SELECT * FROM Movie_Threads";

                OracleDataReader dr = cmd.ExecuteReader();

                List<string> threads = new List<string>();

                while (dr.Read())
                {
                    // thread_id, movie_id, thread_name
                    threads.Add(dr["thread_name"].ToString());
                }

                return threads;
            }
        }
        public string GetTheatreNameFromId(int id)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "SELECT * FROM Theatres WHERE theatre_id = :t_id";
                cmd.Parameters.Add(new OracleParameter("t_id", id));

                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    return dr["name"].ToString();
                }

                return "";
            }
        }
        public string GetMovieNameFromId(int id)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "SELECT * FROM Movies WHERE movie_id = :movie_id";
                cmd.Parameters.Add(new OracleParameter("movie_id", id));

                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    return dr["title"].ToString();
                }

                return "(INVALID ID)";
            }
        }
        public int GetMovieIdFromName(string name)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "SELECT * FROM Movies WHERE title = :name";
                cmd.Parameters.Add(new OracleParameter("name", name));

                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    return Convert.ToInt32(dr["movie_id"]);
                }

                return -1;
            }
        }
        public string GetTheatreNameFromThreadId(int id)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "SELECT * FROM Theatres WHERE theatre_id = (SELECT theatre_id FROM Theatre_Threads WHERE thread_id = :t_id)";
                cmd.Parameters.Add(new OracleParameter("t_id", id));

                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    return dr["name"].ToString();
                }

                return "(INVALID ID)";
            }
        }
        public string GetMovieNameFromThreadId(int id)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "SELECT * FROM Movies WHERE movie_id = (SELECT movie_id FROM Movie_Threads WHERE thread_id = :t_id)";
                cmd.Parameters.Add(new OracleParameter("t_id", id));

                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    return dr["title"].ToString();
                }

                return "(INVALID ID)";
            }
        }
        public int GetLastPostID(ThreadType t_type)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                if (t_type == ThreadType.movie)
                {
                    cmd.CommandText = "SELECT * FROM Movie_Posts ORDER BY post_id";
                }
                else if (t_type == ThreadType.theatre)
                {
                    cmd.CommandText = "SELECT * FROM Theatre_Posts ORDER BY post_id";
                }

                OracleDataReader dr = cmd.ExecuteReader();
                int num = -1;

                while (dr.Read())
                {
                    num = Convert.ToInt32(dr["post_id"]);
                }

                return num + 1;
            }
        }
        public int GetLastThreadID(ThreadType t_type)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                if (t_type == ThreadType.movie)
                {
                    cmd.CommandText = "SELECT * FROM Movie_Threads ORDER BY thread_id";
                }
                else if (t_type == ThreadType.theatre)
                {
                    cmd.CommandText = "SELECT * FROM Theatre_Threads ORDER BY thread_id";
                }

                OracleDataReader dr = cmd.ExecuteReader();
                int num = -1;

                while (dr.Read())
                {
                    num = Convert.ToInt32(dr["thread_id"]);
                }

                return num + 1;
            }
        }
        public void ReviewAddThread(UserInfo ui, int obj_id, int thread_id, string content, ThreadType t_type)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                // add post
                if (t_type == ThreadType.movie)
                {
                    cmd.CommandText = "INSERT INTO Movie_Threads (thread_id, movie_id, thread_name, hits) VALUES (:t_id, :m_id, :content, 0)";
                }
                else if (t_type == ThreadType.theatre)
                {
                    cmd.CommandText = "INSERT INTO Theatre_Threads (thread_id, theatre_id, thread_name, hits) VALUES (:t_id, :m_id, :content, 0)";
                }

                cmd.Parameters.Add(new OracleParameter("t_id", thread_id));
                cmd.Parameters.Add(new OracleParameter("m_id", obj_id));
                cmd.Parameters.Add(new OracleParameter("content", content));
                cmd.ExecuteNonQuery();
            }
        }
        public void ReviewAddReply(UserInfo ui, int thread_id, string content, ThreadType t_type)
        {
            int post_id = GetLastPostID(t_type);

            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                // add post
                if (t_type == ThreadType.movie)
                {
                    cmd.CommandText = "INSERT INTO Movie_Posts (post_id, content, submit, user_id, thread_id) VALUES (:p_id, :content, :n_d, :u_id, :t_id)";
                }
                else if (t_type == ThreadType.theatre)
                {
                    cmd.CommandText = "INSERT INTO Theatre_Posts (post_id, content, submit, user_id, thread_id) VALUES (:p_id, :content, :n_d, :u_id, :t_id)";
                }

                cmd.Parameters.Add(new OracleParameter("p_id", post_id));
                cmd.Parameters.Add(new OracleParameter("content", content));
                cmd.Parameters.Add(new OracleParameter("n_d", DateTime.Now));
                cmd.Parameters.Add(new OracleParameter("u_id", ui.user_id));
                cmd.Parameters.Add(new OracleParameter("t_id", thread_id));
                cmd.ExecuteNonQuery();

                // give em some points
                ModUserPointsById(ui, 10);
            }
        }
        public List<string> GetAllTheatres()
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                List<string> theatres = new List<string>();

                cmd.Connection = con;

                cmd.CommandText = "SELECT * FROM Theatres ORDER BY theatre_id";

                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    theatres.Add(dr["name"].ToString());
                }

                return theatres;
            }
        }
        public List<string> GetAllMovies()
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                List<string> movies = new List<string>();

                cmd.Connection = con;

                cmd.CommandText = "SELECT * FROM Movies";

                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    movies.Add(dr["title"].ToString());
                }

                return movies;
            }
        }
        public void incrementTheatreHits(int threadid)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.CommandText = "UPDATE Theatre_Threads SET hits = hits + 1 WHERE thread_id = :threadid";
                cmd.Parameters.Add(new OracleParameter("threadid", threadid));
                cmd.ExecuteNonQuery();
            }
        }
        public void incrementMovieHits(int threadid)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.CommandText = "UPDATE Movie_Threads SET hits = hits + 1 WHERE thread_id = :threadid";
                cmd.Parameters.Add(new OracleParameter("threadid", threadid));
                cmd.ExecuteNonQuery();
            }
        }

        public bool hasUserVotedOnThread(UserInfo ui, int thread_id, ThreadType t_type)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                if (t_type == ThreadType.movie)
                {
                    cmd.CommandText = "SELECT * FROM MovieThread_Ratings WHERE thread_id = :t_id AND user_id = :u_id";
                }
                else if (t_type == ThreadType.theatre)
                {
                    cmd.CommandText = "SELECT * FROM TheatreThread_Ratings WHERE thread_id = :t_id AND user_id = :u_id";
                }
                cmd.Parameters.Add(new OracleParameter("t_id", thread_id));
                cmd.Parameters.Add(new OracleParameter("u_id", ui.user_id));

                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    return true;
                }

                return false;
            }
        }
        public void UserVoteThread(UserInfo ui, int thread_id, ThreadType t_type, bool is_upvote)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                if (t_type == ThreadType.movie)
                {
                    cmd.CommandText = "INSERT INTO MovieThread_Ratings (thread_id, user_id, vote_type) VALUES (:t_id, :u_id, :v_t)";
                }
                else if (t_type == ThreadType.theatre)
                {
                    cmd.CommandText = "INSERT INTO TheatreThread_Ratings (thread_id, user_id, vote_type) VALUES (:t_id, :u_id, :v_t)";
                }
                cmd.Parameters.Add(new OracleParameter("t_id", thread_id));
                cmd.Parameters.Add(new OracleParameter("u_id", ui.user_id));
                if (is_upvote)
                {
                    cmd.Parameters.Add(new OracleParameter("v_t", 1));
                }
                else
                {
                    cmd.Parameters.Add(new OracleParameter("v_t", 0));
                }
                cmd.ExecuteNonQuery();
            }
        }
        public int getThreadVoteRating(int thread_id, ThreadType t_type)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                if (t_type == ThreadType.movie)
                {
                    cmd.CommandText = "SELECT * FROM MovieThread_Ratings WHERE thread_id = :t_id";
                }
                else if (t_type == ThreadType.theatre)
                {
                    cmd.CommandText = "SELECT * FROM TheatreThread_Ratings WHERE thread_id = :t_id";
                }
                cmd.Parameters.Add(new OracleParameter("t_id", thread_id));

                OracleDataReader dr = cmd.ExecuteReader();
                int score = 0;

                while (dr.Read())
                {
                    if (Convert.ToInt32(dr["vote_type"]) == 1)
                    {
                        score++;
                    }
                    else
                    {
                        score--;
                    }
                }

                return score;
            }
        }
        public int getThreadOwnerID(int thread_id, ThreadType t_type)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                if (t_type == ThreadType.movie)
                {
                    cmd.CommandText = "SELECT * FROM Movie_Posts WHERE thread_id = :t_id";
                }
                else if (t_type == ThreadType.theatre)
                {
                    cmd.CommandText = "SELECT * FROM Theatre_Posts WHERE thread_id = :t_id";
                }
                cmd.Parameters.Add(new OracleParameter("t_id", thread_id));

                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    return Convert.ToInt32(dr["user_id"]);
                }

                return -1;
            }
        }

        // ---------------------------------------------------------------------------------------------------
        // **
        // Queries
        // **
        // ---------------------------------------------------------------------------------------------------
        public List<ThreadPost> Query1(String thread)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                List<ThreadPost> posts = new List<ThreadPost>();
                cmd.Connection = con;
                if (thread.Equals("Theatres"))
                {
                    cmd.CommandText = "SELECT * FROM (SELECT * FROM Theatre_Posts ORDER BY submit DESC) WHERE rownum < 4";
                }
                else
                {
                    cmd.CommandText = "SELECT * FROM (SELECT * FROM Movie_Posts ORDER BY submit DESC) WHERE rownum < 4";
                }
                OracleDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ThreadPost tp = new ThreadPost();
                    tp.content = dr["content"].ToString();
                    tp.user_id = Convert.ToInt32(dr["user_id"]);
                    posts.Add(tp);
                }
                return posts;
            }
        }

        public List<ThreadPost> Query2()
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                List<ThreadPost> posts = new List<ThreadPost>();
                cmd.Connection = con;
                cmd.CommandText = "SELECT * FROM (SELECT * FROM (SELECT * FROM Theatre_Posts UNION SELECT * FROM Movie_Posts) ORDER BY submit DESC) WHERE rownum <4";
                OracleDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ThreadPost tp = new ThreadPost();
                    tp.content = dr["content"].ToString();
                    tp.user_id = Convert.ToInt32(dr["user_id"]);
                    tp.thread_id = Convert.ToInt32(dr["thread_id"]);
                    posts.Add(tp);
                }
                return posts;
            }
        }

        public String GetThreadTypeByThreadId(int thread_id)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                String threadname = null;
                cmd.Connection = con;
                cmd.CommandText = "SELECT * FROM Theatre_Threads WHERE thread_id = :thread_id";
                cmd.Parameters.Add(new OracleParameter("thread_id", thread_id));
                OracleDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    threadname = dr["thread_name"].ToString();
                }
                if (!String.IsNullOrEmpty(threadname))
                {
                    return "Theatres";
                }
                else
                {
                    return "Movies";
                }
            }
        }

        public int GetNumOfTheatreComments(int threadid)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                int numOfComments = -1;
                cmd.Connection = con;
                cmd.CommandText = "SELECT COUNT(*) FROM Theatre_Posts WHERE thread_id = :thread_id";
                cmd.Parameters.Add(new OracleParameter("thread_id", threadid));
                OracleDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    numOfComments = Convert.ToInt32(dr["count(*)"]);
                }
                return numOfComments;
            }
        }

        public int GetNumOfMovieComments(int threadid)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                int numOfComments = -1;
                cmd.Connection = con;
                cmd.CommandText = "SELECT COUNT(*) FROM Movie_Posts WHERE thread_id = :thread_id";
                cmd.Parameters.Add(new OracleParameter("thread_id", threadid));
                OracleDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    numOfComments = Convert.ToInt32(dr["count(*)"]);
                }
                return numOfComments;
            }
        }

        public List<Thread> GetAllTheatreThreadsHits()
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                List<Thread> threads = new List<Thread>();
                cmd.Connection = con;
                cmd.CommandText = "SELECT * FROM Theatre_Threads";
                OracleDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Thread newThread = new Thread();
                    newThread.thread_id = Convert.ToInt32(dr["thread_id"]);
                    newThread.threadname = dr["thread_name"].ToString();
                    newThread.hits = Convert.ToInt32(dr["hits"]);
                    newThread.comments = GetNumOfTheatreComments(newThread.thread_id);
                    threads.Add(newThread);
                }
                return threads;
            }
        }

        public List<Thread> GetAllMovieThreadsHits()
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                List<Thread> threads = new List<Thread>();
                cmd.Connection = con;
                cmd.CommandText = "SELECT * FROM Movie_Threads";
                OracleDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Thread newThread = new Thread();
                    newThread.thread_id = Convert.ToInt32(dr["thread_id"]);
                    newThread.threadname = dr["thread_name"].ToString();
                    newThread.hits = Convert.ToInt32(dr["hits"]);
                    newThread.comments = GetNumOfMovieComments(newThread.thread_id);
                    threads.Add(newThread);
                }
                return threads;
            }
        }

        public UserInfo GetUserIdMostComments()
        {
            UserInfo user = new UserInfo();
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;
                cmd.CommandText = "SELECT user_id,comments FROM (SELECT user_id, count(*) AS comments FROM (SELECT * FROM Theatre_Posts UNION SELECT * FROM Movie_Posts) GROUP BY user_id)  WHERE comments = (SELECT MAX(comments) FROM (SELECT user_id, count(*) AS comments FROM (SELECT * FROM Theatre_Posts UNION SELECT * FROM Movie_Posts) GROUP BY user_id))";
                OracleDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    user.user_id = Convert.ToInt32(dr["user_id"]);
                    user.comments = Convert.ToInt32(dr["comments"]);
                }
                return user;
            }
        }

        public List<TheatreInfo> Query6()
        {
            List<TheatreInfo> theatres = new List<TheatreInfo>();
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;
                cmd.CommandText = "SELECT theatre_id, tcount FROM (SELECT theatre_id, count(theatre_id) AS tcount FROM (SELECT Currently_Playing.room_id, theatre_id FROM Currently_Playing, Screening_Rooms WHERE Currently_Playing.room_id = Screening_Rooms.room_id) GROUP BY theatre_id) WHERE tcount = (SELECT MAX(tcount)  FROM (SELECT theatre_id, count(theatre_id) AS tcount FROM (SELECT Currently_Playing.room_id, theatre_id FROM Currently_Playing, Screening_Rooms WHERE Currently_Playing.room_id = Screening_Rooms.room_id) GROUP BY theatre_id))";
                OracleDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    TheatreInfo t = new TheatreInfo();
                    t.name = GetTheatreNameFromId(Convert.ToInt32(dr["theatre_id"]));
                    t.movies = Convert.ToInt32(dr["tcount"]);
                    theatres.Add(t);
                }
                return theatres;
            }
        }
        public List<TheatreInfo> Query7()
        {
            List<TheatreInfo> theatres = new List<TheatreInfo>();
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;
                cmd.CommandText = "SELECT theatre_id, tcount FROM (SELECT theatre_id, count(theatre_id) AS tcount FROM Ticket GROUP BY theatre_id) WHERE tcount = (SELECT MAX(tcount) FROM (SELECT theatre_id, count(theatre_id) AS tcount FROM Ticket GROUP BY theatre_id))";
                OracleDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    TheatreInfo t = new TheatreInfo();
                    t.name = GetTheatreNameFromId(Convert.ToInt32(dr["theatre_id"]));
                    t.ticketsales = Convert.ToInt32(dr["tcount"]);
                    theatres.Add(t);
                }
                return theatres;
            }
        }

        public string GetNameFromId(int id)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "SELECT name FROM users WHERE user_id = :u_id";
                cmd.Parameters.Add(new OracleParameter("u_id", id));

                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    return dr["name"].ToString();
                }

                return "(INVALID ID)";
            }
        }

        public String GetEmployeeNameById(int employee_id)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;
                cmd.CommandText = "SELECT user_id FROM Employees WHERE employee_id = :employee_id";
                cmd.Parameters.Add(new OracleParameter("employee_id", employee_id));
                OracleDataReader dr = cmd.ExecuteReader();
                int userid = -1;

                while (dr.Read())
                {
                    userid = Convert.ToInt32(dr["user_id"]);
                }
                return GetNameFromId(userid);
            }
        }

        public List<Schedule> Query8(int theatre_id)
        {
            List<Schedule> schedules = new List<Schedule>();
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;
                cmd.CommandText = "SELECT employee_id, theatre_id, job_type, time FROM Schedules WHERE theatre_id = :theatre_id AND day = 'm' ORDER BY employee_id";
                cmd.Parameters.Add(new OracleParameter("theatre_id", theatre_id));
                OracleDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Schedule s = new Schedule();
                    s.name = GetEmployeeNameById(Convert.ToInt32(dr["employee_id"]));
                    s.job = dr["job_type"].ToString();
                    s.time = dr["time"].ToString();
                    schedules.Add(s);
                }
                return schedules;
            }
        }
        // ---------------------------------------------------------------------------------------------------
        // **
        // Tickets
        // **
        // ---------------------------------------------------------------------------------------------------

        public void TicketPurchase(int theatre_id, int movie_id, int user_id, int room_id, string time)
        {
            int t_id = TicketNextID();

            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "INSERT INTO Ticket (ticket_id, movie_id, theatre_id, room_id, time) VALUES (:t_id, :m_id, :th_id, :r_id, :time)";

                cmd.Parameters.Add(new OracleParameter("t_id", t_id));
                cmd.Parameters.Add(new OracleParameter("m_id", movie_id));
                cmd.Parameters.Add(new OracleParameter("th_id", theatre_id));
                cmd.Parameters.Add(new OracleParameter("r_id", room_id));
                cmd.Parameters.Add(new OracleParameter("time", time));
                cmd.ExecuteNonQuery();

                OracleCommand cmd2 = con.CreateCommand();
                cmd2.CommandText = "INSERT INTO TicketPurchases (ticket_id, user_id) VALUES (:t_id, :u_id)";

                cmd2.Parameters.Add(new OracleParameter("t_id", t_id));
                cmd2.Parameters.Add(new OracleParameter("u_id", user_id));
                cmd2.ExecuteNonQuery();
            }
        }
        public int TicketCheckSeating(int theatre_id, int room_id, string time)
        {
            // get max

            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "SELECT * FROM Screening_Rooms WHERE theatre_id = :theatre_id AND room_id = :room_id";
                cmd.Parameters.Add(new OracleParameter("theatre_id", theatre_id));
                cmd.Parameters.Add(new OracleParameter("room_id", room_id));

                OracleDataReader dr = cmd.ExecuteReader();
                int num = 99;

                while (dr.Read())
                {
                    num = Convert.ToInt32(dr["capacity"]);
                }

                OracleCommand cmd2 = con.CreateCommand();

                cmd2.CommandText = "SELECT * FROM Ticket WHERE theatre_id = :theatre_id AND room_id = :room_id AND time = :time";
                cmd2.Parameters.Add(new OracleParameter("theatre_id", theatre_id));
                cmd2.Parameters.Add(new OracleParameter("room_id", room_id));
                cmd2.Parameters.Add(new OracleParameter("time", time));

                OracleDataReader dr2 = cmd2.ExecuteReader();
                while (dr2.Read())
                {
                    num = num - 1;
                }

                return num;
            }
        }
        public int TicketNextID()
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "SELECT * FROM Ticket ORDER BY ticket_id";

                OracleDataReader dr = cmd.ExecuteReader();
                int num = 0;

                while (dr.Read())
                {
                    num = Convert.ToInt32(dr["ticket_id"]) + 1;
                }

                return num;
            }
        }
        public List<Ticket> GetMyTickets(int user_id)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "SELECT * FROM Ticket WHERE ticket_id IN (SELECT ticket_id FROM TicketPurchases WHERE user_id = :u_id) ORDER BY ticket_id";
                cmd.Parameters.Add(new OracleParameter("u_id", user_id));

                OracleDataReader dr = cmd.ExecuteReader();
                List<Ticket> tickets = new List<Ticket>();

                while (dr.Read())
                {
                    Ticket ticket = new Ticket();

                    ticket.ticket_id = Convert.ToInt32(dr["ticket_id"]);
                    ticket.movie_id = Convert.ToInt32(dr["movie_id"]);
                    ticket.theatre_id = Convert.ToInt32(dr["theatre_id"]);
                    ticket.room_id = Convert.ToInt32(dr["room_id"]);
                    ticket.time = dr["time"].ToString();

                    tickets.Add(ticket);
                }

                return tickets;
            }
        }

        // ---------------------------------------------------------------------------------------------------
        // **
        // HW2 related checking functions
        // **
        // ---------------------------------------------------------------------------------------------------

        public int CheckIfScheduleExists(int theatre_id, int job_id, string days, string time)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "SELECT * FROM schedules WHERE theatre_id = :theatre_id AND job_id = :job_id " +
                    "AND days = :days AND time = :time";
                cmd.Parameters.Add(new OracleParameter("theatre_id", theatre_id));
                cmd.Parameters.Add(new OracleParameter("job_id", job_id));
                cmd.Parameters.Add(new OracleParameter("days", days));
                cmd.Parameters.Add(new OracleParameter("time", time));

                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    return Convert.ToInt16(dr["employee_id"]);
                }

                return SCHEDULE_NOT_EXIST;
            }
        }
        public bool CheckIfReviewCreditTheatreIDExists(int id)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.CommandText = "SELECT * FROM review_credits WHERE theatre_id = :theatre_id";
                cmd.Parameters.Add(new OracleParameter("theatre_id", id));

                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    return true;
                }
                return false;
            }
        }

        public int getNumTable()
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.CommandText = "SELECT table_name FROM user_tables";

                OracleDataReader dr = cmd.ExecuteReader();

                int count = 0;
                while (dr.Read())
                {
                    count++;
                }
                return count;
            }
        }

        public List<string> getAllTableNames()
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                List<string> tables = new List<string>();
                cmd.CommandText = "SELECT table_name FROM user_tables";

                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    tables.Add(Convert.ToString(dr["table_name"]));
                }
                return tables;
            }
        }

        public List<string> getTableColumnNames(string tableName)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                List<string> columns = new List<string>();
                cmd.CommandText = "SELECT column_name FROM USER_TAB_COLUMNS WHERE table_name = :tableName";
                cmd.Parameters.Add(new OracleParameter("tableName", tableName));

                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    columns.Add(Convert.ToString(dr["column_name"]));
                }
                return columns;
            }
        }

        public List<string[]> getAllRowsFromTable(string table, List<string> columnNames)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                List<string[]> rows = new List<string[]>();
                cmd.CommandText = "SELECT * FROM " + table;

                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    string[] values = new string[columnNames.Count];
                    int count = 0;
                    foreach (string i in columnNames)
                    {
                        values[count] = Convert.ToString(dr[i]);
                        count++;
                    }
                    rows.Add(values);
                }
                return rows;
            }
        }

        public void updateValue(string tableName, string columnName, string value, string keyName, string key)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "UPDATE " + tableName + " SET " + columnName + " = :value WHERE " + keyName + " = :key";
                cmd.Parameters.Add(new OracleParameter("value", value));
                cmd.Parameters.Add(new OracleParameter("key", key));

                OracleDataReader dr = cmd.ExecuteReader();
            }
        }

        public List<string> getAllEmployeeNames()
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                List<string> employees = new List<string>();
                cmd.CommandText = "SELECT name FROM users WHERE user_id IN (SELECT user_id FROM employees) ORDER BY user_id";
                OracleDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    employees.Add(Convert.ToString(dr["name"]));
                }
                return employees;
            }

        }

        public List<UserInfo> getAllEmployees()
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                List<UserInfo> employees = new List<UserInfo>();
                cmd.CommandText = "SELECT * FROM users WHERE user_id = (SELECT user_id FROM employees) ORDER BY user_id";

                OracleDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    UserInfo userInfo = new UserInfo();
                    userInfo.username = Convert.ToString(dr["username"]);
                    userInfo.name = Convert.ToString(dr["name"]);
                    userInfo.phoneNum = Convert.ToString(dr["phone_number"]);
                    userInfo.address = Convert.ToString(dr["address"]);
                    userInfo.email = Convert.ToString(dr["email"]);
                    userInfo.user_id = Convert.ToInt32(dr["user_id"]);
                    userInfo.credits = Convert.ToInt32(dr["credits"]);
                    userInfo.rank = Convert.ToString(dr["rank"]);
                    userInfo.isEmployee = false;
                    userInfo.accessLevel = (AccessLevel)Convert.ToInt32(dr["access_level"]);
                    userInfo.creditCard = GetUserCreditCard(userInfo.user_id);

                    if ((userInfo.employeeInfo = GetEmployeeInfo(userInfo.user_id)) != null)
                    {
                        userInfo.isEmployee = true;
                    }

                    employees.Add(userInfo);
                }
                return employees;
            }
        }
        public TheatreInfo getManagerTheatre(int employee_id)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                TheatreInfo theatre = new TheatreInfo();
                cmd.CommandText = "SELECT * FROM theatres WHERE theatre_id = (SELECT theatre_id FROM manages WHERE employee_id = :employee_id)";
                cmd.Parameters.Add(new OracleParameter("employee_id", employee_id));

                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    if ((theatre.address = Convert.ToString(dr["address"])) == null)
                        return null;
                    theatre.theatre_id = Convert.ToInt32(dr["theatre_id"]);
                    theatre.name = Convert.ToString(dr["name"]);
                }
                return theatre;
            }
        }
        public TheatreInfo getTheatre(int theatre_id)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                TheatreInfo theatre = new TheatreInfo();
                cmd.CommandText = "SELECT * FROM Theatres WHERE theatre_id = :theatre_id)";
                cmd.Parameters.Add(new OracleParameter("theatre_id", theatre_id));

                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    if ((theatre.address = Convert.ToString(dr["address"])) == null)
                        return null;
                    theatre.theatre_id = Convert.ToInt32(dr["theatre_id"]);
                    theatre.name = Convert.ToString(dr["name"]);
                }
                return theatre;
            }
        }

        public void UpdateShowTime(string title, int room_id, string time)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                cmd.CommandText = "UPDATE currently_playing SET room_id = :room_id, time = :time WHERE movie_id = (SELECT movie_id FROM movies WHERE title = :title)";
                cmd.Parameters.Add(new OracleParameter("room_id", room_id));
                cmd.Parameters.Add(new OracleParameter("time", time));
                cmd.Parameters.Add(new OracleParameter("title", title));
                cmd.ExecuteNonQuery();
            }
        }

        public void InsertShowTime(string title, int room_id, string time)
        {
            using (OracleCommand cmd = con.CreateCommand())
            {
                cmd.Connection = con;

                int movie = GetMovieIdFromName(title);

                cmd.CommandText = "INSERT INTO currently_playing  (room_id, movie_id, time) VALUES (:room_id, :title, :time)";
                cmd.Parameters.Add(new OracleParameter("room_id", room_id));
                cmd.Parameters.Add(new OracleParameter("title", movie));
                cmd.Parameters.Add(new OracleParameter("time", time));
                cmd.ExecuteNonQuery();
            }
        }

    }
}
