﻿namespace TheatreExplorer
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.mainBottomStrip = new System.Windows.Forms.StatusStrip();
            this.textAppName = new System.Windows.Forms.ToolStripStatusLabel();
            this.textAppVersionStatusBar = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.forceReconnectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.progressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.textProgressLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.timerDBConnector = new System.Windows.Forms.Timer(this.components);
            this.tabAllTables = new System.Windows.Forms.TabPage();
            this.containerAllTables = new System.Windows.Forms.FlowLayoutPanel();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabMainMyAccount = new System.Windows.Forms.TabPage();
            this.groupBoxEmployeeInfo = new System.Windows.Forms.GroupBox();
            this.labelSSN = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.buttonConfirmUpdateInfo = new System.Windows.Forms.Button();
            this.textBoxChangeAddress = new System.Windows.Forms.TextBox();
            this.textBoxChangePhoneNum = new System.Windows.Forms.TextBox();
            this.textBoxChangeName = new System.Windows.Forms.TextBox();
            this.labelName = new System.Windows.Forms.Label();
            this.labelPhone = new System.Windows.Forms.Label();
            this.labelAddress = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.labelEmail = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.buttonUpdateInfo = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.btnAccountRefreshPoints = new System.Windows.Forms.Button();
            this.labelRank = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.labelPoints = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.updatePassword = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.textNewPassword = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textOldPassword = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tabMainOptions = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.listDebugLog = new System.Windows.Forms.ListBox();
            this.checkSettingsDebugLog = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSaveDBCredentials = new System.Windows.Forms.Button();
            this.checkSettingsAutoConnect = new System.Windows.Forms.CheckBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textSettingsDBPassword = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textSettingsDBUsername = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textSettingsReconnectionDelay = new System.Windows.Forms.TextBox();
            this.checkSettingsAutoReconnect = new System.Windows.Forms.CheckBox();
            this.tabMainHome = new System.Windows.Forms.TabPage();
            this.panelHomeRegister = new System.Windows.Forms.Panel();
            this.label43 = new System.Windows.Forms.Label();
            this.comboRegisterCCType = new System.Windows.Forms.ComboBox();
            this.textBoxRegisterExpDate = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.textBoxRegisterCreditCardNum = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxRegisterFullName = new System.Windows.Forms.TextBox();
            this.textBoxRegisterPhoneNumber = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.textBoxRegisterAddress = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.textBoxRegisterEmail = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.btnRegisterLogin = new System.Windows.Forms.Button();
            this.btnRegisterCancel = new System.Windows.Forms.Button();
            this.btnRegisterRegister = new System.Windows.Forms.Button();
            this.textBoxRegisterPassword = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxRegisterUsername = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.panelHomeLogin = new System.Windows.Forms.Panel();
            this.btnLoginRegister = new System.Windows.Forms.Button();
            this.btnLoginCancel = new System.Windows.Forms.Button();
            this.btnLoginLogin = new System.Windows.Forms.Button();
            this.textLoginPassword = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textLoginUsername = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panelHomeDefault = new System.Windows.Forms.Panel();
            this.label47 = new System.Windows.Forms.Label();
            this.btnHomeLogin = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.btnHomeRegister = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panelHomeLoggedIn = new System.Windows.Forms.Panel();
            this.buttonLogOut = new System.Windows.Forms.Button();
            this.labelLoggedIn = new System.Windows.Forms.Label();
            this.tabMainControl = new System.Windows.Forms.TabControl();
            this.tabReviews = new System.Windows.Forms.TabPage();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.labelReviewOwner = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.labelReviewAlreadyVoted = new System.Windows.Forms.Label();
            this.btnReviewDownvote = new System.Windows.Forms.Button();
            this.btnReviewUpvote = new System.Windows.Forms.Button();
            this.labelReviewRating = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.btnThreadReply = new System.Windows.Forms.Button();
            this.textReviewReplyBox = new System.Windows.Forms.TextBox();
            this.listReviewData = new System.Windows.Forms.ListBox();
            this.groupReviewAddBox = new System.Windows.Forms.GroupBox();
            this.comboReviewTarget = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.btnReviewCreateThread = new System.Windows.Forms.Button();
            this.textReviewAddMessage = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textReviewAddTitle = new System.Windows.Forms.TextBox();
            this.radioReviewMovie = new System.Windows.Forms.RadioButton();
            this.radioReviewTheatre = new System.Windows.Forms.RadioButton();
            this.labelReviewDiscussion = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.tabReviewControl = new System.Windows.Forms.TabControl();
            this.panelReviewTheatres = new System.Windows.Forms.TabPage();
            this.btnReviewsRefreshTheatres = new System.Windows.Forms.Button();
            this.listReviewTheatres = new System.Windows.Forms.ListBox();
            this.panelReviewMovies = new System.Windows.Forms.TabPage();
            this.btnReviewsRefreshMovies = new System.Windows.Forms.Button();
            this.listReviewMovies = new System.Windows.Forms.ListBox();
            this.tabQueries = new System.Windows.Forms.TabPage();
            this.comboBoxQuery8 = new System.Windows.Forms.ComboBox();
            this.buttonQuery8 = new System.Windows.Forms.Button();
            this.listBoxQuery8 = new System.Windows.Forms.ListBox();
            this.labelQuery8 = new System.Windows.Forms.Label();
            this.buttonQuery7 = new System.Windows.Forms.Button();
            this.listBoxQuery7 = new System.Windows.Forms.ListBox();
            this.labelQuery7 = new System.Windows.Forms.Label();
            this.buttonQuery6 = new System.Windows.Forms.Button();
            this.listBoxQuery6 = new System.Windows.Forms.ListBox();
            this.labelQuery6 = new System.Windows.Forms.Label();
            this.buttonQuery5 = new System.Windows.Forms.Button();
            this.listBoxQuery5 = new System.Windows.Forms.ListBox();
            this.labelQuery5 = new System.Windows.Forms.Label();
            this.buttonQuery3 = new System.Windows.Forms.Button();
            this.listBoxQuery3 = new System.Windows.Forms.ListBox();
            this.label22 = new System.Windows.Forms.Label();
            this.buttonQuery2 = new System.Windows.Forms.Button();
            this.listBoxQuery2 = new System.Windows.Forms.ListBox();
            this.labelQuery2 = new System.Windows.Forms.Label();
            this.buttonQuery1 = new System.Windows.Forms.Button();
            this.listBoxQuery1 = new System.Windows.Forms.ListBox();
            this.comboBoxQuery1 = new System.Windows.Forms.ComboBox();
            this.Query1 = new System.Windows.Forms.Label();
            this.tabSchedule = new System.Windows.Forms.TabPage();
            this.tabTickets = new System.Windows.Forms.TabPage();
            this.listTicketTimeIDHidden = new System.Windows.Forms.ListBox();
            this.panelTicketBillingInfo = new System.Windows.Forms.Panel();
            this.labelTicketSeats = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.textTicketCCType = new System.Windows.Forms.ComboBox();
            this.label42 = new System.Windows.Forms.Label();
            this.btnTicketBuy = new System.Windows.Forms.Button();
            this.label40 = new System.Windows.Forms.Label();
            this.labelTicketMustRegister = new System.Windows.Forms.Label();
            this.textTicketUsername = new System.Windows.Forms.TextBox();
            this.textTicketCCExp = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.textTicketPassword = new System.Windows.Forms.TextBox();
            this.textTicketCC = new System.Windows.Forms.TextBox();
            this.btnTicketRegisterAndBuy = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.textTicketEmail = new System.Windows.Forms.TextBox();
            this.textTicketFullName = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.textTicketPhone = new System.Windows.Forms.TextBox();
            this.textTicketAddress = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.listTicketMovieIDHidden = new System.Windows.Forms.ListBox();
            this.btnTicketReset = new System.Windows.Forms.Button();
            this.groupTicketTime = new System.Windows.Forms.GroupBox();
            this.listTicketTime = new System.Windows.Forms.ListBox();
            this.groupTicketMovie = new System.Windows.Forms.GroupBox();
            this.listTicketMovie = new System.Windows.Forms.ListBox();
            this.label23 = new System.Windows.Forms.Label();
            this.groupTicketTheatre = new System.Windows.Forms.GroupBox();
            this.listTicketTheatre = new System.Windows.Forms.ListBox();
            this.panelTicketFogOfWar = new System.Windows.Forms.Panel();
            this.btnTicketStart = new System.Windows.Forms.Button();
            this.tabMyTickets = new System.Windows.Forms.TabPage();
            this.listMyTickets = new System.Windows.Forms.ListBox();
            this.label46 = new System.Windows.Forms.Label();
            this.btnMyTicketsRefresh = new System.Windows.Forms.Button();
            this.tabEmployeeManagement = new System.Windows.Forms.TabPage();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.label54 = new System.Windows.Forms.Label();
            this.comboJobSelect = new System.Windows.Forms.ComboBox();
            this.buttonSubmitSchedule = new System.Windows.Forms.Button();
            this.panelCurrentSchedule = new System.Windows.Forms.Panel();
            this.labelScheduleJob = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.labelScheduleTheatre = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.labelScheduleTime = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.comboSetEndCycle = new System.Windows.Forms.ComboBox();
            this.label61 = new System.Windows.Forms.Label();
            this.comboSetEndMinute = new System.Windows.Forms.ComboBox();
            this.comboSetEndHour = new System.Windows.Forms.ComboBox();
            this.label60 = new System.Windows.Forms.Label();
            this.comboSetStartCycle = new System.Windows.Forms.ComboBox();
            this.label59 = new System.Windows.Forms.Label();
            this.comboSetStartMinute = new System.Windows.Forms.ComboBox();
            this.label58 = new System.Windows.Forms.Label();
            this.comboSetStartHour = new System.Windows.Forms.ComboBox();
            this.label57 = new System.Windows.Forms.Label();
            this.comboTheatreSelect = new System.Windows.Forms.ComboBox();
            this.label56 = new System.Windows.Forms.Label();
            this.comboDaySelect = new System.Windows.Forms.ComboBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.labelEmployeeSSN = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.labelEmployeePhone = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.labelEmployeeAddress = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.labelEmployeeName = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.comboEmployeeSelect = new System.Windows.Forms.ComboBox();
            this.label48 = new System.Windows.Forms.Label();
            this.tabTheatreManagement = new System.Windows.Forms.TabPage();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.dataShowTimes = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.comboMovieEndCycle = new System.Windows.Forms.ComboBox();
            this.label74 = new System.Windows.Forms.Label();
            this.comboMovieEndMinute = new System.Windows.Forms.ComboBox();
            this.comboMovieEndHour = new System.Windows.Forms.ComboBox();
            this.label75 = new System.Windows.Forms.Label();
            this.comboMovieStartCycle = new System.Windows.Forms.ComboBox();
            this.label76 = new System.Windows.Forms.Label();
            this.comboMovieStartMinute = new System.Windows.Forms.ComboBox();
            this.label77 = new System.Windows.Forms.Label();
            this.comboMovieStartHour = new System.Windows.Forms.ComboBox();
            this.label78 = new System.Windows.Forms.Label();
            this.comboSelectRoom = new System.Windows.Forms.ComboBox();
            this.label79 = new System.Windows.Forms.Label();
            this.comboSelectMovie = new System.Windows.Forms.ComboBox();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.labelTheatreAddress = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.labelTheatreName = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.comboSelectTheatreForUpdate = new System.Windows.Forms.ComboBox();
            this.label88 = new System.Windows.Forms.Label();
            this.mainBottomStrip.SuspendLayout();
            this.tabAllTables.SuspendLayout();
            this.tabMainMyAccount.SuspendLayout();
            this.groupBoxEmployeeInfo.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.tabMainOptions.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabMainHome.SuspendLayout();
            this.panelHomeRegister.SuspendLayout();
            this.panelHomeLogin.SuspendLayout();
            this.panelHomeDefault.SuspendLayout();
            this.panelHomeLoggedIn.SuspendLayout();
            this.tabMainControl.SuspendLayout();
            this.tabReviews.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupReviewAddBox.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.tabReviewControl.SuspendLayout();
            this.panelReviewTheatres.SuspendLayout();
            this.panelReviewMovies.SuspendLayout();
            this.tabQueries.SuspendLayout();
            this.tabTickets.SuspendLayout();
            this.panelTicketBillingInfo.SuspendLayout();
            this.groupTicketTime.SuspendLayout();
            this.groupTicketMovie.SuspendLayout();
            this.groupTicketTheatre.SuspendLayout();
            this.panelTicketFogOfWar.SuspendLayout();
            this.tabMyTickets.SuspendLayout();
            this.tabEmployeeManagement.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.panelCurrentSchedule.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.tabTheatreManagement.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataShowTimes)).BeginInit();
            this.groupBox16.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainBottomStrip
            // 
            this.mainBottomStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.textAppName,
            this.textAppVersionStatusBar,
            this.toolStripDropDownButton1,
            this.progressBar,
            this.textProgressLabel});
            this.mainBottomStrip.Location = new System.Drawing.Point(0, 510);
            this.mainBottomStrip.Name = "mainBottomStrip";
            this.mainBottomStrip.Size = new System.Drawing.Size(932, 22);
            this.mainBottomStrip.TabIndex = 1;
            this.mainBottomStrip.Text = "statusStrip1";
            // 
            // textAppName
            // 
            this.textAppName.Name = "textAppName";
            this.textAppName.Size = new System.Drawing.Size(68, 17);
            this.textAppName.Text = "(app name)";
            // 
            // textAppVersionStatusBar
            // 
            this.textAppVersionStatusBar.Name = "textAppVersionStatusBar";
            this.textAppVersionStatusBar.Size = new System.Drawing.Size(53, 17);
            this.textAppVersionStatusBar.Text = "(version)";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.forceReconnectionToolStripMenuItem});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(60, 20);
            this.toolStripDropDownButton1.Text = "Actions";
            // 
            // forceReconnectionToolStripMenuItem
            // 
            this.forceReconnectionToolStripMenuItem.Name = "forceReconnectionToolStripMenuItem";
            this.forceReconnectionToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.forceReconnectionToolStripMenuItem.Text = "&Force Reconnection";
            this.forceReconnectionToolStripMenuItem.Click += new System.EventHandler(this.forceReconnectionToolStripMenuItem_Click);
            // 
            // progressBar
            // 
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(100, 16);
            // 
            // textProgressLabel
            // 
            this.textProgressLabel.Name = "textProgressLabel";
            this.textProgressLabel.Size = new System.Drawing.Size(26, 17);
            this.textProgressLabel.Text = "idle";
            // 
            // timerDBConnector
            // 
            this.timerDBConnector.Tick += new System.EventHandler(this.timerDBConnector_Tick);
            // 
            // tabAllTables
            // 
            this.tabAllTables.Controls.Add(this.containerAllTables);
            this.tabAllTables.Location = new System.Drawing.Point(4, 22);
            this.tabAllTables.Name = "tabAllTables";
            this.tabAllTables.Padding = new System.Windows.Forms.Padding(3);
            this.tabAllTables.Size = new System.Drawing.Size(924, 506);
            this.tabAllTables.TabIndex = 5;
            this.tabAllTables.Text = "Database";
            this.tabAllTables.UseVisualStyleBackColor = true;
            // 
            // containerAllTables
            // 
            this.containerAllTables.AutoScroll = true;
            this.containerAllTables.Dock = System.Windows.Forms.DockStyle.Fill;
            this.containerAllTables.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.containerAllTables.Location = new System.Drawing.Point(3, 3);
            this.containerAllTables.Name = "containerAllTables";
            this.containerAllTables.Size = new System.Drawing.Size(918, 500);
            this.containerAllTables.TabIndex = 0;
            this.containerAllTables.WrapContents = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(924, 506);
            this.tabPage2.TabIndex = 3;
            this.tabPage2.Text = "Locations";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabMainMyAccount
            // 
            this.tabMainMyAccount.Controls.Add(this.groupBoxEmployeeInfo);
            this.tabMainMyAccount.Controls.Add(this.groupBox6);
            this.tabMainMyAccount.Controls.Add(this.groupBox9);
            this.tabMainMyAccount.Controls.Add(this.groupBox7);
            this.tabMainMyAccount.Controls.Add(this.label9);
            this.tabMainMyAccount.Location = new System.Drawing.Point(4, 22);
            this.tabMainMyAccount.Name = "tabMainMyAccount";
            this.tabMainMyAccount.Padding = new System.Windows.Forms.Padding(3);
            this.tabMainMyAccount.Size = new System.Drawing.Size(924, 506);
            this.tabMainMyAccount.TabIndex = 2;
            this.tabMainMyAccount.Text = "My Account";
            this.tabMainMyAccount.UseVisualStyleBackColor = true;
            // 
            // groupBoxEmployeeInfo
            // 
            this.groupBoxEmployeeInfo.Controls.Add(this.labelSSN);
            this.groupBoxEmployeeInfo.Controls.Add(this.label39);
            this.groupBoxEmployeeInfo.Location = new System.Drawing.Point(579, 249);
            this.groupBoxEmployeeInfo.Name = "groupBoxEmployeeInfo";
            this.groupBoxEmployeeInfo.Size = new System.Drawing.Size(297, 161);
            this.groupBoxEmployeeInfo.TabIndex = 6;
            this.groupBoxEmployeeInfo.TabStop = false;
            this.groupBoxEmployeeInfo.Text = "Employee Information";
            // 
            // labelSSN
            // 
            this.labelSSN.AutoSize = true;
            this.labelSSN.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSSN.Location = new System.Drawing.Point(101, 28);
            this.labelSSN.Name = "labelSSN";
            this.labelSSN.Size = new System.Drawing.Size(37, 22);
            this.labelSSN.TabIndex = 11;
            this.labelSSN.Text = "100";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(8, 34);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(87, 13);
            this.label39.TabIndex = 2;
            this.label39.Text = "Social Security #";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.buttonConfirmUpdateInfo);
            this.groupBox6.Controls.Add(this.textBoxChangeAddress);
            this.groupBox6.Controls.Add(this.textBoxChangePhoneNum);
            this.groupBox6.Controls.Add(this.textBoxChangeName);
            this.groupBox6.Controls.Add(this.labelName);
            this.groupBox6.Controls.Add(this.labelPhone);
            this.groupBox6.Controls.Add(this.labelAddress);
            this.groupBox6.Controls.Add(this.label41);
            this.groupBox6.Controls.Add(this.labelEmail);
            this.groupBox6.Controls.Add(this.label35);
            this.groupBox6.Controls.Add(this.buttonUpdateInfo);
            this.groupBox6.Controls.Add(this.label33);
            this.groupBox6.Controls.Add(this.label34);
            this.groupBox6.Location = new System.Drawing.Point(26, 249);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(547, 161);
            this.groupBox6.TabIndex = 6;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Personal Information";
            // 
            // buttonConfirmUpdateInfo
            // 
            this.buttonConfirmUpdateInfo.Location = new System.Drawing.Point(9, 132);
            this.buttonConfirmUpdateInfo.Name = "buttonConfirmUpdateInfo";
            this.buttonConfirmUpdateInfo.Size = new System.Drawing.Size(188, 23);
            this.buttonConfirmUpdateInfo.TabIndex = 14;
            this.buttonConfirmUpdateInfo.Text = "Confirm Update";
            this.buttonConfirmUpdateInfo.UseVisualStyleBackColor = true;
            this.buttonConfirmUpdateInfo.Click += new System.EventHandler(this.buttonConfirmUpdateInfo_Click);
            // 
            // textBoxChangeAddress
            // 
            this.textBoxChangeAddress.Location = new System.Drawing.Point(99, 105);
            this.textBoxChangeAddress.Name = "textBoxChangeAddress";
            this.textBoxChangeAddress.Size = new System.Drawing.Size(120, 20);
            this.textBoxChangeAddress.TabIndex = 12;
            // 
            // textBoxChangePhoneNum
            // 
            this.textBoxChangePhoneNum.Location = new System.Drawing.Point(99, 68);
            this.textBoxChangePhoneNum.Name = "textBoxChangePhoneNum";
            this.textBoxChangePhoneNum.Size = new System.Drawing.Size(120, 20);
            this.textBoxChangePhoneNum.TabIndex = 11;
            // 
            // textBoxChangeName
            // 
            this.textBoxChangeName.Location = new System.Drawing.Point(99, 33);
            this.textBoxChangeName.Name = "textBoxChangeName";
            this.textBoxChangeName.Size = new System.Drawing.Size(120, 20);
            this.textBoxChangeName.TabIndex = 10;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelName.Location = new System.Drawing.Point(95, 28);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(37, 22);
            this.labelName.TabIndex = 9;
            this.labelName.Text = "100";
            // 
            // labelPhone
            // 
            this.labelPhone.AutoSize = true;
            this.labelPhone.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPhone.Location = new System.Drawing.Point(95, 68);
            this.labelPhone.Name = "labelPhone";
            this.labelPhone.Size = new System.Drawing.Size(37, 22);
            this.labelPhone.TabIndex = 8;
            this.labelPhone.Text = "100";
            // 
            // labelAddress
            // 
            this.labelAddress.AutoSize = true;
            this.labelAddress.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAddress.Location = new System.Drawing.Point(95, 106);
            this.labelAddress.Name = "labelAddress";
            this.labelAddress.Size = new System.Drawing.Size(37, 22);
            this.labelAddress.TabIndex = 7;
            this.labelAddress.Text = "100";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(9, 34);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(35, 13);
            this.label41.TabIndex = 0;
            this.label41.Text = "Name";
            // 
            // labelEmail
            // 
            this.labelEmail.AutoSize = true;
            this.labelEmail.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEmail.Location = new System.Drawing.Point(297, 28);
            this.labelEmail.Name = "labelEmail";
            this.labelEmail.Size = new System.Drawing.Size(37, 22);
            this.labelEmail.TabIndex = 6;
            this.labelEmail.Text = "100";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(9, 74);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(78, 13);
            this.label35.TabIndex = 6;
            this.label35.Text = "Phone Number";
            // 
            // buttonUpdateInfo
            // 
            this.buttonUpdateInfo.Location = new System.Drawing.Point(9, 132);
            this.buttonUpdateInfo.Name = "buttonUpdateInfo";
            this.buttonUpdateInfo.Size = new System.Drawing.Size(188, 23);
            this.buttonUpdateInfo.TabIndex = 5;
            this.buttonUpdateInfo.Text = "Update Information";
            this.buttonUpdateInfo.UseVisualStyleBackColor = true;
            this.buttonUpdateInfo.Click += new System.EventHandler(this.buttonUpdateInfo_Click);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(9, 112);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(45, 13);
            this.label33.TabIndex = 4;
            this.label33.Text = "Address";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(265, 34);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(32, 13);
            this.label34.TabIndex = 2;
            this.label34.Text = "Email";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.btnAccountRefreshPoints);
            this.groupBox9.Controls.Add(this.labelRank);
            this.groupBox9.Controls.Add(this.label15);
            this.groupBox9.Controls.Add(this.labelPoints);
            this.groupBox9.Controls.Add(this.label12);
            this.groupBox9.Location = new System.Drawing.Point(329, 82);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(547, 161);
            this.groupBox9.TabIndex = 4;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "My Rewards";
            // 
            // btnAccountRefreshPoints
            // 
            this.btnAccountRefreshPoints.Location = new System.Drawing.Point(454, 19);
            this.btnAccountRefreshPoints.Name = "btnAccountRefreshPoints";
            this.btnAccountRefreshPoints.Size = new System.Drawing.Size(75, 23);
            this.btnAccountRefreshPoints.TabIndex = 6;
            this.btnAccountRefreshPoints.Text = "Refresh";
            this.btnAccountRefreshPoints.UseVisualStyleBackColor = true;
            this.btnAccountRefreshPoints.Click += new System.EventHandler(this.btnAccountRefreshPoints_Click);
            // 
            // labelRank
            // 
            this.labelRank.AutoSize = true;
            this.labelRank.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRank.Location = new System.Drawing.Point(123, 89);
            this.labelRank.Name = "labelRank";
            this.labelRank.Size = new System.Drawing.Size(64, 29);
            this.labelRank.TabIndex = 3;
            this.labelRank.Text = "Gold";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(44, 97);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(73, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Reward Level";
            // 
            // labelPoints
            // 
            this.labelPoints.AutoSize = true;
            this.labelPoints.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPoints.Location = new System.Drawing.Point(123, 53);
            this.labelPoints.Name = "labelPoints";
            this.labelPoints.Size = new System.Drawing.Size(55, 29);
            this.labelPoints.TabIndex = 1;
            this.labelPoints.Text = "100";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(78, 65);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(39, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Credits";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.updatePassword);
            this.groupBox7.Controls.Add(this.label11);
            this.groupBox7.Controls.Add(this.textNewPassword);
            this.groupBox7.Controls.Add(this.label10);
            this.groupBox7.Controls.Add(this.textOldPassword);
            this.groupBox7.Location = new System.Drawing.Point(26, 82);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(297, 161);
            this.groupBox7.TabIndex = 2;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Password";
            // 
            // updatePassword
            // 
            this.updatePassword.Location = new System.Drawing.Point(54, 115);
            this.updatePassword.Name = "updatePassword";
            this.updatePassword.Size = new System.Drawing.Size(188, 23);
            this.updatePassword.TabIndex = 5;
            this.updatePassword.Text = "Update Password";
            this.updatePassword.UseVisualStyleBackColor = true;
            this.updatePassword.Click += new System.EventHandler(this.updatePassword_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(54, 73);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(78, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "New Password";
            // 
            // textNewPassword
            // 
            this.textNewPassword.Location = new System.Drawing.Point(54, 89);
            this.textNewPassword.Name = "textNewPassword";
            this.textNewPassword.PasswordChar = '*';
            this.textNewPassword.Size = new System.Drawing.Size(188, 20);
            this.textNewPassword.TabIndex = 3;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(54, 34);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "Current Password";
            // 
            // textOldPassword
            // 
            this.textOldPassword.Location = new System.Drawing.Point(54, 50);
            this.textOldPassword.Name = "textOldPassword";
            this.textOldPassword.PasswordChar = '*';
            this.textOldPassword.Size = new System.Drawing.Size(188, 20);
            this.textOldPassword.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Trebuchet MS", 32F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(17, 14);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(251, 54);
            this.label9.TabIndex = 1;
            this.label9.Text = "My Account";
            // 
            // tabMainOptions
            // 
            this.tabMainOptions.Controls.Add(this.groupBox4);
            this.tabMainOptions.Controls.Add(this.groupBox2);
            this.tabMainOptions.Controls.Add(this.groupBox1);
            this.tabMainOptions.Location = new System.Drawing.Point(4, 22);
            this.tabMainOptions.Name = "tabMainOptions";
            this.tabMainOptions.Padding = new System.Windows.Forms.Padding(3);
            this.tabMainOptions.Size = new System.Drawing.Size(924, 506);
            this.tabMainOptions.TabIndex = 1;
            this.tabMainOptions.Text = "Options";
            this.tabMainOptions.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Location = new System.Drawing.Point(17, 319);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(342, 153);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Connection Information";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.listDebugLog);
            this.groupBox2.Controls.Add(this.checkSettingsDebugLog);
            this.groupBox2.Location = new System.Drawing.Point(365, 15);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(535, 457);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Debug Log";
            // 
            // listDebugLog
            // 
            this.listDebugLog.FormattingEnabled = true;
            this.listDebugLog.Location = new System.Drawing.Point(6, 42);
            this.listDebugLog.Name = "listDebugLog";
            this.listDebugLog.Size = new System.Drawing.Size(523, 407);
            this.listDebugLog.TabIndex = 1;
            // 
            // checkSettingsDebugLog
            // 
            this.checkSettingsDebugLog.AutoSize = true;
            this.checkSettingsDebugLog.Location = new System.Drawing.Point(6, 19);
            this.checkSettingsDebugLog.Name = "checkSettingsDebugLog";
            this.checkSettingsDebugLog.Size = new System.Drawing.Size(109, 17);
            this.checkSettingsDebugLog.TabIndex = 0;
            this.checkSettingsDebugLog.Text = "Enable debug log";
            this.checkSettingsDebugLog.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnSaveDBCredentials);
            this.groupBox1.Controls.Add(this.checkSettingsAutoConnect);
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.checkSettingsAutoReconnect);
            this.groupBox1.Location = new System.Drawing.Point(17, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(342, 265);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Connection Options";
            // 
            // btnSaveDBCredentials
            // 
            this.btnSaveDBCredentials.Location = new System.Drawing.Point(97, 203);
            this.btnSaveDBCredentials.Name = "btnSaveDBCredentials";
            this.btnSaveDBCredentials.Size = new System.Drawing.Size(174, 23);
            this.btnSaveDBCredentials.TabIndex = 7;
            this.btnSaveDBCredentials.Text = "Save Settings";
            this.btnSaveDBCredentials.UseVisualStyleBackColor = true;
            this.btnSaveDBCredentials.Click += new System.EventHandler(this.btnSaveDBCredentials_Click);
            // 
            // checkSettingsAutoConnect
            // 
            this.checkSettingsAutoConnect.AutoSize = true;
            this.checkSettingsAutoConnect.Location = new System.Drawing.Point(12, 27);
            this.checkSettingsAutoConnect.Name = "checkSettingsAutoConnect";
            this.checkSettingsAutoConnect.Size = new System.Drawing.Size(149, 17);
            this.checkSettingsAutoConnect.TabIndex = 3;
            this.checkSettingsAutoConnect.Text = "Auto Connect on Startup?";
            this.checkSettingsAutoConnect.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.textSettingsDBPassword);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.textSettingsDBUsername);
            this.groupBox5.Location = new System.Drawing.Point(6, 97);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(330, 100);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Database Settings";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(18, 48);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Password";
            // 
            // textSettingsDBPassword
            // 
            this.textSettingsDBPassword.Location = new System.Drawing.Point(115, 45);
            this.textSettingsDBPassword.Name = "textSettingsDBPassword";
            this.textSettingsDBPassword.Size = new System.Drawing.Size(209, 20);
            this.textSettingsDBPassword.TabIndex = 4;
            this.textSettingsDBPassword.Text = "password";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Username";
            // 
            // textSettingsDBUsername
            // 
            this.textSettingsDBUsername.Location = new System.Drawing.Point(115, 19);
            this.textSettingsDBUsername.Name = "textSettingsDBUsername";
            this.textSettingsDBUsername.Size = new System.Drawing.Size(209, 20);
            this.textSettingsDBUsername.TabIndex = 2;
            this.textSettingsDBUsername.Text = "username";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.textSettingsReconnectionDelay);
            this.groupBox3.Location = new System.Drawing.Point(211, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(120, 48);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Reconnection delay";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(66, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "seconds";
            // 
            // textSettingsReconnectionDelay
            // 
            this.textSettingsReconnectionDelay.Location = new System.Drawing.Point(16, 19);
            this.textSettingsReconnectionDelay.Name = "textSettingsReconnectionDelay";
            this.textSettingsReconnectionDelay.Size = new System.Drawing.Size(44, 20);
            this.textSettingsReconnectionDelay.TabIndex = 0;
            this.textSettingsReconnectionDelay.Text = "5";
            // 
            // checkSettingsAutoReconnect
            // 
            this.checkSettingsAutoReconnect.AutoSize = true;
            this.checkSettingsAutoReconnect.Location = new System.Drawing.Point(12, 50);
            this.checkSettingsAutoReconnect.Name = "checkSettingsAutoReconnect";
            this.checkSettingsAutoReconnect.Size = new System.Drawing.Size(171, 17);
            this.checkSettingsAutoReconnect.TabIndex = 0;
            this.checkSettingsAutoReconnect.Text = "Reconnect on Network Failure";
            this.checkSettingsAutoReconnect.UseVisualStyleBackColor = true;
            // 
            // tabMainHome
            // 
            this.tabMainHome.Controls.Add(this.panelHomeRegister);
            this.tabMainHome.Controls.Add(this.panelHomeLogin);
            this.tabMainHome.Controls.Add(this.panelHomeDefault);
            this.tabMainHome.Controls.Add(this.label1);
            this.tabMainHome.Controls.Add(this.panelHomeLoggedIn);
            this.tabMainHome.Location = new System.Drawing.Point(4, 22);
            this.tabMainHome.Name = "tabMainHome";
            this.tabMainHome.Padding = new System.Windows.Forms.Padding(3);
            this.tabMainHome.Size = new System.Drawing.Size(924, 506);
            this.tabMainHome.TabIndex = 0;
            this.tabMainHome.Text = "Home";
            this.tabMainHome.UseVisualStyleBackColor = true;
            // 
            // panelHomeRegister
            // 
            this.panelHomeRegister.BackColor = System.Drawing.Color.Transparent;
            this.panelHomeRegister.Controls.Add(this.label43);
            this.panelHomeRegister.Controls.Add(this.comboRegisterCCType);
            this.panelHomeRegister.Controls.Add(this.textBoxRegisterExpDate);
            this.panelHomeRegister.Controls.Add(this.label21);
            this.panelHomeRegister.Controls.Add(this.textBoxRegisterCreditCardNum);
            this.panelHomeRegister.Controls.Add(this.label20);
            this.panelHomeRegister.Controls.Add(this.label13);
            this.panelHomeRegister.Controls.Add(this.textBoxRegisterFullName);
            this.panelHomeRegister.Controls.Add(this.textBoxRegisterPhoneNumber);
            this.panelHomeRegister.Controls.Add(this.label32);
            this.panelHomeRegister.Controls.Add(this.textBoxRegisterAddress);
            this.panelHomeRegister.Controls.Add(this.label31);
            this.panelHomeRegister.Controls.Add(this.textBoxRegisterEmail);
            this.panelHomeRegister.Controls.Add(this.label30);
            this.panelHomeRegister.Controls.Add(this.btnRegisterLogin);
            this.panelHomeRegister.Controls.Add(this.btnRegisterCancel);
            this.panelHomeRegister.Controls.Add(this.btnRegisterRegister);
            this.panelHomeRegister.Controls.Add(this.textBoxRegisterPassword);
            this.panelHomeRegister.Controls.Add(this.label3);
            this.panelHomeRegister.Controls.Add(this.textBoxRegisterUsername);
            this.panelHomeRegister.Controls.Add(this.label28);
            this.panelHomeRegister.Controls.Add(this.label29);
            this.panelHomeRegister.Location = new System.Drawing.Point(606, 75);
            this.panelHomeRegister.Name = "panelHomeRegister";
            this.panelHomeRegister.Size = new System.Drawing.Size(283, 394);
            this.panelHomeRegister.TabIndex = 12;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(12, 254);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(31, 13);
            this.label43.TabIndex = 25;
            this.label43.Text = "Type";
            // 
            // comboRegisterCCType
            // 
            this.comboRegisterCCType.FormattingEnabled = true;
            this.comboRegisterCCType.Items.AddRange(new object[] {
            "visa",
            "master",
            "amex",
            "other"});
            this.comboRegisterCCType.Location = new System.Drawing.Point(15, 269);
            this.comboRegisterCCType.Name = "comboRegisterCCType";
            this.comboRegisterCCType.Size = new System.Drawing.Size(43, 21);
            this.comboRegisterCCType.TabIndex = 24;
            // 
            // textBoxRegisterExpDate
            // 
            this.textBoxRegisterExpDate.Location = new System.Drawing.Point(191, 270);
            this.textBoxRegisterExpDate.Name = "textBoxRegisterExpDate";
            this.textBoxRegisterExpDate.Size = new System.Drawing.Size(72, 20);
            this.textBoxRegisterExpDate.TabIndex = 23;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(188, 254);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(54, 13);
            this.label21.TabIndex = 22;
            this.label21.Text = "Exp. Date";
            // 
            // textBoxRegisterCreditCardNum
            // 
            this.textBoxRegisterCreditCardNum.Location = new System.Drawing.Point(63, 270);
            this.textBoxRegisterCreditCardNum.Name = "textBoxRegisterCreditCardNum";
            this.textBoxRegisterCreditCardNum.Size = new System.Drawing.Size(122, 20);
            this.textBoxRegisterCreditCardNum.TabIndex = 21;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(60, 254);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(99, 13);
            this.label20.TabIndex = 20;
            this.label20.Text = "Credit Card Number";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(14, 215);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(54, 13);
            this.label13.TabIndex = 19;
            this.label13.Text = "Full Name";
            // 
            // textBoxRegisterFullName
            // 
            this.textBoxRegisterFullName.Location = new System.Drawing.Point(15, 231);
            this.textBoxRegisterFullName.Name = "textBoxRegisterFullName";
            this.textBoxRegisterFullName.Size = new System.Drawing.Size(248, 20);
            this.textBoxRegisterFullName.TabIndex = 18;
            // 
            // textBoxRegisterPhoneNumber
            // 
            this.textBoxRegisterPhoneNumber.Location = new System.Drawing.Point(15, 192);
            this.textBoxRegisterPhoneNumber.Name = "textBoxRegisterPhoneNumber";
            this.textBoxRegisterPhoneNumber.Size = new System.Drawing.Size(248, 20);
            this.textBoxRegisterPhoneNumber.TabIndex = 17;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(12, 176);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(78, 13);
            this.label32.TabIndex = 16;
            this.label32.Text = "Phone Number";
            // 
            // textBoxRegisterAddress
            // 
            this.textBoxRegisterAddress.Location = new System.Drawing.Point(15, 153);
            this.textBoxRegisterAddress.Name = "textBoxRegisterAddress";
            this.textBoxRegisterAddress.Size = new System.Drawing.Size(248, 20);
            this.textBoxRegisterAddress.TabIndex = 15;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(12, 137);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(45, 13);
            this.label31.TabIndex = 14;
            this.label31.Text = "Address";
            // 
            // textBoxRegisterEmail
            // 
            this.textBoxRegisterEmail.Location = new System.Drawing.Point(15, 114);
            this.textBoxRegisterEmail.Name = "textBoxRegisterEmail";
            this.textBoxRegisterEmail.Size = new System.Drawing.Size(248, 20);
            this.textBoxRegisterEmail.TabIndex = 13;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(12, 98);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(32, 13);
            this.label30.TabIndex = 12;
            this.label30.Text = "Email";
            // 
            // btnRegisterLogin
            // 
            this.btnRegisterLogin.Location = new System.Drawing.Point(15, 325);
            this.btnRegisterLogin.Name = "btnRegisterLogin";
            this.btnRegisterLogin.Size = new System.Drawing.Size(248, 23);
            this.btnRegisterLogin.TabIndex = 11;
            this.btnRegisterLogin.Text = "Already have an account?";
            this.btnRegisterLogin.UseVisualStyleBackColor = true;
            this.btnRegisterLogin.Click += new System.EventHandler(this.btnRegisterLogin_Click);
            // 
            // btnRegisterCancel
            // 
            this.btnRegisterCancel.Location = new System.Drawing.Point(15, 354);
            this.btnRegisterCancel.Name = "btnRegisterCancel";
            this.btnRegisterCancel.Size = new System.Drawing.Size(248, 23);
            this.btnRegisterCancel.TabIndex = 10;
            this.btnRegisterCancel.Text = "Cancel";
            this.btnRegisterCancel.UseVisualStyleBackColor = true;
            this.btnRegisterCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnRegisterRegister
            // 
            this.btnRegisterRegister.Location = new System.Drawing.Point(15, 296);
            this.btnRegisterRegister.Name = "btnRegisterRegister";
            this.btnRegisterRegister.Size = new System.Drawing.Size(248, 23);
            this.btnRegisterRegister.TabIndex = 9;
            this.btnRegisterRegister.Text = "Register";
            this.btnRegisterRegister.UseVisualStyleBackColor = true;
            this.btnRegisterRegister.Click += new System.EventHandler(this.btnRegisterRegister_Click);
            // 
            // textBoxRegisterPassword
            // 
            this.textBoxRegisterPassword.Location = new System.Drawing.Point(15, 77);
            this.textBoxRegisterPassword.Name = "textBoxRegisterPassword";
            this.textBoxRegisterPassword.PasswordChar = '*';
            this.textBoxRegisterPassword.Size = new System.Drawing.Size(248, 20);
            this.textBoxRegisterPassword.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Password";
            // 
            // textBoxRegisterUsername
            // 
            this.textBoxRegisterUsername.Location = new System.Drawing.Point(15, 39);
            this.textBoxRegisterUsername.Name = "textBoxRegisterUsername";
            this.textBoxRegisterUsername.Size = new System.Drawing.Size(248, 20);
            this.textBoxRegisterUsername.TabIndex = 6;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(12, 23);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(55, 13);
            this.label28.TabIndex = 5;
            this.label28.Text = "Username";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(12, 10);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(46, 13);
            this.label29.TabIndex = 0;
            this.label29.Text = "Register";
            // 
            // panelHomeLogin
            // 
            this.panelHomeLogin.BackColor = System.Drawing.Color.Transparent;
            this.panelHomeLogin.Controls.Add(this.btnLoginRegister);
            this.panelHomeLogin.Controls.Add(this.btnLoginCancel);
            this.panelHomeLogin.Controls.Add(this.btnLoginLogin);
            this.panelHomeLogin.Controls.Add(this.textLoginPassword);
            this.panelHomeLogin.Controls.Add(this.label19);
            this.panelHomeLogin.Controls.Add(this.textLoginUsername);
            this.panelHomeLogin.Controls.Add(this.label18);
            this.panelHomeLogin.Controls.Add(this.label4);
            this.panelHomeLogin.Location = new System.Drawing.Point(317, 75);
            this.panelHomeLogin.Name = "panelHomeLogin";
            this.panelHomeLogin.Size = new System.Drawing.Size(283, 394);
            this.panelHomeLogin.TabIndex = 5;
            // 
            // btnLoginRegister
            // 
            this.btnLoginRegister.Location = new System.Drawing.Point(15, 325);
            this.btnLoginRegister.Name = "btnLoginRegister";
            this.btnLoginRegister.Size = new System.Drawing.Size(248, 23);
            this.btnLoginRegister.TabIndex = 11;
            this.btnLoginRegister.Text = "No account? Click to Register";
            this.btnLoginRegister.UseVisualStyleBackColor = true;
            this.btnLoginRegister.Click += new System.EventHandler(this.btnLoginRegister_Click);
            // 
            // btnLoginCancel
            // 
            this.btnLoginCancel.Location = new System.Drawing.Point(15, 354);
            this.btnLoginCancel.Name = "btnLoginCancel";
            this.btnLoginCancel.Size = new System.Drawing.Size(248, 23);
            this.btnLoginCancel.TabIndex = 10;
            this.btnLoginCancel.Text = "Cancel";
            this.btnLoginCancel.UseVisualStyleBackColor = true;
            this.btnLoginCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnLoginLogin
            // 
            this.btnLoginLogin.Location = new System.Drawing.Point(15, 131);
            this.btnLoginLogin.Name = "btnLoginLogin";
            this.btnLoginLogin.Size = new System.Drawing.Size(248, 23);
            this.btnLoginLogin.TabIndex = 9;
            this.btnLoginLogin.Text = "Log in";
            this.btnLoginLogin.UseVisualStyleBackColor = true;
            this.btnLoginLogin.Click += new System.EventHandler(this.btnLoginLogin_Click);
            // 
            // textLoginPassword
            // 
            this.textLoginPassword.Location = new System.Drawing.Point(15, 99);
            this.textLoginPassword.Name = "textLoginPassword";
            this.textLoginPassword.PasswordChar = '*';
            this.textLoginPassword.Size = new System.Drawing.Size(248, 20);
            this.textLoginPassword.TabIndex = 8;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(12, 80);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(53, 13);
            this.label19.TabIndex = 7;
            this.label19.Text = "Password";
            // 
            // textLoginUsername
            // 
            this.textLoginUsername.Location = new System.Drawing.Point(15, 57);
            this.textLoginUsername.Name = "textLoginUsername";
            this.textLoginUsername.Size = new System.Drawing.Size(248, 20);
            this.textLoginUsername.TabIndex = 6;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(12, 41);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(55, 13);
            this.label18.TabIndex = 5;
            this.label18.Text = "Username";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Log In";
            // 
            // panelHomeDefault
            // 
            this.panelHomeDefault.BackColor = System.Drawing.Color.Transparent;
            this.panelHomeDefault.Controls.Add(this.label47);
            this.panelHomeDefault.Controls.Add(this.btnHomeLogin);
            this.panelHomeDefault.Controls.Add(this.label6);
            this.panelHomeDefault.Controls.Add(this.btnHomeRegister);
            this.panelHomeDefault.Controls.Add(this.label2);
            this.panelHomeDefault.Location = new System.Drawing.Point(28, 75);
            this.panelHomeDefault.Name = "panelHomeDefault";
            this.panelHomeDefault.Size = new System.Drawing.Size(283, 394);
            this.panelHomeDefault.TabIndex = 1;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(58, 42);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(162, 13);
            this.label47.TabIndex = 5;
            this.label47.Text = "Feel free to continue as guest or:";
            // 
            // btnHomeLogin
            // 
            this.btnHomeLogin.Location = new System.Drawing.Point(34, 131);
            this.btnHomeLogin.Name = "btnHomeLogin";
            this.btnHomeLogin.Size = new System.Drawing.Size(217, 23);
            this.btnHomeLogin.TabIndex = 4;
            this.btnHomeLogin.Text = "Log in";
            this.btnHomeLogin.UseVisualStyleBackColor = true;
            this.btnHomeLogin.Click += new System.EventHandler(this.btnHomeLogin_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 105);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(132, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Already have an account?";
            // 
            // btnHomeRegister
            // 
            this.btnHomeRegister.Location = new System.Drawing.Point(34, 65);
            this.btnHomeRegister.Name = "btnHomeRegister";
            this.btnHomeRegister.Size = new System.Drawing.Size(217, 23);
            this.btnHomeRegister.TabIndex = 2;
            this.btnHomeRegister.Text = "Register...";
            this.btnHomeRegister.UseVisualStyleBackColor = true;
            this.btnHomeRegister.Click += new System.EventHandler(this.btnHomeRegister_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "New User?";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Trebuchet MS", 32F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(206, 54);
            this.label1.TabIndex = 0;
            this.label1.Text = "Welcome";
            // 
            // panelHomeLoggedIn
            // 
            this.panelHomeLoggedIn.BackColor = System.Drawing.Color.White;
            this.panelHomeLoggedIn.Controls.Add(this.buttonLogOut);
            this.panelHomeLoggedIn.Controls.Add(this.labelLoggedIn);
            this.panelHomeLoggedIn.Location = new System.Drawing.Point(28, 75);
            this.panelHomeLoggedIn.Name = "panelHomeLoggedIn";
            this.panelHomeLoggedIn.Size = new System.Drawing.Size(283, 394);
            this.panelHomeLoggedIn.TabIndex = 8;
            // 
            // buttonLogOut
            // 
            this.buttonLogOut.Location = new System.Drawing.Point(34, 36);
            this.buttonLogOut.Name = "buttonLogOut";
            this.buttonLogOut.Size = new System.Drawing.Size(217, 23);
            this.buttonLogOut.TabIndex = 1;
            this.buttonLogOut.Text = "Log Out";
            this.buttonLogOut.UseVisualStyleBackColor = true;
            this.buttonLogOut.Click += new System.EventHandler(this.buttonLogOut_Click);
            // 
            // labelLoggedIn
            // 
            this.labelLoggedIn.AutoSize = true;
            this.labelLoggedIn.Location = new System.Drawing.Point(12, 10);
            this.labelLoggedIn.Name = "labelLoggedIn";
            this.labelLoggedIn.Size = new System.Drawing.Size(60, 13);
            this.labelLoggedIn.TabIndex = 0;
            this.labelLoggedIn.Text = "New User?";
            // 
            // tabMainControl
            // 
            this.tabMainControl.Controls.Add(this.tabMainHome);
            this.tabMainControl.Controls.Add(this.tabMainOptions);
            this.tabMainControl.Controls.Add(this.tabMainMyAccount);
            this.tabMainControl.Controls.Add(this.tabPage2);
            this.tabMainControl.Controls.Add(this.tabAllTables);
            this.tabMainControl.Controls.Add(this.tabReviews);
            this.tabMainControl.Controls.Add(this.tabQueries);
            this.tabMainControl.Controls.Add(this.tabSchedule);
            this.tabMainControl.Controls.Add(this.tabTickets);
            this.tabMainControl.Controls.Add(this.tabMyTickets);
            this.tabMainControl.Controls.Add(this.tabEmployeeManagement);
            this.tabMainControl.Controls.Add(this.tabTheatreManagement);
            this.tabMainControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabMainControl.HotTrack = true;
            this.tabMainControl.Location = new System.Drawing.Point(0, 0);
            this.tabMainControl.Multiline = true;
            this.tabMainControl.Name = "tabMainControl";
            this.tabMainControl.SelectedIndex = 0;
            this.tabMainControl.Size = new System.Drawing.Size(932, 532);
            this.tabMainControl.TabIndex = 0;
            this.tabMainControl.SelectedIndexChanged += new System.EventHandler(this.tabMainControl_SelectedIndexChanged);
            this.tabMainControl.Click += new System.EventHandler(this.buttonQueriesTab);
            // 
            // tabReviews
            // 
            this.tabReviews.Controls.Add(this.groupBox10);
            this.tabReviews.Controls.Add(this.groupBox8);
            this.tabReviews.Location = new System.Drawing.Point(4, 22);
            this.tabReviews.Name = "tabReviews";
            this.tabReviews.Padding = new System.Windows.Forms.Padding(3);
            this.tabReviews.Size = new System.Drawing.Size(924, 506);
            this.tabReviews.TabIndex = 6;
            this.tabReviews.Text = "Reviews";
            this.tabReviews.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.labelReviewOwner);
            this.groupBox10.Controls.Add(this.label64);
            this.groupBox10.Controls.Add(this.labelReviewAlreadyVoted);
            this.groupBox10.Controls.Add(this.btnReviewDownvote);
            this.groupBox10.Controls.Add(this.btnReviewUpvote);
            this.groupBox10.Controls.Add(this.labelReviewRating);
            this.groupBox10.Controls.Add(this.label63);
            this.groupBox10.Controls.Add(this.btnThreadReply);
            this.groupBox10.Controls.Add(this.textReviewReplyBox);
            this.groupBox10.Controls.Add(this.listReviewData);
            this.groupBox10.Controls.Add(this.groupReviewAddBox);
            this.groupBox10.Controls.Add(this.labelReviewDiscussion);
            this.groupBox10.Controls.Add(this.label14);
            this.groupBox10.Location = new System.Drawing.Point(230, 6);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(686, 476);
            this.groupBox10.TabIndex = 1;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Reviews";
            // 
            // labelReviewOwner
            // 
            this.labelReviewOwner.AutoSize = true;
            this.labelReviewOwner.Location = new System.Drawing.Point(97, 34);
            this.labelReviewOwner.Name = "labelReviewOwner";
            this.labelReviewOwner.Size = new System.Drawing.Size(13, 13);
            this.labelReviewOwner.TabIndex = 14;
            this.labelReviewOwner.Text = "--";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(30, 34);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(61, 13);
            this.label64.TabIndex = 13;
            this.label64.Text = "Created by:";
            // 
            // labelReviewAlreadyVoted
            // 
            this.labelReviewAlreadyVoted.AutoSize = true;
            this.labelReviewAlreadyVoted.Location = new System.Drawing.Point(517, 16);
            this.labelReviewAlreadyVoted.Name = "labelReviewAlreadyVoted";
            this.labelReviewAlreadyVoted.Size = new System.Drawing.Size(123, 13);
            this.labelReviewAlreadyVoted.TabIndex = 12;
            this.labelReviewAlreadyVoted.Text = "You have already voted!";
            this.labelReviewAlreadyVoted.Visible = false;
            // 
            // btnReviewDownvote
            // 
            this.btnReviewDownvote.Location = new System.Drawing.Point(578, 29);
            this.btnReviewDownvote.Name = "btnReviewDownvote";
            this.btnReviewDownvote.Size = new System.Drawing.Size(75, 23);
            this.btnReviewDownvote.TabIndex = 11;
            this.btnReviewDownvote.Text = "Downvote";
            this.btnReviewDownvote.UseVisualStyleBackColor = true;
            this.btnReviewDownvote.Visible = false;
            this.btnReviewDownvote.Click += new System.EventHandler(this.btnReviewDownvote_Click);
            // 
            // btnReviewUpvote
            // 
            this.btnReviewUpvote.Location = new System.Drawing.Point(497, 29);
            this.btnReviewUpvote.Name = "btnReviewUpvote";
            this.btnReviewUpvote.Size = new System.Drawing.Size(75, 23);
            this.btnReviewUpvote.TabIndex = 10;
            this.btnReviewUpvote.Text = "Upvote";
            this.btnReviewUpvote.UseVisualStyleBackColor = true;
            this.btnReviewUpvote.Visible = false;
            this.btnReviewUpvote.Click += new System.EventHandler(this.btnReviewUpvote_Click);
            // 
            // labelReviewRating
            // 
            this.labelReviewRating.AutoSize = true;
            this.labelReviewRating.Location = new System.Drawing.Point(451, 29);
            this.labelReviewRating.Name = "labelReviewRating";
            this.labelReviewRating.Size = new System.Drawing.Size(13, 13);
            this.labelReviewRating.TabIndex = 9;
            this.labelReviewRating.Text = "0";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(404, 29);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(41, 13);
            this.label63.TabIndex = 8;
            this.label63.Text = "Rating:";
            // 
            // btnThreadReply
            // 
            this.btnThreadReply.Location = new System.Drawing.Point(578, 323);
            this.btnThreadReply.Name = "btnThreadReply";
            this.btnThreadReply.Size = new System.Drawing.Size(87, 23);
            this.btnThreadReply.TabIndex = 7;
            this.btnThreadReply.Text = "Reply";
            this.btnThreadReply.UseVisualStyleBackColor = true;
            this.btnThreadReply.Click += new System.EventHandler(this.btnThreadReply_Click);
            // 
            // textReviewReplyBox
            // 
            this.textReviewReplyBox.Location = new System.Drawing.Point(18, 325);
            this.textReviewReplyBox.Name = "textReviewReplyBox";
            this.textReviewReplyBox.Size = new System.Drawing.Size(554, 20);
            this.textReviewReplyBox.TabIndex = 6;
            // 
            // listReviewData
            // 
            this.listReviewData.FormattingEnabled = true;
            this.listReviewData.Location = new System.Drawing.Point(18, 55);
            this.listReviewData.Name = "listReviewData";
            this.listReviewData.Size = new System.Drawing.Size(647, 264);
            this.listReviewData.TabIndex = 5;
            // 
            // groupReviewAddBox
            // 
            this.groupReviewAddBox.Controls.Add(this.comboReviewTarget);
            this.groupReviewAddBox.Controls.Add(this.label17);
            this.groupReviewAddBox.Controls.Add(this.btnReviewCreateThread);
            this.groupReviewAddBox.Controls.Add(this.textReviewAddMessage);
            this.groupReviewAddBox.Controls.Add(this.label16);
            this.groupReviewAddBox.Controls.Add(this.textReviewAddTitle);
            this.groupReviewAddBox.Controls.Add(this.radioReviewMovie);
            this.groupReviewAddBox.Controls.Add(this.radioReviewTheatre);
            this.groupReviewAddBox.Location = new System.Drawing.Point(18, 366);
            this.groupReviewAddBox.Name = "groupReviewAddBox";
            this.groupReviewAddBox.Size = new System.Drawing.Size(647, 100);
            this.groupReviewAddBox.TabIndex = 3;
            this.groupReviewAddBox.TabStop = false;
            this.groupReviewAddBox.Text = "Add Review";
            // 
            // comboReviewTarget
            // 
            this.comboReviewTarget.FormattingEnabled = true;
            this.comboReviewTarget.Location = new System.Drawing.Point(7, 64);
            this.comboReviewTarget.Name = "comboReviewTarget";
            this.comboReviewTarget.Size = new System.Drawing.Size(135, 21);
            this.comboReviewTarget.TabIndex = 7;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(148, 45);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 13);
            this.label17.TabIndex = 6;
            this.label17.Text = "Message:";
            // 
            // btnReviewCreateThread
            // 
            this.btnReviewCreateThread.Location = new System.Drawing.Point(521, 68);
            this.btnReviewCreateThread.Name = "btnReviewCreateThread";
            this.btnReviewCreateThread.Size = new System.Drawing.Size(120, 23);
            this.btnReviewCreateThread.TabIndex = 5;
            this.btnReviewCreateThread.Text = "Create Thread";
            this.btnReviewCreateThread.UseVisualStyleBackColor = true;
            this.btnReviewCreateThread.Click += new System.EventHandler(this.btnReviewCreateThread_Click);
            // 
            // textReviewAddMessage
            // 
            this.textReviewAddMessage.Location = new System.Drawing.Point(207, 42);
            this.textReviewAddMessage.Name = "textReviewAddMessage";
            this.textReviewAddMessage.Size = new System.Drawing.Size(434, 20);
            this.textReviewAddMessage.TabIndex = 4;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(171, 21);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(30, 13);
            this.label16.TabIndex = 3;
            this.label16.Text = "Title:";
            // 
            // textReviewAddTitle
            // 
            this.textReviewAddTitle.Location = new System.Drawing.Point(207, 18);
            this.textReviewAddTitle.Name = "textReviewAddTitle";
            this.textReviewAddTitle.Size = new System.Drawing.Size(434, 20);
            this.textReviewAddTitle.TabIndex = 2;
            // 
            // radioReviewMovie
            // 
            this.radioReviewMovie.AutoSize = true;
            this.radioReviewMovie.Location = new System.Drawing.Point(7, 41);
            this.radioReviewMovie.Name = "radioReviewMovie";
            this.radioReviewMovie.Size = new System.Drawing.Size(54, 17);
            this.radioReviewMovie.TabIndex = 1;
            this.radioReviewMovie.TabStop = true;
            this.radioReviewMovie.Text = "Movie";
            this.radioReviewMovie.UseVisualStyleBackColor = true;
            this.radioReviewMovie.CheckedChanged += new System.EventHandler(this.radioReviewMovie_CheckedChanged);
            // 
            // radioReviewTheatre
            // 
            this.radioReviewTheatre.AutoSize = true;
            this.radioReviewTheatre.Location = new System.Drawing.Point(7, 19);
            this.radioReviewTheatre.Name = "radioReviewTheatre";
            this.radioReviewTheatre.Size = new System.Drawing.Size(62, 17);
            this.radioReviewTheatre.TabIndex = 0;
            this.radioReviewTheatre.TabStop = true;
            this.radioReviewTheatre.Text = "Theatre";
            this.radioReviewTheatre.UseVisualStyleBackColor = true;
            this.radioReviewTheatre.CheckedChanged += new System.EventHandler(this.radioReviewTheatre_CheckedChanged);
            // 
            // labelReviewDiscussion
            // 
            this.labelReviewDiscussion.AutoSize = true;
            this.labelReviewDiscussion.Location = new System.Drawing.Point(97, 16);
            this.labelReviewDiscussion.Name = "labelReviewDiscussion";
            this.labelReviewDiscussion.Size = new System.Drawing.Size(13, 13);
            this.labelReviewDiscussion.TabIndex = 2;
            this.labelReviewDiscussion.Text = "--";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(15, 16);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(76, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Discussion for:";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.tabReviewControl);
            this.groupBox8.Location = new System.Drawing.Point(8, 6);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(216, 479);
            this.groupBox8.TabIndex = 0;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Navigation";
            // 
            // tabReviewControl
            // 
            this.tabReviewControl.Controls.Add(this.panelReviewTheatres);
            this.tabReviewControl.Controls.Add(this.panelReviewMovies);
            this.tabReviewControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabReviewControl.Location = new System.Drawing.Point(3, 16);
            this.tabReviewControl.Name = "tabReviewControl";
            this.tabReviewControl.SelectedIndex = 0;
            this.tabReviewControl.Size = new System.Drawing.Size(210, 460);
            this.tabReviewControl.TabIndex = 0;
            // 
            // panelReviewTheatres
            // 
            this.panelReviewTheatres.Controls.Add(this.btnReviewsRefreshTheatres);
            this.panelReviewTheatres.Controls.Add(this.listReviewTheatres);
            this.panelReviewTheatres.Location = new System.Drawing.Point(4, 22);
            this.panelReviewTheatres.Name = "panelReviewTheatres";
            this.panelReviewTheatres.Padding = new System.Windows.Forms.Padding(3);
            this.panelReviewTheatres.Size = new System.Drawing.Size(202, 434);
            this.panelReviewTheatres.TabIndex = 0;
            this.panelReviewTheatres.Text = "Theatres";
            this.panelReviewTheatres.UseVisualStyleBackColor = true;
            // 
            // btnReviewsRefreshTheatres
            // 
            this.btnReviewsRefreshTheatres.Location = new System.Drawing.Point(6, 405);
            this.btnReviewsRefreshTheatres.Name = "btnReviewsRefreshTheatres";
            this.btnReviewsRefreshTheatres.Size = new System.Drawing.Size(190, 23);
            this.btnReviewsRefreshTheatres.TabIndex = 1;
            this.btnReviewsRefreshTheatres.Text = "Refresh Theatres";
            this.btnReviewsRefreshTheatres.UseVisualStyleBackColor = true;
            this.btnReviewsRefreshTheatres.Click += new System.EventHandler(this.btnReviewsRefreshTheatres_Click);
            // 
            // listReviewTheatres
            // 
            this.listReviewTheatres.FormattingEnabled = true;
            this.listReviewTheatres.Location = new System.Drawing.Point(6, 6);
            this.listReviewTheatres.Name = "listReviewTheatres";
            this.listReviewTheatres.Size = new System.Drawing.Size(190, 394);
            this.listReviewTheatres.TabIndex = 0;
            this.listReviewTheatres.SelectedIndexChanged += new System.EventHandler(this.listReviewTheatres_SelectedIndexChanged);
            // 
            // panelReviewMovies
            // 
            this.panelReviewMovies.Controls.Add(this.btnReviewsRefreshMovies);
            this.panelReviewMovies.Controls.Add(this.listReviewMovies);
            this.panelReviewMovies.Location = new System.Drawing.Point(4, 22);
            this.panelReviewMovies.Name = "panelReviewMovies";
            this.panelReviewMovies.Padding = new System.Windows.Forms.Padding(3);
            this.panelReviewMovies.Size = new System.Drawing.Size(202, 434);
            this.panelReviewMovies.TabIndex = 1;
            this.panelReviewMovies.Text = "Movies";
            this.panelReviewMovies.UseVisualStyleBackColor = true;
            // 
            // btnReviewsRefreshMovies
            // 
            this.btnReviewsRefreshMovies.Location = new System.Drawing.Point(6, 405);
            this.btnReviewsRefreshMovies.Name = "btnReviewsRefreshMovies";
            this.btnReviewsRefreshMovies.Size = new System.Drawing.Size(190, 23);
            this.btnReviewsRefreshMovies.TabIndex = 3;
            this.btnReviewsRefreshMovies.Text = "Refresh Movies";
            this.btnReviewsRefreshMovies.UseVisualStyleBackColor = true;
            this.btnReviewsRefreshMovies.Click += new System.EventHandler(this.btnReviewsRefreshMovies_Click);
            // 
            // listReviewMovies
            // 
            this.listReviewMovies.FormattingEnabled = true;
            this.listReviewMovies.Location = new System.Drawing.Point(6, 6);
            this.listReviewMovies.Name = "listReviewMovies";
            this.listReviewMovies.Size = new System.Drawing.Size(190, 394);
            this.listReviewMovies.TabIndex = 2;
            this.listReviewMovies.SelectedIndexChanged += new System.EventHandler(this.listReviewMovies_SelectedIndexChanged);
            // 
            // tabQueries
            // 
            this.tabQueries.Controls.Add(this.comboBoxQuery8);
            this.tabQueries.Controls.Add(this.buttonQuery8);
            this.tabQueries.Controls.Add(this.listBoxQuery8);
            this.tabQueries.Controls.Add(this.labelQuery8);
            this.tabQueries.Controls.Add(this.buttonQuery7);
            this.tabQueries.Controls.Add(this.listBoxQuery7);
            this.tabQueries.Controls.Add(this.labelQuery7);
            this.tabQueries.Controls.Add(this.buttonQuery6);
            this.tabQueries.Controls.Add(this.listBoxQuery6);
            this.tabQueries.Controls.Add(this.labelQuery6);
            this.tabQueries.Controls.Add(this.buttonQuery5);
            this.tabQueries.Controls.Add(this.listBoxQuery5);
            this.tabQueries.Controls.Add(this.labelQuery5);
            this.tabQueries.Controls.Add(this.buttonQuery3);
            this.tabQueries.Controls.Add(this.listBoxQuery3);
            this.tabQueries.Controls.Add(this.label22);
            this.tabQueries.Controls.Add(this.buttonQuery2);
            this.tabQueries.Controls.Add(this.listBoxQuery2);
            this.tabQueries.Controls.Add(this.labelQuery2);
            this.tabQueries.Controls.Add(this.buttonQuery1);
            this.tabQueries.Controls.Add(this.listBoxQuery1);
            this.tabQueries.Controls.Add(this.comboBoxQuery1);
            this.tabQueries.Controls.Add(this.Query1);
            this.tabQueries.Location = new System.Drawing.Point(4, 22);
            this.tabQueries.Name = "tabQueries";
            this.tabQueries.Padding = new System.Windows.Forms.Padding(3);
            this.tabQueries.Size = new System.Drawing.Size(924, 506);
            this.tabQueries.TabIndex = 7;
            this.tabQueries.Text = "Queries";
            this.tabQueries.UseVisualStyleBackColor = true;
            // 
            // comboBoxQuery8
            // 
            this.comboBoxQuery8.AutoCompleteCustomSource.AddRange(new string[] {
            "Theatres",
            "Movies"});
            this.comboBoxQuery8.FormattingEnabled = true;
            this.comboBoxQuery8.Items.AddRange(new object[] {
            "Theatres",
            "Movies"});
            this.comboBoxQuery8.Location = new System.Drawing.Point(213, 215);
            this.comboBoxQuery8.MaxDropDownItems = 2;
            this.comboBoxQuery8.Name = "comboBoxQuery8";
            this.comboBoxQuery8.Size = new System.Drawing.Size(121, 21);
            this.comboBoxQuery8.TabIndex = 22;
            // 
            // buttonQuery8
            // 
            this.buttonQuery8.Location = new System.Drawing.Point(259, 447);
            this.buttonQuery8.Name = "buttonQuery8";
            this.buttonQuery8.Size = new System.Drawing.Size(75, 23);
            this.buttonQuery8.TabIndex = 21;
            this.buttonQuery8.Text = "Enter";
            this.buttonQuery8.UseVisualStyleBackColor = true;
            this.buttonQuery8.Click += new System.EventHandler(this.buttonQuery8_Click);
            // 
            // listBoxQuery8
            // 
            this.listBoxQuery8.FormattingEnabled = true;
            this.listBoxQuery8.Location = new System.Drawing.Point(2, 242);
            this.listBoxQuery8.Name = "listBoxQuery8";
            this.listBoxQuery8.Size = new System.Drawing.Size(332, 199);
            this.listBoxQuery8.TabIndex = 20;
            // 
            // labelQuery8
            // 
            this.labelQuery8.AutoSize = true;
            this.labelQuery8.Location = new System.Drawing.Point(3, 218);
            this.labelQuery8.Name = "labelQuery8";
            this.labelQuery8.Size = new System.Drawing.Size(214, 13);
            this.labelQuery8.TabIndex = 19;
            this.labelQuery8.Text = "Employees working on Mondays at Theatre:";
            // 
            // buttonQuery7
            // 
            this.buttonQuery7.Location = new System.Drawing.Point(685, 180);
            this.buttonQuery7.Name = "buttonQuery7";
            this.buttonQuery7.Size = new System.Drawing.Size(75, 23);
            this.buttonQuery7.TabIndex = 18;
            this.buttonQuery7.Text = "Enter";
            this.buttonQuery7.UseVisualStyleBackColor = true;
            this.buttonQuery7.Click += new System.EventHandler(this.buttonQuery7_Click);
            // 
            // listBoxQuery7
            // 
            this.listBoxQuery7.FormattingEnabled = true;
            this.listBoxQuery7.Location = new System.Drawing.Point(512, 131);
            this.listBoxQuery7.Name = "listBoxQuery7";
            this.listBoxQuery7.Size = new System.Drawing.Size(248, 43);
            this.listBoxQuery7.TabIndex = 17;
            // 
            // labelQuery7
            // 
            this.labelQuery7.AutoSize = true;
            this.labelQuery7.Location = new System.Drawing.Point(513, 115);
            this.labelQuery7.Name = "labelQuery7";
            this.labelQuery7.Size = new System.Drawing.Size(172, 13);
            this.labelQuery7.TabIndex = 16;
            this.labelQuery7.Text = "theatre(s) with the most ticket sales";
            // 
            // buttonQuery6
            // 
            this.buttonQuery6.Location = new System.Drawing.Point(431, 180);
            this.buttonQuery6.Name = "buttonQuery6";
            this.buttonQuery6.Size = new System.Drawing.Size(75, 23);
            this.buttonQuery6.TabIndex = 15;
            this.buttonQuery6.Text = "Enter";
            this.buttonQuery6.UseVisualStyleBackColor = true;
            this.buttonQuery6.Click += new System.EventHandler(this.buttonQuery6_Click);
            // 
            // listBoxQuery6
            // 
            this.listBoxQuery6.FormattingEnabled = true;
            this.listBoxQuery6.Location = new System.Drawing.Point(258, 131);
            this.listBoxQuery6.Name = "listBoxQuery6";
            this.listBoxQuery6.Size = new System.Drawing.Size(248, 43);
            this.listBoxQuery6.TabIndex = 14;
            // 
            // labelQuery6
            // 
            this.labelQuery6.AutoSize = true;
            this.labelQuery6.Location = new System.Drawing.Point(259, 115);
            this.labelQuery6.Name = "labelQuery6";
            this.labelQuery6.Size = new System.Drawing.Size(172, 13);
            this.labelQuery6.TabIndex = 13;
            this.labelQuery6.Text = "theatre(s) showing the most movies";
            // 
            // buttonQuery5
            // 
            this.buttonQuery5.Location = new System.Drawing.Point(176, 180);
            this.buttonQuery5.Name = "buttonQuery5";
            this.buttonQuery5.Size = new System.Drawing.Size(75, 23);
            this.buttonQuery5.TabIndex = 12;
            this.buttonQuery5.Text = "Enter";
            this.buttonQuery5.UseVisualStyleBackColor = true;
            this.buttonQuery5.Click += new System.EventHandler(this.buttonQuery5_Click);
            // 
            // listBoxQuery5
            // 
            this.listBoxQuery5.FormattingEnabled = true;
            this.listBoxQuery5.Location = new System.Drawing.Point(3, 131);
            this.listBoxQuery5.Name = "listBoxQuery5";
            this.listBoxQuery5.Size = new System.Drawing.Size(248, 43);
            this.listBoxQuery5.TabIndex = 11;
            // 
            // labelQuery5
            // 
            this.labelQuery5.AutoSize = true;
            this.labelQuery5.Location = new System.Drawing.Point(3, 115);
            this.labelQuery5.Name = "labelQuery5";
            this.labelQuery5.Size = new System.Drawing.Size(174, 13);
            this.labelQuery5.TabIndex = 10;
            this.labelQuery5.Text = "registered user with most comments";
            // 
            // buttonQuery3
            // 
            this.buttonQuery3.Location = new System.Drawing.Point(739, 76);
            this.buttonQuery3.Name = "buttonQuery3";
            this.buttonQuery3.Size = new System.Drawing.Size(75, 23);
            this.buttonQuery3.TabIndex = 9;
            this.buttonQuery3.Text = "Enter";
            this.buttonQuery3.UseVisualStyleBackColor = true;
            this.buttonQuery3.Click += new System.EventHandler(this.buttonQuery3_Click);
            // 
            // listBoxQuery3
            // 
            this.listBoxQuery3.FormattingEnabled = true;
            this.listBoxQuery3.Location = new System.Drawing.Point(516, 27);
            this.listBoxQuery3.Name = "listBoxQuery3";
            this.listBoxQuery3.Size = new System.Drawing.Size(298, 43);
            this.listBoxQuery3.TabIndex = 8;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(513, 3);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(301, 13);
            this.label22.TabIndex = 7;
            this.label22.Text = "least popular discussion thread in terms of visits and comments";
            // 
            // buttonQuery2
            // 
            this.buttonQuery2.Location = new System.Drawing.Point(427, 76);
            this.buttonQuery2.Name = "buttonQuery2";
            this.buttonQuery2.Size = new System.Drawing.Size(75, 23);
            this.buttonQuery2.TabIndex = 6;
            this.buttonQuery2.Text = "Enter";
            this.buttonQuery2.UseVisualStyleBackColor = true;
            this.buttonQuery2.Click += new System.EventHandler(this.buttonQuery2_Click);
            // 
            // listBoxQuery2
            // 
            this.listBoxQuery2.FormattingEnabled = true;
            this.listBoxQuery2.Location = new System.Drawing.Point(258, 27);
            this.listBoxQuery2.Name = "listBoxQuery2";
            this.listBoxQuery2.Size = new System.Drawing.Size(244, 43);
            this.listBoxQuery2.TabIndex = 5;
            // 
            // labelQuery2
            // 
            this.labelQuery2.AutoSize = true;
            this.labelQuery2.Location = new System.Drawing.Point(259, 4);
            this.labelQuery2.Name = "labelQuery2";
            this.labelQuery2.Size = new System.Drawing.Size(135, 13);
            this.labelQuery2.TabIndex = 4;
            this.labelQuery2.Text = "3 most recent posts from all";
            // 
            // buttonQuery1
            // 
            this.buttonQuery1.Location = new System.Drawing.Point(176, 77);
            this.buttonQuery1.Name = "buttonQuery1";
            this.buttonQuery1.Size = new System.Drawing.Size(75, 23);
            this.buttonQuery1.TabIndex = 3;
            this.buttonQuery1.Text = "Enter";
            this.buttonQuery1.UseVisualStyleBackColor = true;
            this.buttonQuery1.Click += new System.EventHandler(this.buttonQuery1_Click);
            // 
            // listBoxQuery1
            // 
            this.listBoxQuery1.FormattingEnabled = true;
            this.listBoxQuery1.Location = new System.Drawing.Point(8, 27);
            this.listBoxQuery1.Name = "listBoxQuery1";
            this.listBoxQuery1.Size = new System.Drawing.Size(244, 43);
            this.listBoxQuery1.TabIndex = 2;
            // 
            // comboBoxQuery1
            // 
            this.comboBoxQuery1.AutoCompleteCustomSource.AddRange(new string[] {
            "Theatres",
            "Movies"});
            this.comboBoxQuery1.FormattingEnabled = true;
            this.comboBoxQuery1.Items.AddRange(new object[] {
            "Theatres",
            "Movies"});
            this.comboBoxQuery1.Location = new System.Drawing.Point(131, 0);
            this.comboBoxQuery1.MaxDropDownItems = 2;
            this.comboBoxQuery1.Name = "comboBoxQuery1";
            this.comboBoxQuery1.Size = new System.Drawing.Size(121, 21);
            this.comboBoxQuery1.TabIndex = 1;
            // 
            // Query1
            // 
            this.Query1.AutoSize = true;
            this.Query1.Location = new System.Drawing.Point(3, 3);
            this.Query1.Name = "Query1";
            this.Query1.Size = new System.Drawing.Size(122, 13);
            this.Query1.TabIndex = 0;
            this.Query1.Text = "3 most recent posts from";
            // 
            // tabSchedule
            // 
            this.tabSchedule.Location = new System.Drawing.Point(4, 22);
            this.tabSchedule.Name = "tabSchedule";
            this.tabSchedule.Padding = new System.Windows.Forms.Padding(3);
            this.tabSchedule.Size = new System.Drawing.Size(924, 506);
            this.tabSchedule.TabIndex = 8;
            this.tabSchedule.Text = "Schedules";
            this.tabSchedule.UseVisualStyleBackColor = true;
            // 
            // tabTickets
            // 
            this.tabTickets.Controls.Add(this.listTicketTimeIDHidden);
            this.tabTickets.Controls.Add(this.panelTicketBillingInfo);
            this.tabTickets.Controls.Add(this.listTicketMovieIDHidden);
            this.tabTickets.Controls.Add(this.btnTicketReset);
            this.tabTickets.Controls.Add(this.groupTicketTime);
            this.tabTickets.Controls.Add(this.groupTicketMovie);
            this.tabTickets.Controls.Add(this.label23);
            this.tabTickets.Controls.Add(this.groupTicketTheatre);
            this.tabTickets.Controls.Add(this.panelTicketFogOfWar);
            this.tabTickets.Location = new System.Drawing.Point(4, 22);
            this.tabTickets.Name = "tabTickets";
            this.tabTickets.Padding = new System.Windows.Forms.Padding(3);
            this.tabTickets.Size = new System.Drawing.Size(924, 506);
            this.tabTickets.TabIndex = 9;
            this.tabTickets.Text = "Buy Tickets";
            this.tabTickets.UseVisualStyleBackColor = true;
            // 
            // listTicketTimeIDHidden
            // 
            this.listTicketTimeIDHidden.FormattingEnabled = true;
            this.listTicketTimeIDHidden.Location = new System.Drawing.Point(388, 6);
            this.listTicketTimeIDHidden.Name = "listTicketTimeIDHidden";
            this.listTicketTimeIDHidden.Size = new System.Drawing.Size(117, 56);
            this.listTicketTimeIDHidden.TabIndex = 45;
            this.listTicketTimeIDHidden.Visible = false;
            // 
            // panelTicketBillingInfo
            // 
            this.panelTicketBillingInfo.Controls.Add(this.labelTicketSeats);
            this.panelTicketBillingInfo.Controls.Add(this.label45);
            this.panelTicketBillingInfo.Controls.Add(this.label44);
            this.panelTicketBillingInfo.Controls.Add(this.textTicketCCType);
            this.panelTicketBillingInfo.Controls.Add(this.label42);
            this.panelTicketBillingInfo.Controls.Add(this.btnTicketBuy);
            this.panelTicketBillingInfo.Controls.Add(this.label40);
            this.panelTicketBillingInfo.Controls.Add(this.labelTicketMustRegister);
            this.panelTicketBillingInfo.Controls.Add(this.textTicketUsername);
            this.panelTicketBillingInfo.Controls.Add(this.textTicketCCExp);
            this.panelTicketBillingInfo.Controls.Add(this.label38);
            this.panelTicketBillingInfo.Controls.Add(this.label24);
            this.panelTicketBillingInfo.Controls.Add(this.textTicketPassword);
            this.panelTicketBillingInfo.Controls.Add(this.textTicketCC);
            this.panelTicketBillingInfo.Controls.Add(this.btnTicketRegisterAndBuy);
            this.panelTicketBillingInfo.Controls.Add(this.label25);
            this.panelTicketBillingInfo.Controls.Add(this.label37);
            this.panelTicketBillingInfo.Controls.Add(this.label26);
            this.panelTicketBillingInfo.Controls.Add(this.textTicketEmail);
            this.panelTicketBillingInfo.Controls.Add(this.textTicketFullName);
            this.panelTicketBillingInfo.Controls.Add(this.label36);
            this.panelTicketBillingInfo.Controls.Add(this.textTicketPhone);
            this.panelTicketBillingInfo.Controls.Add(this.textTicketAddress);
            this.panelTicketBillingInfo.Controls.Add(this.label27);
            this.panelTicketBillingInfo.Location = new System.Drawing.Point(644, 71);
            this.panelTicketBillingInfo.Name = "panelTicketBillingInfo";
            this.panelTicketBillingInfo.Size = new System.Drawing.Size(263, 392);
            this.panelTicketBillingInfo.TabIndex = 44;
            this.panelTicketBillingInfo.Visible = false;
            // 
            // labelTicketSeats
            // 
            this.labelTicketSeats.AutoSize = true;
            this.labelTicketSeats.Location = new System.Drawing.Point(222, 5);
            this.labelTicketSeats.Name = "labelTicketSeats";
            this.labelTicketSeats.Size = new System.Drawing.Size(25, 13);
            this.labelTicketSeats.TabIndex = 47;
            this.labelTicketSeats.Text = "100";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(179, 5);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(37, 13);
            this.label45.TabIndex = 46;
            this.label45.Text = "Seats:";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(5, 284);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(31, 13);
            this.label44.TabIndex = 45;
            this.label44.Text = "Type";
            // 
            // textTicketCCType
            // 
            this.textTicketCCType.FormattingEnabled = true;
            this.textTicketCCType.Items.AddRange(new object[] {
            "visa",
            "master",
            "amex",
            "other"});
            this.textTicketCCType.Location = new System.Drawing.Point(5, 299);
            this.textTicketCCType.Name = "textTicketCCType";
            this.textTicketCCType.Size = new System.Drawing.Size(43, 21);
            this.textTicketCCType.TabIndex = 44;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(3, 5);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(89, 13);
            this.label42.TabIndex = 24;
            this.label42.Text = "Billing Information";
            // 
            // btnTicketBuy
            // 
            this.btnTicketBuy.Location = new System.Drawing.Point(6, 363);
            this.btnTicketBuy.Name = "btnTicketBuy";
            this.btnTicketBuy.Size = new System.Drawing.Size(248, 23);
            this.btnTicketBuy.TabIndex = 43;
            this.btnTicketBuy.Text = "Purchase Ticket";
            this.btnTicketBuy.UseVisualStyleBackColor = true;
            this.btnTicketBuy.Click += new System.EventHandler(this.btnTicketBuy_Click);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(3, 52);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(55, 13);
            this.label40.TabIndex = 25;
            this.label40.Text = "Username";
            // 
            // labelTicketMustRegister
            // 
            this.labelTicketMustRegister.AutoSize = true;
            this.labelTicketMustRegister.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTicketMustRegister.Location = new System.Drawing.Point(3, 26);
            this.labelTicketMustRegister.Name = "labelTicketMustRegister";
            this.labelTicketMustRegister.Size = new System.Drawing.Size(158, 13);
            this.labelTicketMustRegister.TabIndex = 42;
            this.labelTicketMustRegister.Text = "You are not yet registered!";
            // 
            // textTicketUsername
            // 
            this.textTicketUsername.Location = new System.Drawing.Point(6, 68);
            this.textTicketUsername.Name = "textTicketUsername";
            this.textTicketUsername.Size = new System.Drawing.Size(248, 20);
            this.textTicketUsername.TabIndex = 26;
            // 
            // textTicketCCExp
            // 
            this.textTicketCCExp.Location = new System.Drawing.Point(182, 299);
            this.textTicketCCExp.Name = "textTicketCCExp";
            this.textTicketCCExp.Size = new System.Drawing.Size(72, 20);
            this.textTicketCCExp.TabIndex = 41;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(3, 89);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(53, 13);
            this.label38.TabIndex = 27;
            this.label38.Text = "Password";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(179, 283);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(54, 13);
            this.label24.TabIndex = 40;
            this.label24.Text = "Exp. Date";
            // 
            // textTicketPassword
            // 
            this.textTicketPassword.Location = new System.Drawing.Point(6, 106);
            this.textTicketPassword.Name = "textTicketPassword";
            this.textTicketPassword.PasswordChar = '*';
            this.textTicketPassword.Size = new System.Drawing.Size(248, 20);
            this.textTicketPassword.TabIndex = 28;
            // 
            // textTicketCC
            // 
            this.textTicketCC.Location = new System.Drawing.Point(52, 299);
            this.textTicketCC.Name = "textTicketCC";
            this.textTicketCC.Size = new System.Drawing.Size(124, 20);
            this.textTicketCC.TabIndex = 39;
            // 
            // btnTicketRegisterAndBuy
            // 
            this.btnTicketRegisterAndBuy.Location = new System.Drawing.Point(6, 334);
            this.btnTicketRegisterAndBuy.Name = "btnTicketRegisterAndBuy";
            this.btnTicketRegisterAndBuy.Size = new System.Drawing.Size(248, 23);
            this.btnTicketRegisterAndBuy.TabIndex = 29;
            this.btnTicketRegisterAndBuy.Text = "Register and Purchase Ticket";
            this.btnTicketRegisterAndBuy.UseVisualStyleBackColor = true;
            this.btnTicketRegisterAndBuy.Click += new System.EventHandler(this.btnTicketRegisterAndBuy_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(49, 283);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(99, 13);
            this.label25.TabIndex = 38;
            this.label25.Text = "Credit Card Number";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(3, 127);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(32, 13);
            this.label37.TabIndex = 30;
            this.label37.Text = "Email";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(5, 244);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(54, 13);
            this.label26.TabIndex = 37;
            this.label26.Text = "Full Name";
            // 
            // textTicketEmail
            // 
            this.textTicketEmail.Location = new System.Drawing.Point(6, 143);
            this.textTicketEmail.Name = "textTicketEmail";
            this.textTicketEmail.Size = new System.Drawing.Size(248, 20);
            this.textTicketEmail.TabIndex = 31;
            // 
            // textTicketFullName
            // 
            this.textTicketFullName.Location = new System.Drawing.Point(6, 260);
            this.textTicketFullName.Name = "textTicketFullName";
            this.textTicketFullName.Size = new System.Drawing.Size(248, 20);
            this.textTicketFullName.TabIndex = 36;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(3, 166);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(45, 13);
            this.label36.TabIndex = 32;
            this.label36.Text = "Address";
            // 
            // textTicketPhone
            // 
            this.textTicketPhone.Location = new System.Drawing.Point(6, 221);
            this.textTicketPhone.Name = "textTicketPhone";
            this.textTicketPhone.Size = new System.Drawing.Size(248, 20);
            this.textTicketPhone.TabIndex = 35;
            // 
            // textTicketAddress
            // 
            this.textTicketAddress.Location = new System.Drawing.Point(6, 182);
            this.textTicketAddress.Name = "textTicketAddress";
            this.textTicketAddress.Size = new System.Drawing.Size(248, 20);
            this.textTicketAddress.TabIndex = 33;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(3, 205);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(78, 13);
            this.label27.TabIndex = 34;
            this.label27.Text = "Phone Number";
            // 
            // listTicketMovieIDHidden
            // 
            this.listTicketMovieIDHidden.FormattingEnabled = true;
            this.listTicketMovieIDHidden.Location = new System.Drawing.Point(276, 6);
            this.listTicketMovieIDHidden.Name = "listTicketMovieIDHidden";
            this.listTicketMovieIDHidden.Size = new System.Drawing.Size(106, 56);
            this.listTicketMovieIDHidden.TabIndex = 6;
            this.listTicketMovieIDHidden.Visible = false;
            // 
            // btnTicketReset
            // 
            this.btnTicketReset.Location = new System.Drawing.Point(575, 45);
            this.btnTicketReset.Name = "btnTicketReset";
            this.btnTicketReset.Size = new System.Drawing.Size(57, 23);
            this.btnTicketReset.TabIndex = 4;
            this.btnTicketReset.Text = "Reset";
            this.btnTicketReset.UseVisualStyleBackColor = true;
            this.btnTicketReset.Click += new System.EventHandler(this.btnTicketReset_Click);
            // 
            // groupTicketTime
            // 
            this.groupTicketTime.Controls.Add(this.listTicketTime);
            this.groupTicketTime.Location = new System.Drawing.Point(438, 71);
            this.groupTicketTime.Name = "groupTicketTime";
            this.groupTicketTime.Size = new System.Drawing.Size(200, 400);
            this.groupTicketTime.TabIndex = 3;
            this.groupTicketTime.TabStop = false;
            this.groupTicketTime.Text = "Time";
            this.groupTicketTime.Visible = false;
            // 
            // listTicketTime
            // 
            this.listTicketTime.FormattingEnabled = true;
            this.listTicketTime.Location = new System.Drawing.Point(6, 16);
            this.listTicketTime.Name = "listTicketTime";
            this.listTicketTime.Size = new System.Drawing.Size(188, 368);
            this.listTicketTime.TabIndex = 1;
            this.listTicketTime.SelectedIndexChanged += new System.EventHandler(this.listTicketTime_SelectedIndexChanged);
            // 
            // groupTicketMovie
            // 
            this.groupTicketMovie.Controls.Add(this.listTicketMovie);
            this.groupTicketMovie.Location = new System.Drawing.Point(232, 71);
            this.groupTicketMovie.Name = "groupTicketMovie";
            this.groupTicketMovie.Size = new System.Drawing.Size(200, 400);
            this.groupTicketMovie.TabIndex = 1;
            this.groupTicketMovie.TabStop = false;
            this.groupTicketMovie.Text = "Movie";
            this.groupTicketMovie.Visible = false;
            // 
            // listTicketMovie
            // 
            this.listTicketMovie.FormattingEnabled = true;
            this.listTicketMovie.Location = new System.Drawing.Point(6, 16);
            this.listTicketMovie.Name = "listTicketMovie";
            this.listTicketMovie.Size = new System.Drawing.Size(188, 368);
            this.listTicketMovie.TabIndex = 1;
            this.listTicketMovie.SelectedIndexChanged += new System.EventHandler(this.listTicketMovie_SelectedIndexChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Trebuchet MS", 32F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(17, 14);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(253, 54);
            this.label23.TabIndex = 2;
            this.label23.Text = "Buy Tickets";
            // 
            // groupTicketTheatre
            // 
            this.groupTicketTheatre.Controls.Add(this.listTicketTheatre);
            this.groupTicketTheatre.Location = new System.Drawing.Point(26, 71);
            this.groupTicketTheatre.Name = "groupTicketTheatre";
            this.groupTicketTheatre.Size = new System.Drawing.Size(200, 400);
            this.groupTicketTheatre.TabIndex = 0;
            this.groupTicketTheatre.TabStop = false;
            this.groupTicketTheatre.Text = "Theatre";
            // 
            // listTicketTheatre
            // 
            this.listTicketTheatre.FormattingEnabled = true;
            this.listTicketTheatre.Location = new System.Drawing.Point(6, 19);
            this.listTicketTheatre.Name = "listTicketTheatre";
            this.listTicketTheatre.Size = new System.Drawing.Size(188, 368);
            this.listTicketTheatre.TabIndex = 0;
            this.listTicketTheatre.SelectedIndexChanged += new System.EventHandler(this.listTicketTheatre_SelectedIndexChanged);
            // 
            // panelTicketFogOfWar
            // 
            this.panelTicketFogOfWar.Controls.Add(this.btnTicketStart);
            this.panelTicketFogOfWar.Location = new System.Drawing.Point(26, 71);
            this.panelTicketFogOfWar.Name = "panelTicketFogOfWar";
            this.panelTicketFogOfWar.Size = new System.Drawing.Size(612, 400);
            this.panelTicketFogOfWar.TabIndex = 5;
            // 
            // btnTicketStart
            // 
            this.btnTicketStart.Location = new System.Drawing.Point(212, 173);
            this.btnTicketStart.Name = "btnTicketStart";
            this.btnTicketStart.Size = new System.Drawing.Size(193, 53);
            this.btnTicketStart.TabIndex = 0;
            this.btnTicketStart.Text = "Click here to buy tickets";
            this.btnTicketStart.UseVisualStyleBackColor = true;
            this.btnTicketStart.Click += new System.EventHandler(this.btnTicketStart_Click);
            // 
            // tabMyTickets
            // 
            this.tabMyTickets.Controls.Add(this.listMyTickets);
            this.tabMyTickets.Controls.Add(this.label46);
            this.tabMyTickets.Controls.Add(this.btnMyTicketsRefresh);
            this.tabMyTickets.Location = new System.Drawing.Point(4, 22);
            this.tabMyTickets.Name = "tabMyTickets";
            this.tabMyTickets.Padding = new System.Windows.Forms.Padding(3);
            this.tabMyTickets.Size = new System.Drawing.Size(924, 506);
            this.tabMyTickets.TabIndex = 10;
            this.tabMyTickets.Text = "My Tickets";
            this.tabMyTickets.UseVisualStyleBackColor = true;
            // 
            // listMyTickets
            // 
            this.listMyTickets.FormattingEnabled = true;
            this.listMyTickets.Location = new System.Drawing.Point(26, 100);
            this.listMyTickets.Name = "listMyTickets";
            this.listMyTickets.Size = new System.Drawing.Size(860, 368);
            this.listMyTickets.TabIndex = 4;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Trebuchet MS", 32F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(17, 14);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(234, 54);
            this.label46.TabIndex = 3;
            this.label46.Text = "My Tickets";
            // 
            // btnMyTicketsRefresh
            // 
            this.btnMyTicketsRefresh.Location = new System.Drawing.Point(26, 71);
            this.btnMyTicketsRefresh.Name = "btnMyTicketsRefresh";
            this.btnMyTicketsRefresh.Size = new System.Drawing.Size(125, 23);
            this.btnMyTicketsRefresh.TabIndex = 0;
            this.btnMyTicketsRefresh.Text = "Refresh Tickets";
            this.btnMyTicketsRefresh.UseVisualStyleBackColor = true;
            this.btnMyTicketsRefresh.Click += new System.EventHandler(this.btnMyTicketsRefresh_Click);
            // 
            // tabEmployeeManagement
            // 
            this.tabEmployeeManagement.Controls.Add(this.groupBox11);
            this.tabEmployeeManagement.Location = new System.Drawing.Point(4, 22);
            this.tabEmployeeManagement.Name = "tabEmployeeManagement";
            this.tabEmployeeManagement.Padding = new System.Windows.Forms.Padding(3);
            this.tabEmployeeManagement.Size = new System.Drawing.Size(924, 506);
            this.tabEmployeeManagement.TabIndex = 11;
            this.tabEmployeeManagement.Text = "Employee Management";
            this.tabEmployeeManagement.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.groupBox13);
            this.groupBox11.Controls.Add(this.groupBox12);
            this.groupBox11.Controls.Add(this.comboEmployeeSelect);
            this.groupBox11.Controls.Add(this.label48);
            this.groupBox11.Location = new System.Drawing.Point(8, 17);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(908, 354);
            this.groupBox11.TabIndex = 3;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Employees";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.label54);
            this.groupBox13.Controls.Add(this.comboJobSelect);
            this.groupBox13.Controls.Add(this.buttonSubmitSchedule);
            this.groupBox13.Controls.Add(this.panelCurrentSchedule);
            this.groupBox13.Controls.Add(this.label52);
            this.groupBox13.Controls.Add(this.label49);
            this.groupBox13.Controls.Add(this.comboSetEndCycle);
            this.groupBox13.Controls.Add(this.label61);
            this.groupBox13.Controls.Add(this.comboSetEndMinute);
            this.groupBox13.Controls.Add(this.comboSetEndHour);
            this.groupBox13.Controls.Add(this.label60);
            this.groupBox13.Controls.Add(this.comboSetStartCycle);
            this.groupBox13.Controls.Add(this.label59);
            this.groupBox13.Controls.Add(this.comboSetStartMinute);
            this.groupBox13.Controls.Add(this.label58);
            this.groupBox13.Controls.Add(this.comboSetStartHour);
            this.groupBox13.Controls.Add(this.label57);
            this.groupBox13.Controls.Add(this.comboTheatreSelect);
            this.groupBox13.Controls.Add(this.label56);
            this.groupBox13.Controls.Add(this.comboDaySelect);
            this.groupBox13.Location = new System.Drawing.Point(341, 65);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(561, 281);
            this.groupBox13.TabIndex = 8;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Schedule";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(236, 222);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(46, 13);
            this.label54.TabIndex = 25;
            this.label54.Text = "Set Job:";
            // 
            // comboJobSelect
            // 
            this.comboJobSelect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboJobSelect.FormattingEnabled = true;
            this.comboJobSelect.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.comboJobSelect.Location = new System.Drawing.Point(235, 240);
            this.comboJobSelect.Name = "comboJobSelect";
            this.comboJobSelect.Size = new System.Drawing.Size(59, 21);
            this.comboJobSelect.TabIndex = 24;
            // 
            // buttonSubmitSchedule
            // 
            this.buttonSubmitSchedule.Location = new System.Drawing.Point(459, 238);
            this.buttonSubmitSchedule.Name = "buttonSubmitSchedule";
            this.buttonSubmitSchedule.Size = new System.Drawing.Size(75, 23);
            this.buttonSubmitSchedule.TabIndex = 23;
            this.buttonSubmitSchedule.Text = "Submit Schedule";
            this.buttonSubmitSchedule.UseVisualStyleBackColor = true;
            this.buttonSubmitSchedule.Click += new System.EventHandler(this.buttonSubmitSchedule_Click);
            // 
            // panelCurrentSchedule
            // 
            this.panelCurrentSchedule.Controls.Add(this.labelScheduleJob);
            this.panelCurrentSchedule.Controls.Add(this.label62);
            this.panelCurrentSchedule.Controls.Add(this.labelScheduleTheatre);
            this.panelCurrentSchedule.Controls.Add(this.label70);
            this.panelCurrentSchedule.Controls.Add(this.labelScheduleTime);
            this.panelCurrentSchedule.Location = new System.Drawing.Point(9, 101);
            this.panelCurrentSchedule.Name = "panelCurrentSchedule";
            this.panelCurrentSchedule.Size = new System.Drawing.Size(546, 44);
            this.panelCurrentSchedule.TabIndex = 22;
            // 
            // labelScheduleJob
            // 
            this.labelScheduleJob.AutoSize = true;
            this.labelScheduleJob.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelScheduleJob.Location = new System.Drawing.Point(424, 11);
            this.labelScheduleJob.Name = "labelScheduleJob";
            this.labelScheduleJob.Size = new System.Drawing.Size(52, 16);
            this.labelScheduleJob.TabIndex = 31;
            this.labelScheduleJob.Text = "label71";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(354, 14);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(64, 13);
            this.label62.TabIndex = 30;
            this.label62.Text = "Doing Job";
            // 
            // labelScheduleTheatre
            // 
            this.labelScheduleTheatre.AutoSize = true;
            this.labelScheduleTheatre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelScheduleTheatre.Location = new System.Drawing.Point(222, 12);
            this.labelScheduleTheatre.Name = "labelScheduleTheatre";
            this.labelScheduleTheatre.Size = new System.Drawing.Size(52, 16);
            this.labelScheduleTheatre.TabIndex = 29;
            this.labelScheduleTheatre.Text = "label71";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(197, 14);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(19, 13);
            this.label70.TabIndex = 28;
            this.label70.Text = "At";
            // 
            // labelScheduleTime
            // 
            this.labelScheduleTime.AutoSize = true;
            this.labelScheduleTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelScheduleTime.Location = new System.Drawing.Point(8, 14);
            this.labelScheduleTime.Name = "labelScheduleTime";
            this.labelScheduleTime.Size = new System.Drawing.Size(52, 16);
            this.labelScheduleTime.TabIndex = 0;
            this.labelScheduleTime.Text = "label54";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(15, 112);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(145, 13);
            this.label52.TabIndex = 22;
            this.label52.Text = "No schedule for this day";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(6, 85);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(92, 13);
            this.label49.TabIndex = 9;
            this.label49.Text = "Current Schedule:";
            // 
            // comboSetEndCycle
            // 
            this.comboSetEndCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboSetEndCycle.FormattingEnabled = true;
            this.comboSetEndCycle.Items.AddRange(new object[] {
            "AM",
            "PM"});
            this.comboSetEndCycle.Location = new System.Drawing.Point(386, 192);
            this.comboSetEndCycle.Name = "comboSetEndCycle";
            this.comboSetEndCycle.Size = new System.Drawing.Size(41, 21);
            this.comboSetEndCycle.TabIndex = 21;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(304, 195);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(11, 13);
            this.label61.TabIndex = 20;
            this.label61.Text = ":";
            // 
            // comboSetEndMinute
            // 
            this.comboSetEndMinute.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboSetEndMinute.FormattingEnabled = true;
            this.comboSetEndMinute.Items.AddRange(new object[] {
            "00",
            "10",
            "20",
            "30",
            "40",
            "50"});
            this.comboSetEndMinute.Location = new System.Drawing.Point(321, 192);
            this.comboSetEndMinute.Name = "comboSetEndMinute";
            this.comboSetEndMinute.Size = new System.Drawing.Size(59, 21);
            this.comboSetEndMinute.TabIndex = 19;
            // 
            // comboSetEndHour
            // 
            this.comboSetEndHour.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboSetEndHour.FormattingEnabled = true;
            this.comboSetEndHour.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.comboSetEndHour.Location = new System.Drawing.Point(239, 191);
            this.comboSetEndHour.Name = "comboSetEndHour";
            this.comboSetEndHour.Size = new System.Drawing.Size(59, 21);
            this.comboSetEndHour.TabIndex = 18;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(206, 196);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(22, 13);
            this.label60.TabIndex = 17;
            this.label60.Text = "To";
            // 
            // comboSetStartCycle
            // 
            this.comboSetStartCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboSetStartCycle.FormattingEnabled = true;
            this.comboSetStartCycle.Items.AddRange(new object[] {
            "AM",
            "PM"});
            this.comboSetStartCycle.Location = new System.Drawing.Point(153, 191);
            this.comboSetStartCycle.Name = "comboSetStartCycle";
            this.comboSetStartCycle.Size = new System.Drawing.Size(41, 21);
            this.comboSetStartCycle.TabIndex = 16;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(71, 194);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(11, 13);
            this.label59.TabIndex = 15;
            this.label59.Text = ":";
            // 
            // comboSetStartMinute
            // 
            this.comboSetStartMinute.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboSetStartMinute.FormattingEnabled = true;
            this.comboSetStartMinute.Items.AddRange(new object[] {
            "00",
            "10",
            "20",
            "30",
            "40",
            "50"});
            this.comboSetStartMinute.Location = new System.Drawing.Point(88, 191);
            this.comboSetStartMinute.Name = "comboSetStartMinute";
            this.comboSetStartMinute.Size = new System.Drawing.Size(59, 21);
            this.comboSetStartMinute.TabIndex = 14;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(6, 172);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(52, 13);
            this.label58.TabIndex = 12;
            this.label58.Text = "Set Time:";
            // 
            // comboSetStartHour
            // 
            this.comboSetStartHour.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboSetStartHour.FormattingEnabled = true;
            this.comboSetStartHour.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.comboSetStartHour.Location = new System.Drawing.Point(6, 190);
            this.comboSetStartHour.Name = "comboSetStartHour";
            this.comboSetStartHour.Size = new System.Drawing.Size(59, 21);
            this.comboSetStartHour.TabIndex = 13;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(9, 222);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(66, 13);
            this.label57.TabIndex = 10;
            this.label57.Text = "Set Theatre:";
            // 
            // comboTheatreSelect
            // 
            this.comboTheatreSelect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboTheatreSelect.FormattingEnabled = true;
            this.comboTheatreSelect.Location = new System.Drawing.Point(9, 240);
            this.comboTheatreSelect.Name = "comboTheatreSelect";
            this.comboTheatreSelect.Size = new System.Drawing.Size(163, 21);
            this.comboTheatreSelect.TabIndex = 11;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(6, 28);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(62, 13);
            this.label56.TabIndex = 9;
            this.label56.Text = "Select Day:";
            // 
            // comboDaySelect
            // 
            this.comboDaySelect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboDaySelect.FormattingEnabled = true;
            this.comboDaySelect.Items.AddRange(new object[] {
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
            "Sunday"});
            this.comboDaySelect.Location = new System.Drawing.Point(6, 46);
            this.comboDaySelect.Name = "comboDaySelect";
            this.comboDaySelect.Size = new System.Drawing.Size(163, 21);
            this.comboDaySelect.TabIndex = 9;
            this.comboDaySelect.SelectedIndexChanged += new System.EventHandler(this.employeeDayTheatre_SelectedIndexChanged);
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.labelEmployeeSSN);
            this.groupBox12.Controls.Add(this.label55);
            this.groupBox12.Controls.Add(this.labelEmployeePhone);
            this.groupBox12.Controls.Add(this.label53);
            this.groupBox12.Controls.Add(this.labelEmployeeAddress);
            this.groupBox12.Controls.Add(this.label51);
            this.groupBox12.Controls.Add(this.labelEmployeeName);
            this.groupBox12.Controls.Add(this.label50);
            this.groupBox12.Location = new System.Drawing.Point(6, 65);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(329, 281);
            this.groupBox12.TabIndex = 7;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Information";
            // 
            // labelEmployeeSSN
            // 
            this.labelEmployeeSSN.AutoSize = true;
            this.labelEmployeeSSN.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEmployeeSSN.Location = new System.Drawing.Point(222, 106);
            this.labelEmployeeSSN.Name = "labelEmployeeSSN";
            this.labelEmployeeSSN.Size = new System.Drawing.Size(50, 22);
            this.labelEmployeeSSN.TabIndex = 9;
            this.labelEmployeeSSN.Text = "        ";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(173, 106);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(40, 17);
            this.label55.TabIndex = 8;
            this.label55.Text = "SSN:";
            // 
            // labelEmployeePhone
            // 
            this.labelEmployeePhone.AutoSize = true;
            this.labelEmployeePhone.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEmployeePhone.Location = new System.Drawing.Point(70, 187);
            this.labelEmployeePhone.Name = "labelEmployeePhone";
            this.labelEmployeePhone.Size = new System.Drawing.Size(50, 22);
            this.labelEmployeePhone.TabIndex = 7;
            this.labelEmployeePhone.Text = "        ";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(6, 187);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(53, 17);
            this.label53.TabIndex = 6;
            this.label53.Text = "Phone:";
            // 
            // labelEmployeeAddress
            // 
            this.labelEmployeeAddress.AutoSize = true;
            this.labelEmployeeAddress.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEmployeeAddress.Location = new System.Drawing.Point(70, 105);
            this.labelEmployeeAddress.Name = "labelEmployeeAddress";
            this.labelEmployeeAddress.Size = new System.Drawing.Size(50, 22);
            this.labelEmployeeAddress.TabIndex = 5;
            this.labelEmployeeAddress.Text = "        ";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(6, 105);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(64, 17);
            this.label51.TabIndex = 4;
            this.label51.Text = "Address:";
            // 
            // labelEmployeeName
            // 
            this.labelEmployeeName.AutoSize = true;
            this.labelEmployeeName.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEmployeeName.Location = new System.Drawing.Point(70, 28);
            this.labelEmployeeName.Name = "labelEmployeeName";
            this.labelEmployeeName.Size = new System.Drawing.Size(50, 22);
            this.labelEmployeeName.TabIndex = 3;
            this.labelEmployeeName.Text = "        ";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(6, 28);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(49, 17);
            this.label50.TabIndex = 2;
            this.label50.Text = "Name:";
            // 
            // comboEmployeeSelect
            // 
            this.comboEmployeeSelect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboEmployeeSelect.FormattingEnabled = true;
            this.comboEmployeeSelect.Location = new System.Drawing.Point(6, 38);
            this.comboEmployeeSelect.Name = "comboEmployeeSelect";
            this.comboEmployeeSelect.Size = new System.Drawing.Size(163, 21);
            this.comboEmployeeSelect.TabIndex = 6;
            this.comboEmployeeSelect.SelectedIndexChanged += new System.EventHandler(this.comboEmployeeSelect_SelectedIndexChanged);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(6, 22);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(89, 13);
            this.label48.TabIndex = 4;
            this.label48.Text = "Select Employee:";
            // 
            // tabTheatreManagement
            // 
            this.tabTheatreManagement.Controls.Add(this.groupBox14);
            this.tabTheatreManagement.Location = new System.Drawing.Point(4, 22);
            this.tabTheatreManagement.Name = "tabTheatreManagement";
            this.tabTheatreManagement.Padding = new System.Windows.Forms.Padding(3);
            this.tabTheatreManagement.Size = new System.Drawing.Size(924, 506);
            this.tabTheatreManagement.TabIndex = 12;
            this.tabTheatreManagement.Text = "Theatre Management";
            this.tabTheatreManagement.UseVisualStyleBackColor = true;
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.groupBox15);
            this.groupBox14.Controls.Add(this.groupBox16);
            this.groupBox14.Controls.Add(this.comboSelectTheatreForUpdate);
            this.groupBox14.Controls.Add(this.label88);
            this.groupBox14.Location = new System.Drawing.Point(3, 6);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(908, 354);
            this.groupBox14.TabIndex = 4;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Employees";
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.dataShowTimes);
            this.groupBox15.Controls.Add(this.button1);
            this.groupBox15.Controls.Add(this.comboMovieEndCycle);
            this.groupBox15.Controls.Add(this.label74);
            this.groupBox15.Controls.Add(this.comboMovieEndMinute);
            this.groupBox15.Controls.Add(this.comboMovieEndHour);
            this.groupBox15.Controls.Add(this.label75);
            this.groupBox15.Controls.Add(this.comboMovieStartCycle);
            this.groupBox15.Controls.Add(this.label76);
            this.groupBox15.Controls.Add(this.comboMovieStartMinute);
            this.groupBox15.Controls.Add(this.label77);
            this.groupBox15.Controls.Add(this.comboMovieStartHour);
            this.groupBox15.Controls.Add(this.label78);
            this.groupBox15.Controls.Add(this.comboSelectRoom);
            this.groupBox15.Controls.Add(this.label79);
            this.groupBox15.Controls.Add(this.comboSelectMovie);
            this.groupBox15.Location = new System.Drawing.Point(186, 65);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(716, 281);
            this.groupBox15.TabIndex = 8;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Schedule";
            // 
            // dataShowTimes
            // 
            this.dataShowTimes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataShowTimes.Location = new System.Drawing.Point(6, 73);
            this.dataShowTimes.Name = "dataShowTimes";
            this.dataShowTimes.Size = new System.Drawing.Size(701, 96);
            this.dataShowTimes.TabIndex = 26;
            this.dataShowTimes.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataShowTimes_CellValueChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 228);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 23;
            this.button1.Text = "Submit Schedule";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.buttonAddShowTime_Click);
            // 
            // comboMovieEndCycle
            // 
            this.comboMovieEndCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboMovieEndCycle.FormattingEnabled = true;
            this.comboMovieEndCycle.Items.AddRange(new object[] {
            "AM",
            "PM"});
            this.comboMovieEndCycle.Location = new System.Drawing.Point(386, 192);
            this.comboMovieEndCycle.Name = "comboMovieEndCycle";
            this.comboMovieEndCycle.Size = new System.Drawing.Size(41, 21);
            this.comboMovieEndCycle.TabIndex = 21;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.Location = new System.Drawing.Point(304, 195);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(11, 13);
            this.label74.TabIndex = 20;
            this.label74.Text = ":";
            // 
            // comboMovieEndMinute
            // 
            this.comboMovieEndMinute.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboMovieEndMinute.FormattingEnabled = true;
            this.comboMovieEndMinute.Items.AddRange(new object[] {
            "00",
            "10",
            "20",
            "30",
            "40",
            "50"});
            this.comboMovieEndMinute.Location = new System.Drawing.Point(321, 192);
            this.comboMovieEndMinute.Name = "comboMovieEndMinute";
            this.comboMovieEndMinute.Size = new System.Drawing.Size(59, 21);
            this.comboMovieEndMinute.TabIndex = 19;
            // 
            // comboMovieEndHour
            // 
            this.comboMovieEndHour.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboMovieEndHour.FormattingEnabled = true;
            this.comboMovieEndHour.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.comboMovieEndHour.Location = new System.Drawing.Point(239, 191);
            this.comboMovieEndHour.Name = "comboMovieEndHour";
            this.comboMovieEndHour.Size = new System.Drawing.Size(59, 21);
            this.comboMovieEndHour.TabIndex = 18;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.Location = new System.Drawing.Point(206, 196);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(22, 13);
            this.label75.TabIndex = 17;
            this.label75.Text = "To";
            // 
            // comboMovieStartCycle
            // 
            this.comboMovieStartCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboMovieStartCycle.FormattingEnabled = true;
            this.comboMovieStartCycle.Items.AddRange(new object[] {
            "AM",
            "PM"});
            this.comboMovieStartCycle.Location = new System.Drawing.Point(153, 191);
            this.comboMovieStartCycle.Name = "comboMovieStartCycle";
            this.comboMovieStartCycle.Size = new System.Drawing.Size(41, 21);
            this.comboMovieStartCycle.TabIndex = 16;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.Location = new System.Drawing.Point(71, 194);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(11, 13);
            this.label76.TabIndex = 15;
            this.label76.Text = ":";
            // 
            // comboMovieStartMinute
            // 
            this.comboMovieStartMinute.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboMovieStartMinute.FormattingEnabled = true;
            this.comboMovieStartMinute.Items.AddRange(new object[] {
            "00",
            "10",
            "20",
            "30",
            "40",
            "50"});
            this.comboMovieStartMinute.Location = new System.Drawing.Point(88, 191);
            this.comboMovieStartMinute.Name = "comboMovieStartMinute";
            this.comboMovieStartMinute.Size = new System.Drawing.Size(59, 21);
            this.comboMovieStartMinute.TabIndex = 14;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(6, 172);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(52, 13);
            this.label77.TabIndex = 12;
            this.label77.Text = "Set Time:";
            // 
            // comboMovieStartHour
            // 
            this.comboMovieStartHour.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboMovieStartHour.FormattingEnabled = true;
            this.comboMovieStartHour.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.comboMovieStartHour.Location = new System.Drawing.Point(6, 190);
            this.comboMovieStartHour.Name = "comboMovieStartHour";
            this.comboMovieStartHour.Size = new System.Drawing.Size(59, 21);
            this.comboMovieStartHour.TabIndex = 13;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(456, 176);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(57, 13);
            this.label78.TabIndex = 10;
            this.label78.Text = "Set Room:";
            // 
            // comboSelectRoom
            // 
            this.comboSelectRoom.AutoCompleteCustomSource.AddRange(new string[] {
            "0",
            "1",
            "2",
            "3",
            "4"});
            this.comboSelectRoom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboSelectRoom.FormattingEnabled = true;
            this.comboSelectRoom.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4"});
            this.comboSelectRoom.Location = new System.Drawing.Point(459, 192);
            this.comboSelectRoom.Name = "comboSelectRoom";
            this.comboSelectRoom.Size = new System.Drawing.Size(163, 21);
            this.comboSelectRoom.TabIndex = 11;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(6, 28);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(72, 13);
            this.label79.TabIndex = 9;
            this.label79.Text = "Select Movie:";
            // 
            // comboSelectMovie
            // 
            this.comboSelectMovie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboSelectMovie.FormattingEnabled = true;
            this.comboSelectMovie.Items.AddRange(new object[] {
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
            "Sunday"});
            this.comboSelectMovie.Location = new System.Drawing.Point(6, 46);
            this.comboSelectMovie.Name = "comboSelectMovie";
            this.comboSelectMovie.Size = new System.Drawing.Size(163, 21);
            this.comboSelectMovie.TabIndex = 9;
            this.comboSelectMovie.SelectedIndexChanged += new System.EventHandler(this.comboSelectMovie_SelectedIndexChanged);
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.labelTheatreAddress);
            this.groupBox16.Controls.Add(this.label85);
            this.groupBox16.Controls.Add(this.labelTheatreName);
            this.groupBox16.Controls.Add(this.label87);
            this.groupBox16.Location = new System.Drawing.Point(6, 65);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(174, 281);
            this.groupBox16.TabIndex = 7;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Show Times";
            // 
            // labelTheatreAddress
            // 
            this.labelTheatreAddress.AutoSize = true;
            this.labelTheatreAddress.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTheatreAddress.Location = new System.Drawing.Point(5, 123);
            this.labelTheatreAddress.Name = "labelTheatreAddress";
            this.labelTheatreAddress.Size = new System.Drawing.Size(50, 22);
            this.labelTheatreAddress.TabIndex = 5;
            this.labelTheatreAddress.Text = "        ";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.Location = new System.Drawing.Point(6, 105);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(64, 17);
            this.label85.TabIndex = 4;
            this.label85.Text = "Address:";
            // 
            // labelTheatreName
            // 
            this.labelTheatreName.AutoSize = true;
            this.labelTheatreName.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTheatreName.Location = new System.Drawing.Point(70, 28);
            this.labelTheatreName.Name = "labelTheatreName";
            this.labelTheatreName.Size = new System.Drawing.Size(50, 22);
            this.labelTheatreName.TabIndex = 3;
            this.labelTheatreName.Text = "        ";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.Location = new System.Drawing.Point(6, 28);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(49, 17);
            this.label87.TabIndex = 2;
            this.label87.Text = "Name:";
            // 
            // comboSelectTheatreForUpdate
            // 
            this.comboSelectTheatreForUpdate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboSelectTheatreForUpdate.FormattingEnabled = true;
            this.comboSelectTheatreForUpdate.Location = new System.Drawing.Point(6, 38);
            this.comboSelectTheatreForUpdate.Name = "comboSelectTheatreForUpdate";
            this.comboSelectTheatreForUpdate.Size = new System.Drawing.Size(163, 21);
            this.comboSelectTheatreForUpdate.TabIndex = 6;
            this.comboSelectTheatreForUpdate.SelectedIndexChanged += new System.EventHandler(this.comboSelectTheatreForUpdate_SelectedIndexChanged);
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(6, 22);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(80, 13);
            this.label88.TabIndex = 4;
            this.label88.Text = "Select Theatre:";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(932, 532);
            this.Controls.Add(this.mainBottomStrip);
            this.Controls.Add(this.tabMainControl);
            this.Name = "Main";
            this.Text = "(app name)";
            this.Load += new System.EventHandler(this.Main_Load);
            this.mainBottomStrip.ResumeLayout(false);
            this.mainBottomStrip.PerformLayout();
            this.tabAllTables.ResumeLayout(false);
            this.tabMainMyAccount.ResumeLayout(false);
            this.tabMainMyAccount.PerformLayout();
            this.groupBoxEmployeeInfo.ResumeLayout(false);
            this.groupBoxEmployeeInfo.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.tabMainOptions.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabMainHome.ResumeLayout(false);
            this.tabMainHome.PerformLayout();
            this.panelHomeRegister.ResumeLayout(false);
            this.panelHomeRegister.PerformLayout();
            this.panelHomeLogin.ResumeLayout(false);
            this.panelHomeLogin.PerformLayout();
            this.panelHomeDefault.ResumeLayout(false);
            this.panelHomeDefault.PerformLayout();
            this.panelHomeLoggedIn.ResumeLayout(false);
            this.panelHomeLoggedIn.PerformLayout();
            this.tabMainControl.ResumeLayout(false);
            this.tabReviews.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupReviewAddBox.ResumeLayout(false);
            this.groupReviewAddBox.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.tabReviewControl.ResumeLayout(false);
            this.panelReviewTheatres.ResumeLayout(false);
            this.panelReviewMovies.ResumeLayout(false);
            this.tabQueries.ResumeLayout(false);
            this.tabQueries.PerformLayout();
            this.tabTickets.ResumeLayout(false);
            this.tabTickets.PerformLayout();
            this.panelTicketBillingInfo.ResumeLayout(false);
            this.panelTicketBillingInfo.PerformLayout();
            this.groupTicketTime.ResumeLayout(false);
            this.groupTicketMovie.ResumeLayout(false);
            this.groupTicketTheatre.ResumeLayout(false);
            this.panelTicketFogOfWar.ResumeLayout(false);
            this.tabMyTickets.ResumeLayout(false);
            this.tabMyTickets.PerformLayout();
            this.tabEmployeeManagement.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.panelCurrentSchedule.ResumeLayout(false);
            this.panelCurrentSchedule.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.tabTheatreManagement.ResumeLayout(false);
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataShowTimes)).EndInit();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.StatusStrip mainBottomStrip;
        private System.Windows.Forms.ToolStripStatusLabel textAppName;
        private System.Windows.Forms.ToolStripStatusLabel textAppVersionStatusBar;
        private System.Windows.Forms.Timer timerDBConnector;
        private System.Windows.Forms.ToolStripProgressBar progressBar;
        private System.Windows.Forms.ToolStripStatusLabel textProgressLabel;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem forceReconnectionToolStripMenuItem;
        private System.Windows.Forms.TabPage tabAllTables;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabMainMyAccount;
        private System.Windows.Forms.GroupBox groupBoxEmployeeInfo;
        private System.Windows.Forms.Label labelSSN;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button buttonConfirmUpdateInfo;
        private System.Windows.Forms.TextBox textBoxChangeAddress;
        private System.Windows.Forms.TextBox textBoxChangePhoneNum;
        private System.Windows.Forms.TextBox textBoxChangeName;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelPhone;
        private System.Windows.Forms.Label labelAddress;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label labelEmail;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Button buttonUpdateInfo;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label labelRank;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label labelPoints;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button updatePassword;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textNewPassword;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textOldPassword;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabPage tabMainOptions;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox listDebugLog;
        private System.Windows.Forms.CheckBox checkSettingsDebugLog;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSaveDBCredentials;
        private System.Windows.Forms.CheckBox checkSettingsAutoConnect;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textSettingsDBPassword;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textSettingsDBUsername;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textSettingsReconnectionDelay;
        private System.Windows.Forms.CheckBox checkSettingsAutoReconnect;
        private System.Windows.Forms.TabPage tabMainHome;
        private System.Windows.Forms.Panel panelHomeLoggedIn;
        private System.Windows.Forms.Button buttonLogOut;
        private System.Windows.Forms.Label labelLoggedIn;
        private System.Windows.Forms.Panel panelHomeRegister;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxRegisterFullName;
        private System.Windows.Forms.TextBox textBoxRegisterPhoneNumber;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox textBoxRegisterAddress;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBoxRegisterEmail;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button btnRegisterLogin;
        private System.Windows.Forms.Button btnRegisterCancel;
        private System.Windows.Forms.Button btnRegisterRegister;
        private System.Windows.Forms.TextBox textBoxRegisterPassword;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxRegisterUsername;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Panel panelHomeLogin;
        private System.Windows.Forms.Button btnLoginRegister;
        private System.Windows.Forms.Button btnLoginCancel;
        private System.Windows.Forms.Button btnLoginLogin;
        private System.Windows.Forms.TextBox textLoginPassword;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textLoginUsername;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panelHomeDefault;
        private System.Windows.Forms.Button btnHomeLogin;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnHomeRegister;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabMainControl;
        private System.Windows.Forms.FlowLayoutPanel containerAllTables;
        private System.Windows.Forms.TabPage tabReviews;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TabControl tabReviewControl;
        private System.Windows.Forms.TabPage panelReviewTheatres;
        private System.Windows.Forms.ListBox listReviewTheatres;
        private System.Windows.Forms.TabPage panelReviewMovies;
        private System.Windows.Forms.Button btnReviewsRefreshTheatres;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label labelReviewDiscussion;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupReviewAddBox;
        private System.Windows.Forms.ListBox listReviewData;
        private System.Windows.Forms.Button btnReviewsRefreshMovies;
        private System.Windows.Forms.ListBox listReviewMovies;
        private System.Windows.Forms.Button btnThreadReply;
        private System.Windows.Forms.TextBox textReviewReplyBox;
        private System.Windows.Forms.Button btnAccountRefreshPoints;
        private System.Windows.Forms.ComboBox comboReviewTarget;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnReviewCreateThread;
        private System.Windows.Forms.TextBox textReviewAddMessage;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textReviewAddTitle;
        private System.Windows.Forms.RadioButton radioReviewMovie;
        private System.Windows.Forms.RadioButton radioReviewTheatre;
        private System.Windows.Forms.TabPage tabQueries;
        private System.Windows.Forms.Label Query1;
        private System.Windows.Forms.ComboBox comboBoxQuery1;
        private System.Windows.Forms.ListBox listBoxQuery1;
        private System.Windows.Forms.Button buttonQuery1;
        private System.Windows.Forms.Button buttonQuery2;
        private System.Windows.Forms.ListBox listBoxQuery2;
        private System.Windows.Forms.Label labelQuery2;
        private System.Windows.Forms.TextBox textBoxRegisterExpDate;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBoxRegisterCreditCardNum;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ListBox listBoxQuery3;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button buttonQuery3;
        private System.Windows.Forms.Button buttonQuery5;
        private System.Windows.Forms.ListBox listBoxQuery5;
        private System.Windows.Forms.Label labelQuery5;
        private System.Windows.Forms.TabPage tabSchedule;
        private System.Windows.Forms.TabPage tabTickets;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.GroupBox groupTicketTheatre;
        private System.Windows.Forms.GroupBox groupTicketTime;
        private System.Windows.Forms.GroupBox groupTicketMovie;
        private System.Windows.Forms.ListBox listTicketTime;
        private System.Windows.Forms.ListBox listTicketMovie;
        private System.Windows.Forms.ListBox listTicketTheatre;
        private System.Windows.Forms.Button btnTicketReset;
        private System.Windows.Forms.Panel panelTicketFogOfWar;
        private System.Windows.Forms.Button btnTicketStart;
        private System.Windows.Forms.ListBox listTicketMovieIDHidden;
        private System.Windows.Forms.Label labelTicketMustRegister;
        private System.Windows.Forms.TextBox textTicketCCExp;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox textTicketCC;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textTicketFullName;
        private System.Windows.Forms.TextBox textTicketPhone;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox textTicketAddress;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox textTicketEmail;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Button btnTicketRegisterAndBuy;
        private System.Windows.Forms.TextBox textTicketPassword;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox textTicketUsername;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Button btnTicketBuy;
        private System.Windows.Forms.Panel panelTicketBillingInfo;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.ComboBox comboRegisterCCType;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.ComboBox textTicketCCType;
        private System.Windows.Forms.ListBox listTicketTimeIDHidden;
        private System.Windows.Forms.Label labelTicketSeats;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TabPage tabMyTickets;
        private System.Windows.Forms.Button btnMyTicketsRefresh;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.ListBox listMyTickets;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TabPage tabEmployeeManagement;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.ComboBox comboEmployeeSelect;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Label labelEmployeeName;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.ComboBox comboDaySelect;
        private System.Windows.Forms.Label labelEmployeeSSN;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label labelEmployeePhone;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label labelEmployeeAddress;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.ComboBox comboSetEndCycle;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.ComboBox comboSetEndMinute;
        private System.Windows.Forms.ComboBox comboSetEndHour;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.ComboBox comboSetStartCycle;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.ComboBox comboSetStartMinute;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.ComboBox comboSetStartHour;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.ComboBox comboTheatreSelect;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Panel panelCurrentSchedule;
        private System.Windows.Forms.Label labelScheduleTheatre;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label labelScheduleTime;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Button buttonQuery6;
        private System.Windows.Forms.ListBox listBoxQuery6;
        private System.Windows.Forms.Label labelQuery6;
        private System.Windows.Forms.Button buttonQuery7;
        private System.Windows.Forms.ListBox listBoxQuery7;
        private System.Windows.Forms.Label labelQuery7;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.ComboBox comboJobSelect;
        private System.Windows.Forms.Button buttonSubmitSchedule;
        private System.Windows.Forms.Label labelScheduleJob;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.ComboBox comboBoxQuery8;
        private System.Windows.Forms.Button buttonQuery8;
        private System.Windows.Forms.ListBox listBoxQuery8;
        private System.Windows.Forms.Label labelQuery8;
        private System.Windows.Forms.Button btnReviewDownvote;
        private System.Windows.Forms.Button btnReviewUpvote;
        private System.Windows.Forms.Label labelReviewRating;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label labelReviewAlreadyVoted;
        private System.Windows.Forms.Label labelReviewOwner;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.TabPage tabTheatreManagement;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox comboMovieEndCycle;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.ComboBox comboMovieEndMinute;
        private System.Windows.Forms.ComboBox comboMovieEndHour;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.ComboBox comboMovieStartCycle;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.ComboBox comboMovieStartMinute;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.ComboBox comboMovieStartHour;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.ComboBox comboSelectRoom;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.ComboBox comboSelectMovie;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.Label labelTheatreAddress;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label labelTheatreName;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.ComboBox comboSelectTheatreForUpdate;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.DataGridView dataShowTimes;
    }
}

