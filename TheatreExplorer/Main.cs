﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.ManagedDataAccess.Client;

namespace TheatreExplorer
{
    public partial class Main : Form
    {
        bool is_logging = false;
        bool db_connected = false;
        string connection_str;
        public OracleConnection conn;
        TheatreExplorer.fDB fDB;

        //current info
        UserInfo userInfo = new UserInfo();
        UserInfo selectedEmployee;


        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            // Load settings
            checkSettingsAutoConnect.Checked = TheatreExplorer.Properties.Settings.Default.propAutoConnect;
            checkSettingsAutoReconnect.Checked = TheatreExplorer.Properties.Settings.Default.propReconnect;
            textSettingsReconnectionDelay.Text = TheatreExplorer.Properties.Settings.Default.propReconnectDelay.ToString();
            //textSettingsDBUsername.Text = TheatreExplorer.Properties.Settings.Default.propUsername;
            //textSettingsDBPassword.Text = TheatreExplorer.Properties.Settings.Default.propPassword;

            textSettingsDBUsername.Text = "apate131";
            textSettingsDBPassword.Text = "YCsBUo3...uK3Y15";

            checkSettingsDebugLog.Checked = TheatreExplorer.Properties.Settings.Default.propDebugLog;
            if (checkSettingsDebugLog.Checked)
            {
                is_logging = true;
            }
            debugLog("[Settings] Loaded settings.");
            textProgressLabel.Text = "Loaded settings";
            progressBar.Value = 20;

            // DB stuff.
            timerDBConnector.Interval = TheatreExplorer.Properties.Settings.Default.propReconnectDelay * 1000;
            if (checkSettingsAutoConnect.Checked)
            {
                textProgressLabel.Text = "Ready to connect to database...";
                timerDBConnector.Start();
            }

            // Initialize user interface
            this.Text = TheatreExplorer.Properties.Resources.resApplicationName;
            textAppName.Text = TheatreExplorer.Properties.Resources.resApplicationName;
            textAppVersionStatusBar.Text = TheatreExplorer.Properties.Resources.resApplicationVersion;
            // Hide tabs and panels
            switchUImode(AccessLevel.guest);
            switchHomePanelUImode(HomeUIMode.prompt);
            // set guest credentials
            userInfo.accessLevel = AccessLevel.guest;

            panelCurrentSchedule.Hide();

            debugLog("[UI] Finished loading interface.");
        }

        // remember settings

        private void btnSaveDBCredentials_Click(object sender, EventArgs e)
        {
            TheatreExplorer.Properties.Settings.Default.propAutoConnect = checkSettingsAutoConnect.Checked;
            TheatreExplorer.Properties.Settings.Default.propReconnect = checkSettingsAutoReconnect.Checked;
            TheatreExplorer.Properties.Settings.Default.propReconnectDelay = Convert.ToInt32(textSettingsReconnectionDelay.Text);
            TheatreExplorer.Properties.Settings.Default.propUsername = textSettingsDBUsername.Text;
            TheatreExplorer.Properties.Settings.Default.propPassword = textSettingsDBPassword.Text;
            TheatreExplorer.Properties.Settings.Default.propDebugLog = checkSettingsDebugLog.Checked;
            TheatreExplorer.Properties.Settings.Default.Save();
            debugLog("[Settings] Saved settings.");
        }

        private void debugLog(string text, bool displayError = false)
        {
            if (is_logging)
            {
                listDebugLog.Items.Add(text);
                listDebugLog.SelectedIndex = listDebugLog.Items.Count - 1;
            }
            if (displayError)
            {
                textProgressLabel.Text = text;
            }
        }

        public void updateMyAccountTab()
        {
            userInfo = fDB.GetUserInfo(userInfo.username); // reupdate

            labelPoints.Text = Convert.ToString(userInfo.credits);
            labelRank.Text = userInfo.rank;
            labelName.Text = userInfo.name;
            labelEmail.Text = userInfo.email;
            labelPhone.Text = userInfo.phoneNum;
            labelAddress.Text = userInfo.address;

            if (!userInfo.isEmployee)
            {
                groupBoxEmployeeInfo.Hide();
            }
            else
            {
                //employee info
                groupBoxEmployeeInfo.Show();
                labelSSN.Text = Convert.ToString(userInfo.employeeInfo.ssn);
            }

            buttonUpdateInfo.Show();
            buttonConfirmUpdateInfo.Hide();

            textBoxChangeName.Hide();
            textBoxChangePhoneNum.Hide();
            textBoxChangeAddress.Hide();
        }

        private void timerDBConnector_Tick(object sender, EventArgs e)
        {
            if (db_connected)
            {
                timerDBConnector.Stop();
            }
            else
            {
                // safety checks
                if (timerDBConnector.Interval < 1000)
                {
                    debugLog("Invalid interval (less than one second).");
                    timerDBConnector.Stop();
                    return;
                }
                if (textSettingsDBUsername.Text.Equals("username"))
                {
                    debugLog("Please change the username in the settings before retrying.", true);
                    timerDBConnector.Stop();
                    return;
                }
                // end safety checks
                connection_str = "user id=" + textSettingsDBUsername.Text + ";password=" + textSettingsDBPassword.Text + ";data source=fourier";
                conn = new OracleConnection(connection_str);
                debugLog("Attempting to connect to DB using username '" + textSettingsDBUsername.Text + "'", true);
                progressBar.Value = 60;
                try
                {
                    conn.Open();
                    progressBar.Value = 100;
                    debugLog("Connected to database.", true);
                    timerDBConnector.Stop();
                    fDB = new fDB(conn);
                    db_connected = true;
                }
                catch (Exception err)
                {
                    progressBar.Value = 0;
                    debugLog("[ERROR] Failed to connect to database.");
                    debugLog("[ERROR] " + err.Message);
                    debugLog("Trying again in " + textSettingsReconnectionDelay.Text + " seconds...");
                    textProgressLabel.Text = "Could not connect to database, retrying...";
                }
            }
        }

        private void btnHomeLogin_Click(object sender, EventArgs e)
        {
            switchHomePanelUImode(HomeUIMode.login);
        }

        // UI mode
        private void switchUImode(AccessLevel mode)
        {
            // Hide all tabs (for now)
            while (tabMainControl.TabCount > 0)
            {
                tabMainControl.TabPages.RemoveAt(0);
            }

            switch (mode)
            {
                case AccessLevel.normal:
                    tabMainControl.TabPages.Insert(0, tabMainHome);
                    tabMainControl.TabPages.Insert(1, tabMainMyAccount);
                    tabMainControl.TabPages.Insert(2, tabMainOptions);
                    tabMainControl.TabPages.Insert(3, tabReviews);
                    tabMainControl.TabPages.Insert(4, tabTickets);
                    tabMainControl.TabPages.Insert(5, tabMyTickets);
                    tabMainControl.TabPages.Insert(6, tabQueries);
                    break;
                case AccessLevel.employee:
                    tabMainControl.TabPages.Insert(0, tabMainHome);
                    tabMainControl.TabPages.Insert(1, tabMainMyAccount);
                    tabMainControl.TabPages.Insert(2, tabMainOptions);
                    break;
                case AccessLevel.manager:
                    tabMainControl.TabPages.Insert(0, tabMainHome);
                    tabMainControl.TabPages.Insert(1, tabMainMyAccount);
                    tabMainControl.TabPages.Insert(2, tabMainOptions);
                    tabMainControl.TabPages.Insert(3, tabQueries);
                    tabMainControl.TabPages.Insert(4, tabEmployeeManagement);
                    break;
                case AccessLevel.manager_withaccess:
                    tabMainControl.TabPages.Insert(0, tabMainHome);
                    tabMainControl.TabPages.Insert(1, tabMainMyAccount);
                    tabMainControl.TabPages.Insert(2, tabMainOptions);
                    tabMainControl.TabPages.Insert(3, tabTheatreManagement);
                    tabMainControl.TabPages.Insert(4, tabQueries);
                    tabMainControl.TabPages.Insert(5, tabEmployeeManagement);
                    break;
                case AccessLevel.owner:
                    tabMainControl.TabPages.Insert(0, tabMainHome);
                    tabMainControl.TabPages.Insert(1, tabMainMyAccount);
                    tabMainControl.TabPages.Insert(2, tabMainOptions);
                    tabMainControl.TabPages.Insert(3, tabEmployeeManagement);
                    tabMainControl.TabPages.Insert(4, tabAllTables);
                    tabMainControl.TabPages.Insert(5, tabQueries);
                    tabMainControl.TabPages.Insert(6, tabReviews);
                    break;
                default:
                case AccessLevel.guest:
                    tabMainControl.TabPages.Insert(0, tabMainHome);
                    tabMainControl.TabPages.Insert(1, tabMainOptions);
                    tabMainControl.TabPages.Insert(2, tabReviews);
                    tabMainControl.TabPages.Insert(3, tabTickets);
                    break;
            }

            switch (mode)
            {
                case AccessLevel.manager:
                case AccessLevel.manager_withaccess:
                case AccessLevel.owner:
                    fDB.CheckSecurityStatus();
                    break;
                case AccessLevel.guest:
                    groupReviewAddBox.Visible = false; // hide "Add Review" box
                    textReviewReplyBox.Visible = false;
                    btnThreadReply.Visible = false;
                    break;
                default:
                    groupReviewAddBox.Visible = true; // unhide "Add Review" box
                    textReviewReplyBox.Visible = true;
                    btnThreadReply.Visible = true;
                    break;
            }

        }

        private void switchHomePanelUImode(HomeUIMode stat)
        {
            // Hide all panels (for now)
            panelHomeDefault.Hide();
            panelHomeLogin.Hide();
            panelHomeRegister.Hide();
            panelHomeLoggedIn.Hide();

            switch (stat)
            {
                case HomeUIMode.login:
                    panelHomeLogin.Show();
                    break;
                case HomeUIMode.register:
                    panelHomeRegister.Show();
                    break;
                case HomeUIMode.loggedin:
                    panelHomeLoggedIn.Show();
                    panelHomeLoggedIn.BringToFront();
                    break;
                default:
                case HomeUIMode.prompt:
                    panelHomeDefault.Show();
                    break;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            switchHomePanelUImode(HomeUIMode.prompt);
        }

        private void btnHomeRegister_Click(object sender, EventArgs e)
        {
            switchHomePanelUImode(HomeUIMode.register);
        }

        private void btnLoginRegister_Click(object sender, EventArgs e)
        {
            switchHomePanelUImode(HomeUIMode.register);
        }

        private void btnRegisterRegister_Click(object sender, EventArgs e)
        {
            bool enteredInfo = true;
            String username = textBoxRegisterUsername.Text;
            String password = textBoxRegisterPassword.Text;
            String email = textBoxRegisterEmail.Text;
            String address = textBoxRegisterAddress.Text;
            String phone = textBoxRegisterPhoneNumber.Text;
            String fullname = textBoxRegisterFullName.Text;
            String cc_type = comboRegisterCCType.Text;
            String creditcard = textBoxRegisterCreditCardNum.Text;
            String expdate = textBoxRegisterExpDate.Text;
            String[] registerInfo = { username, password, email, address, phone, fullname, cc_type, creditcard, expdate };
            String[] registerParams = { "username", "password", "email", "address", "phone", "full name", "credit card type", "credit card number", "expiration date" };
            for (int i = 0; i < registerInfo.Length; i++)
            {
                if (String.IsNullOrEmpty(registerInfo[i]))
                {
                    textProgressLabel.Text = "Registration Error: Please enter your " + registerParams[i];
                    debugLog("Registration Error: " + registerParams[i] + " not entered", false);
                    MessageBox.Show("Please enter your " + registerParams[i], "Registration Error", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                    enteredInfo = false;
                    break;
                }
            }
            if (enteredInfo)
            {
                int uid = fDB.NewGuestRegistration(username, email, password, address, phone, fullname);
                if (uid == -1)
                {
                    debugLog("[Registration] Something went wrong.", true);
                }
                else
                {
                    fDB.NewCreditCardRegistration(uid, creditcard, cc_type, expdate);
                    debugLog("Registration Successful. You may now login.", true);
                    switchHomePanelUImode(HomeUIMode.login);
                }
            }
        }

        private void btnRegisterLogin_Click(object sender, EventArgs e)
        {
            switchHomePanelUImode(HomeUIMode.login);
        }

        private void forceReconnectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textProgressLabel.Text = "Ready to connect to database...";
            timerDBConnector.Start();
        }

        private void btnLoginLogin_Click(object sender, EventArgs e)
        {
            if (!db_connected)
            {
                debugLog("Could not login - Not connected to database", true);
            }
            else
            {
                debugLog("Attempting to login...", true);
                if (fDB.VerifyLogin(textLoginUsername.Text, textLoginPassword.Text))
                {
                    // success

                    userInfo.username = textLoginUsername.Text;
                    debugLog("Welcome back, " + userInfo.username, true);
                    labelLoggedIn.Text = "You are logged in as: " + userInfo.username;
                    switchHomePanelUImode(HomeUIMode.loggedin);

                    // @@ add manager/employee/user switch here
                    userInfo = fDB.GetUserInfo(userInfo.username);

                    switchUImode(userInfo.accessLevel);
                    updateMyAccountTab();
                }
                else
                {
                    // failed
                    debugLog("Failed to login (invalid username/password)", true);
                }
            }
        }

        private void updatePassword_Click(object sender, EventArgs e)
        {
            if (fDB.verifyPassword(userInfo.username, textOldPassword.Text))
            {
                fDB.updatePassword(userInfo.username, textNewPassword.Text);
                debugLog("Password successfully changed", true);
                textOldPassword.Text = "";
                textNewPassword.Text = "";
            }
            else
            {
                MessageBox.Show("Enter correct old password");
            }
        }

        private void buttonLogOut_Click(object sender, EventArgs e)
        {
            switchUImode(AccessLevel.guest);
            switchHomePanelUImode(HomeUIMode.prompt);
        }

        private void buttonUpdateInfo_Click(object sender, EventArgs e)
        {
            textBoxChangeName.Text = labelName.Text;
            textBoxChangePhoneNum.Text = labelPhone.Text;
            textBoxChangeAddress.Text = labelAddress.Text;

            textBoxChangeName.Show();
            textBoxChangePhoneNum.Show();
            textBoxChangeAddress.Show();

            buttonUpdateInfo.Hide();
            buttonConfirmUpdateInfo.Show();
        }

        private void buttonConfirmUpdateInfo_Click(object sender, EventArgs e)
        {
            textBoxChangeName.Hide();
            textBoxChangePhoneNum.Hide();
            textBoxChangeAddress.Hide();

            fDB.updateUserInfo(userInfo.user_id, textBoxChangeName.Text, textBoxChangePhoneNum.Text, textBoxChangeAddress.Text);
            userInfo = fDB.GetUserInfo(userInfo.username);
            updateMyAccountTab();
        }

        private void tabMainControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabMainControl.SelectedTab == tabAllTables)
            {
                if (fDB != null)
                    updateAllTablesTab();
            }
            else if (tabMainControl.SelectedTab == tabReviews)
            {
                // update accordingly
                tabReviewControl.SelectedTab = panelReviewTheatres;
                listReviewData.Items.Clear();
                textReviewReplyBox.Text = "";
            }
            else if (tabMainControl.SelectedTab == tabEmployeeManagement)
            {
                if (fDB != null)
                    updateEmployeeManagement();
            }
            else if (tabMainControl.SelectedTab == tabTheatreManagement)
            {
                if (fDB != null)
                    updateTheatreManagement();
            }
        }

        private void updateAllTablesTab()
        {
            foreach (Control i in containerAllTables.Controls)
            {
                if (i is DataGridView)
                    ((DataGridView)i).CellValueChanged -= cellChanged;
            }
            containerAllTables.Controls.Clear();
            foreach (string tableName in fDB.getAllTableNames())
            {
                Label name = new Label();
                name.Text = tableName;
                containerAllTables.Controls.Add(name);

                DataGridView grid = new DataGridView();
                grid.Name = tableName;
                grid.Width = containerAllTables.Size.Width;

                List<string> columns = fDB.getTableColumnNames(tableName);
                grid.ColumnCount = columns.Count;
                for (int i = 0; i < columns.Count; i++)
                {
                    grid.Columns[i].Name = columns[i];
                }
                List<string[]> values = fDB.getAllRowsFromTable(tableName, columns);
                foreach (string[] i in values)
                {
                    grid.Rows.Add(i);
                }
                grid.CellValueChanged += cellChanged;
                containerAllTables.Controls.Add(grid);
            }
        }

        private void cellChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView grid = (DataGridView)sender;
            string columnName = grid.Columns[e.ColumnIndex].Name;
            fDB.updateValue(grid.Name, columnName, grid.Rows[e.RowIndex].Cells[columnName].Value.ToString(), grid.Columns[0].Name, grid.Rows[e.RowIndex].Cells[0].Value.ToString());
        }

        private void btnReviewsRefreshTheatres_Click(object sender, EventArgs e)
        {
            listReviewTheatres.Items.Clear();
            List<string> theatres = fDB.GetAllTheatreThreads();
            foreach (string i in theatres)
            {
                listReviewTheatres.Items.Add(i);
            }
        }

        private void btnReviewsRefreshMovies_Click(object sender, EventArgs e)
        {
            listReviewMovies.Items.Clear();
            List<string> theatres = fDB.GetAllMovieThreads();
            foreach (string i in theatres)
            {
                listReviewMovies.Items.Add(i);
            }
        }

        private void listReviewTheatres_SelectedIndexChanged(object sender, EventArgs e)
        {
            // load thread
            int thread_id = listReviewTheatres.SelectedIndex;
            fDB.incrementTheatreHits(thread_id);
            List<ThreadPost> thread = fDB.GetThread(thread_id, ThreadType.theatre);
            listReviewData.Items.Clear();

            foreach (ThreadPost i in thread)
            {
                listReviewData.Items.Add(fDB.GetUsernameFromId(i.user_id) + ": " + i.content);
            }

            // update label
            labelReviewDiscussion.Text = fDB.GetTheatreNameFromThreadId(thread_id);
            if (userInfo.accessLevel == AccessLevel.guest)
            {
                labelReviewAlreadyVoted.Visible = false;
                btnReviewUpvote.Visible = false;
                btnReviewDownvote.Visible = false;
            }
            else if (fDB.hasUserVotedOnThread(userInfo, thread_id, ThreadType.theatre))
            {
                labelReviewAlreadyVoted.Visible = true;
                btnReviewUpvote.Visible = false;
                btnReviewDownvote.Visible = false;
            }
            else
            {
                labelReviewAlreadyVoted.Visible = false;
                btnReviewUpvote.Visible = true;
                btnReviewDownvote.Visible = true;
            }
            labelReviewRating.Text = fDB.getThreadVoteRating(thread_id, ThreadType.theatre).ToString();
            labelReviewOwner.Text = fDB.GetUsernameFromId(fDB.getThreadOwnerID(thread_id, ThreadType.theatre));
        }

        private void listReviewMovies_SelectedIndexChanged(object sender, EventArgs e)
        {
            // load thread
            int thread_id = listReviewMovies.SelectedIndex;
            fDB.incrementMovieHits(thread_id);
            List<ThreadPost> thread = fDB.GetThread(thread_id, ThreadType.movie);
            listReviewData.Items.Clear();

            foreach (ThreadPost i in thread)
            {
                listReviewData.Items.Add(fDB.GetUsernameFromId(i.user_id) + ": " + i.content);
            }

            // update label
            labelReviewDiscussion.Text = fDB.GetMovieNameFromThreadId(thread_id);
            if (userInfo.accessLevel == AccessLevel.guest)
            {
                labelReviewAlreadyVoted.Visible = false;
                btnReviewUpvote.Visible = false;
                btnReviewDownvote.Visible = false;
            }
            else if (fDB.hasUserVotedOnThread(userInfo, thread_id, ThreadType.movie))
            {
                labelReviewAlreadyVoted.Visible = true;
                btnReviewUpvote.Visible = false;
                btnReviewDownvote.Visible = false;
            }
            else
            {
                labelReviewAlreadyVoted.Visible = false;
                btnReviewUpvote.Visible = true;
                btnReviewDownvote.Visible = true;
            }
            labelReviewRating.Text = fDB.getThreadVoteRating(thread_id, ThreadType.movie).ToString();
            labelReviewOwner.Text = fDB.GetUsernameFromId(fDB.getThreadOwnerID(thread_id, ThreadType.movie));
        }

        private void btnThreadReply_Click(object sender, EventArgs e)
        {
            // oh boy.
            if (tabReviewControl.SelectedTab == panelReviewMovies)
            {
                int thread_id = listReviewMovies.SelectedIndex;
                fDB.ReviewAddReply(userInfo, thread_id, textReviewReplyBox.Text, ThreadType.movie);

                // re-load thread
                List<ThreadPost> thread = fDB.GetThread(thread_id, ThreadType.movie);
                listReviewData.Items.Clear();

                foreach (ThreadPost i in thread)
                {
                    listReviewData.Items.Add(fDB.GetUsernameFromId(i.user_id) + ": " + i.content);
                }
            }
            else if (tabReviewControl.SelectedTab == panelReviewTheatres)
            {
                int thread_id = listReviewTheatres.SelectedIndex;
                fDB.ReviewAddReply(userInfo, thread_id, textReviewReplyBox.Text, ThreadType.theatre);

                // re-load thread
                List<ThreadPost> thread = fDB.GetThread(thread_id, ThreadType.theatre);
                listReviewData.Items.Clear();

                foreach (ThreadPost i in thread)
                {
                    listReviewData.Items.Add(fDB.GetUsernameFromId(i.user_id) + ": " + i.content);
                }
            }

            // clear and reset
            textReviewReplyBox.Text = "";


            debugLog("Reply successful! You earned some points!", true);
        }

        private void btnAccountRefreshPoints_Click(object sender, EventArgs e)
        {
            //userInfo = fDB.GetUserInfo(userInfo.username);
            //labelPoints.Text = userInfo.credits.ToString();
            updateMyAccountTab();
        }

        private void radioReviewTheatre_CheckedChanged(object sender, EventArgs e)
        {
            List<string> theatres = fDB.GetAllTheatres();
            comboReviewTarget.Items.Clear();
            foreach (string theatre in theatres)
            {
                comboReviewTarget.Items.Add(theatre);
            }
        }

        private void radioReviewMovie_CheckedChanged(object sender, EventArgs e)
        {
            List<string> theatres = fDB.GetAllMovies();
            comboReviewTarget.Items.Clear();
            foreach (string theatre in theatres)
            {
                comboReviewTarget.Items.Add(theatre);
            }
        }

        private void btnReviewCreateThread_Click(object sender, EventArgs e)
        {
            bool valid = true;
            if (textReviewAddTitle.Text == "") valid = false;
            if (textReviewAddMessage.Text == "") valid = false;
            if (comboReviewTarget.Text == "") valid = false;
            if (!valid)
            {
                MessageBox.Show("Incomplete form! Please check your input.");
            }
            else
            {
                if (radioReviewMovie.Checked)
                {
                    int thread_id = fDB.GetLastThreadID(ThreadType.movie);
                    fDB.ReviewAddThread(userInfo, comboReviewTarget.SelectedIndex, thread_id, textReviewAddTitle.Text, ThreadType.movie);
                    fDB.ReviewAddReply(userInfo, thread_id, textReviewAddMessage.Text, ThreadType.movie);
                }
                else if (radioReviewTheatre.Checked)
                {
                    int thread_id = fDB.GetLastThreadID(ThreadType.theatre);
                    fDB.ReviewAddThread(userInfo, comboReviewTarget.SelectedIndex, thread_id, textReviewAddTitle.Text, ThreadType.theatre);
                    fDB.ReviewAddReply(userInfo, thread_id, textReviewAddMessage.Text, ThreadType.theatre);
                }

                debugLog("Reply successful! You earned some points!", true);
            }
        }

        private void buttonQuery1_Click(object sender, EventArgs e)
        {
            List<ThreadPost> posts = new List<ThreadPost>();
            if (comboBoxQuery1.SelectedItem != null)
            {
                posts = fDB.Query1(comboBoxQuery1.SelectedItem.ToString());
                listBoxQuery1.Items.Clear();
                foreach (ThreadPost tp in posts)
                {
                    listBoxQuery1.Items.Add(fDB.GetUsernameFromId(tp.user_id) + ": " + tp.content);
                }
            }
            else
            {
                MessageBox.Show("Please select a thread", "Error", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
        }

        private void buttonQuery2_Click(object sender, EventArgs e)
        {
            List<ThreadPost> posts = new List<ThreadPost>();
            posts = fDB.Query2();
            listBoxQuery2.Items.Clear();
            foreach (ThreadPost tp in posts)
            {
                listBoxQuery2.Items.Add(fDB.GetThreadTypeByThreadId(tp.thread_id) + " - " + fDB.GetUsernameFromId(tp.user_id) + ": " + tp.content);
            }
        }

        private void buttonQuery3_Click(object sender, EventArgs e)
        {
            listBoxQuery3.Items.Clear();
            List<Thread> threads = fDB.GetAllTheatreThreadsHits();
            List<Thread> movieThreads = fDB.GetAllMovieThreadsHits();
            threads.AddRange(movieThreads);
            if (threads.Count == 0)
            {
                MessageBox.Show("No Threads At All", "Error", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
            else
            {
                int minHitsComs = threads[0].hits + threads[0].comments;
                int minThread = threads[0].thread_id;
                String minName = threads[0].threadname;
                //debugLog("Listing All Threads", false);
                foreach (Thread t in threads)
                {
                    //debugLog(t.threadname + " - hits:" + t.hits + " - comments:" + t.comments, false);
                    if ((t.hits + t.comments) < minHitsComs)
                    {
                        minHitsComs = t.hits + t.comments;
                        minThread = t.thread_id;
                        minName = t.threadname;
                    }
                }
                listBoxQuery3.Items.Add(fDB.GetThreadTypeByThreadId(minThread) + " - " + minName);
            }
        }

        private void buttonQuery5_Click(object sender, EventArgs e)
        {
            listBoxQuery5.Items.Clear();
            UserInfo user = fDB.GetUserIdMostComments();
            String username = fDB.GetUsernameFromId(user.user_id);
            listBoxQuery5.Items.Add(username + " has " + user.comments + " comments! WOW!");
        }

        private void btnTicketStart_Click(object sender, EventArgs e)
        {
            panelTicketFogOfWar.Visible = false;

            listTicketTheatre.Items.Clear();
            listTicketMovie.Items.Clear();
            listTicketTime.Items.Clear();
            listTicketMovieIDHidden.Items.Clear();
            listTicketTimeIDHidden.Items.Clear();

            listTicketTheatre.Enabled = true;
            listTicketMovie.Enabled = true;
            listTicketTime.Enabled = true;

            groupTicketTheatre.Visible = true;
            groupTicketMovie.Visible = false;
            groupTicketTime.Visible = false;

            List<string> theatres = fDB.GetAllTheatres();
            foreach (string theatre in theatres)
            {
                listTicketTheatre.Items.Add(theatre);
            }
        }

        private void btnTicketReset_Click(object sender, EventArgs e)
        {
            panelTicketFogOfWar.Visible = true;
            groupTicketTheatre.Visible = false;
            groupTicketMovie.Visible = false;
            groupTicketTime.Visible = false;
            panelTicketBillingInfo.Visible = false;
        }

        private void listTicketTheatre_SelectedIndexChanged(object sender, EventArgs e)
        {
            // got theatre. lock it in.
            listTicketTheatre.Enabled = false;
            groupTicketMovie.Visible = true;
            int theatre_id = listTicketTheatre.SelectedIndex;

            List<MovieRoomPlaying> movies = fDB.GetAllMoviesInTheatre(theatre_id);
            listTicketMovie.Items.Clear();
            foreach (MovieRoomPlaying movie in movies)
            {
                Console.WriteLine(movie.time);
                Console.WriteLine(movie.room_id);
                if (listTicketMovie.Items.IndexOf(movie.name) > -1 && listTicketMovieIDHidden.Items.IndexOf(movie.room_id) > -1)
                {
                    continue;
                }
                listTicketMovie.Items.Add(movie.name);
                listTicketMovieIDHidden.Items.Add(movie.room_id);
            }
        }

        private void listTicketMovie_SelectedIndexChanged(object sender, EventArgs e)
        {
            // got movie.
            listTicketTime.Items.Clear();
            listTicketTimeIDHidden.Items.Clear();

            groupTicketTime.Visible = true;
            int room_id = Convert.ToInt32(listTicketMovieIDHidden.Items[listTicketMovie.SelectedIndex]); // a bit hacky

            int theatre_id = listTicketTheatre.SelectedIndex;
            List<MovieRoomPlaying> movies = fDB.GetAllMoviesInTheatre(theatre_id);
            foreach (MovieRoomPlaying movie in movies)
            {
                if (movie.room_id == room_id && movie.name == listTicketMovie.SelectedItem.ToString())
                {
                    // too lazy to make a new fDB
                    //List<string> times = movie.time.Split(',').ToList<string>();

                    //foreach (string time in times)
                    //{
                    //    listTicketTime.Items.Add("Showing at: " + time);
                    //}
                    listTicketTime.Items.Add(movie.time);
                    listTicketTimeIDHidden.Items.Add(movie.room_id);
                }
            }
        }

        private void listTicketTime_SelectedIndexChanged(object sender, EventArgs e)
        {
            panelTicketBillingInfo.Visible = true;
            // preload stuff if registered else cry
            if (userInfo.accessLevel == AccessLevel.guest)
            {
                // force stuff
                labelTicketMustRegister.Visible = true;
                btnTicketRegisterAndBuy.Visible = true;
                btnTicketBuy.Visible = false;
            }
            else
            {
                btnTicketRegisterAndBuy.Visible = false;
                btnTicketBuy.Visible = true;

                labelTicketMustRegister.Visible = false;

                textTicketUsername.Text = userInfo.username;
                textTicketUsername.Enabled = false;
                textTicketEmail.Text = userInfo.username;
                textTicketEmail.Enabled = false;
                textTicketFullName.Text = userInfo.name;
                textTicketFullName.Enabled = false;
                textTicketPhone.Text = userInfo.phoneNum;
                textTicketPhone.Enabled = false;
                textTicketAddress.Text = userInfo.address;
                textTicketAddress.Enabled = false;

                textTicketCC.Text = userInfo.creditCard.cc.ToString();
                textTicketCCExp.Text = userInfo.creditCard.cc_exp.ToString();
                textTicketCCType.Text = userInfo.creditCard.cc_type.ToString();
            }

            // update seating
            labelTicketSeats.Text = fDB.TicketCheckSeating(listTicketTheatre.SelectedIndex, Convert.ToInt32(listTicketTimeIDHidden.SelectedItem), listTicketTime.SelectedItem.ToString()).ToString();

        }

        private void btnTicketBuy_Click(object sender, EventArgs e)
        {
            int theatre_id = listTicketTheatre.SelectedIndex;
            //int movie_id = Convert.ToInt32(listTicketMovieIDHidden.Items[listTicketMovie.SelectedIndex]);
            int movie_id = fDB.GetMovieIdFromName(listTicketMovie.SelectedItem.ToString());
            int room_id = Convert.ToInt32(listTicketTimeIDHidden.SelectedItem);
            string time = listTicketTime.SelectedItem.ToString();

            fDB.TicketPurchase(theatre_id, movie_id, userInfo.user_id, room_id, time);
            debugLog("You have purchased a ticket.", true);
            MessageBox.Show("You have purchased a ticket.");
        }

        private void btnTicketRegisterAndBuy_Click(object sender, EventArgs e)
        {
            bool enteredInfo = true;
            String username = textTicketUsername.Text;
            String password = textTicketPassword.Text;
            String email = textTicketEmail.Text;
            String address = textTicketAddress.Text;
            String phone = textTicketPhone.Text;
            String fullname = textTicketFullName.Text;
            String cc_type = textTicketCCType.Text;
            String creditcard = textTicketCC.Text;
            String expdate = textTicketCCExp.Text;
            String[] registerInfo = { username, password, email, address, phone, fullname, cc_type, creditcard, expdate };
            String[] registerParams = { "username", "password", "email", "address", "phone", "full name", "credit card type", "credit card number", "expiration date" };
            for (int i = 0; i < registerInfo.Length; i++)
            {
                if (String.IsNullOrEmpty(registerInfo[i]))
                {
                    textProgressLabel.Text = "Registration Error: Please enter your " + registerParams[i];
                    debugLog("Registration Error: " + registerParams[i] + " not entered", false);
                    MessageBox.Show("Please enter your " + registerParams[i], "Registration Error", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                    enteredInfo = false;
                    break;
                }
            }
            if (enteredInfo)
            {
                int uid = fDB.NewGuestRegistration(username, email, password, address, phone, fullname);
                if (uid == -1)
                {
                    debugLog("[Registration] Something went wrong.", true);
                }
                else
                {
                    fDB.NewCreditCardRegistration(uid, creditcard, cc_type, expdate);
                    // yeah assume it works

                    // @@ add manager/employee/user switch here
                    userInfo = fDB.GetUserInfo(userInfo.username);

                    int theatre_id = listTicketTheatre.SelectedIndex;
                    int movie_id = fDB.GetMovieIdFromName(listTicketMovie.SelectedItem.ToString());
                    int room_id = Convert.ToInt32(listTicketTimeIDHidden.SelectedItem);
                    string time = listTicketTime.SelectedItem.ToString();

                    fDB.TicketPurchase(theatre_id, movie_id, userInfo.user_id, room_id, time);
                    debugLog("You have registered and purchased a ticket.", true);
                    MessageBox.Show("You have registered and purchased a ticket.");

                    // autologin
                    switchHomePanelUImode(HomeUIMode.loggedin);

                    switchUImode(userInfo.accessLevel);
                    updateMyAccountTab();
                }
            }
        }

        private void btnMyTicketsRefresh_Click(object sender, EventArgs e)
        {
            List<Ticket> tickets = fDB.GetMyTickets(userInfo.user_id);
            listMyTickets.Items.Clear();

            foreach (Ticket t in tickets)
            {
                listMyTickets.Items.Add("Ticket #" + t.ticket_id + " - Theatre: " + fDB.GetTheatreNameFromId(t.theatre_id) + " - Movie: " +
                    fDB.GetMovieNameFromId(t.movie_id) + " - Time: " + t.time);
            }
        }

        private void updateEmployeeManagement()
        {
            List<string> employees = fDB.getAllEmployeeNames();
            comboEmployeeSelect.Items.Clear();
            foreach (string i in employees)
            {
                comboEmployeeSelect.Items.Add(i);
            }

            List<string> theatres = fDB.GetAllTheatres();
            comboTheatreSelect.Items.Clear();
            foreach (string i in theatres)
            {
                comboTheatreSelect.Items.Add(i);
            }

        }

        private void comboEmployeeSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedEmployee = fDB.GetUserInfoFromName(comboEmployeeSelect.Text);

            labelEmployeeName.Text = selectedEmployee.name;
            labelEmployeeAddress.Text = selectedEmployee.address;
            labelEmployeePhone.Text = selectedEmployee.phoneNum;
            labelEmployeeSSN.Text = selectedEmployee.employeeInfo.ssn.ToString();

            List<string> jobs = fDB.GetEmployeeJobs(selectedEmployee.user_id);
            comboJobSelect.Items.Clear();
            foreach (string i in jobs)
            {
                comboJobSelect.Items.Add(i);
            }

            
        }

        private void buttonQuery6_Click(object sender, EventArgs e) { 
            listBoxQuery6.Items.Clear();
            List<TheatreInfo> theatres = fDB.Query6();
            foreach(TheatreInfo t in theatres)
            {
                listBoxQuery6.Items.Add(t.name +" is showing " + t.movies + " movies!");
            }
        }

        private void buttonQuery7_Click(object sender, EventArgs e)
        {
            listBoxQuery7.Items.Clear();
            List<TheatreInfo> theatres = fDB.Query7();
            foreach (TheatreInfo t in theatres)
            {
                listBoxQuery7.Items.Add(t.name + " - " + t.ticketsales + " tickets sold!");
            }
        }
                    
        
        private void employeeDayTheatre_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboDaySelect.Text == "" || selectedEmployee == null) { 
            MessageBox.Show("asdas");
            return; }
            ScheduleInfo schedule = fDB.GetEmployeeSchedule(selectedEmployee.user_id);
            string dayIdentifier = schedule.times.Keys.ElementAt(comboDaySelect.SelectedIndex);
            if (schedule.times[dayIdentifier] != null)
            {
                labelScheduleTime.Text = schedule.times[dayIdentifier];
                labelScheduleJob.Text = schedule.jobs[dayIdentifier];
                labelScheduleTheatre.Text = schedule.theatres[dayIdentifier];
                panelCurrentSchedule.Show();

            }
            else
            {
                panelCurrentSchedule.Hide();
            }
        }

        private void buttonSubmitSchedule_Click(object sender, EventArgs e)
        {
            if (comboSetStartHour.Text == "" || comboSetStartMinute.Text == "" || comboSetStartCycle.Text == "" || comboSetEndHour.Text == "" || comboSetEndMinute.Text == "" || comboSetEndCycle.Text == "" || comboTheatreSelect.Text == "" || comboJobSelect.Text == "")
            {
                MessageBox.Show("Fill in all sections");
                return;
            }
            ScheduleInfo schedule = fDB.GetEmployeeSchedule(selectedEmployee.user_id);
            string dayIdentifier = schedule.times.Keys.ElementAt(comboDaySelect.SelectedIndex);
            if (schedule.times[dayIdentifier] == null)
            {
                fDB.InsertNewSchedule(selectedEmployee.employeeInfo.employee_id, comboTheatreSelect.SelectedIndex, comboJobSelect.Text, dayIdentifier, comboSetStartHour.Text + ":" + comboSetStartMinute.Text + comboSetStartCycle.Text[0] + " to " + comboSetEndHour.Text + ":" + comboSetEndMinute.Text + comboSetEndCycle.Text[0]);
            }
            else
            {
                fDB.UpdateSchedule(selectedEmployee.employeeInfo.employee_id, comboTheatreSelect.SelectedIndex, comboJobSelect.Text, dayIdentifier, comboSetStartHour.Text + ":" + comboSetStartMinute.Text + comboSetStartCycle.Text[0] + " to " + comboSetEndHour.Text + ":" + comboSetEndMinute.Text + comboSetEndCycle.Text[0]);
            }

            employeeDayTheatre_SelectedIndexChanged(null, null);
        }

        private void buttonQuery8_Click(object sender, EventArgs e)
        {
            if(comboBoxQuery8.SelectedItem == null)
            {
                MessageBox.Show("Please select a theatre", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                listBoxQuery8.Items.Clear();
                List<Schedule> schedules = fDB.Query8(comboBoxQuery8.SelectedIndex);
                listBoxQuery8.Items.Add("Name\t\tJob\tTime");
                foreach (Schedule t in schedules)
                {
                    listBoxQuery8.Items.Add(t.name + "\t" + t.job + "\t" + t.time);
                }
            }           
        }

        private void buttonQueriesTab(object sender, EventArgs e)
        {
            List<string> theatres = fDB.GetAllTheatres();
            comboBoxQuery8.Items.Clear();
            foreach (string i in theatres)
            {
                comboBoxQuery8.Items.Add(i);
            }
        }
        
        private void btnReviewUpvote_Click(object sender, EventArgs e)
        {
            int thread_id = 0;
            ThreadType t_type = new ThreadType();
            if (tabReviewControl.SelectedTab == panelReviewMovies)
            {
                thread_id = listReviewMovies.SelectedIndex;
                t_type = ThreadType.movie;
            }
            else if (tabReviewControl.SelectedTab == panelReviewTheatres)
            {
                thread_id = listReviewTheatres.SelectedIndex;
                t_type = ThreadType.theatre;
            }
            fDB.UserVoteThread(userInfo, thread_id, t_type, true);
            labelReviewRating.Text = fDB.getThreadVoteRating(thread_id, t_type).ToString();
            labelReviewAlreadyVoted.Visible = true;
            btnReviewUpvote.Visible = false;
            btnReviewDownvote.Visible = false;

            UserInfo ui = fDB.GetUserInfo(labelReviewOwner.Text);
            debugLog("original owner of thread got points: " + labelReviewOwner.Text);
            fDB.ModUserPointsById(ui, 10);
        }

        private void btnReviewDownvote_Click(object sender, EventArgs e)
        {
            int thread_id = 0;
            ThreadType t_type = new ThreadType();
            if (tabReviewControl.SelectedTab == panelReviewMovies)
            {
                thread_id = listReviewMovies.SelectedIndex;
                t_type = ThreadType.movie;
            }
            else if (tabReviewControl.SelectedTab == panelReviewTheatres)
            {
                thread_id = listReviewTheatres.SelectedIndex;
                t_type = ThreadType.theatre;
            }
            fDB.UserVoteThread(userInfo, thread_id, t_type, false);
            labelReviewRating.Text = fDB.getThreadVoteRating(thread_id, t_type).ToString();
            labelReviewAlreadyVoted.Visible = true;
            btnReviewUpvote.Visible = false;
            btnReviewDownvote.Visible = false;

            UserInfo ui = fDB.GetUserInfo(labelReviewOwner.Text);
            debugLog("original owner of thread got points: " + labelReviewOwner.Text);
            fDB.ModUserPointsById(ui, 10);
        }

        public void updateTheatreManagement()
        {
            TheatreInfo theatre;
            if (userInfo.isEmployee && fDB.getManagerTheatre(userInfo.employeeInfo.employee_id) == null)
            {
                comboSelectTheatreForUpdate.Enabled = true;
                List<string> theatres = fDB.GetAllTheatres();
                comboTheatreSelect.Items.Clear();
                foreach (string i in theatres)
                {
                    comboTheatreSelect.Items.Add(i);
                }
            }
            else
            {
                theatre = fDB.getManagerTheatre(userInfo.employeeInfo.employee_id);
                comboSelectTheatreForUpdate.Enabled = false;
                comboSelectTheatreForUpdate.Text = theatre.name;

                labelTheatreName.Text = theatre.name;
                labelTheatreAddress.Text = theatre.address;
                comboSelectTheatreForUpdate_SelectedIndexChanged(null, null);
            }

        }

        private void comboSelectTheatreForUpdate_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> movies = fDB.GetAllMovies();
            comboSelectMovie.Items.Clear();
            foreach (string i in movies)
            {
                comboSelectMovie.Items.Add(i);
            }

        }

        private void comboSelectMovie_SelectedIndexChanged(object sender, EventArgs e)
        {
            TheatreInfo theatre;
            if (userInfo.isEmployee && fDB.getManagerTheatre(userInfo.employeeInfo.employee_id) == null)
                theatre = fDB.getTheatre(comboSelectTheatreForUpdate.SelectedIndex);
            else
                theatre = fDB.getManagerTheatre(userInfo.employeeInfo.employee_id);
            dataShowTimes.Rows.Clear();
            dataShowTimes.ColumnCount = 2;
            dataShowTimes.Columns[0].Name = "Room Number";
            dataShowTimes.Columns[1].Name = "Time";

            List<MovieRoomPlaying> shows = fDB.GetAllMoviesInTheatre(theatre.theatre_id);
            foreach (MovieRoomPlaying i in shows)
            {
                dataShowTimes.Rows.Add(new string[] { i.room_id.ToString(), i.time });
            }
        }

        private void dataShowTimes_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            fDB.UpdateShowTime(comboSelectMovie.Text, Convert.ToInt32(dataShowTimes.Rows[e.RowIndex].Cells[0].Value), dataShowTimes.Rows[e.RowIndex].Cells[1].Value.ToString());
        }

        private void buttonAddShowTime_Click(object sender, EventArgs e)
        {
            if (comboMovieStartHour.Text == "" || comboMovieStartMinute.Text == "" || comboMovieStartCycle.Text == "" || comboMovieEndHour.Text == "" || comboMovieEndMinute.Text == "" || comboMovieEndCycle.Text == "" || comboSelectRoom.Text == "" || comboSelectMovie.Text == "")
            {
                MessageBox.Show("Fill in all sections");
                return;
            }
            fDB.InsertShowTime(comboSelectMovie.Text, Convert.ToInt32(comboSelectRoom.Text), comboMovieStartHour.Text + ":" + comboMovieStartMinute.Text + comboMovieStartCycle.Text[0] + " to " + comboMovieEndHour.Text + ":" + comboMovieEndMinute.Text + comboMovieEndCycle.Text[0]);
            comboSelectMovie_SelectedIndexChanged(null, null);
        }
    }
}

public class UserInfo
{
    public int user_id;
    public string username;
    public string name;
    public string email;
    public int credits;
    public string rank;
    public string address;
    public string phoneNum;
    public AccessLevel accessLevel;
    public bool isEmployee;
    public EmployeeInfo employeeInfo;
    public int comments;

    public CreditCard creditCard;
}

public class CreditCard
{
    public string cc;
    public string cc_type;
    public string cc_exp;
}

public class Ticket
{
    public int ticket_id;
    public int movie_id;
    public int theatre_id;
    public int room_id;
    public string time;
}

public class EmployeeInfo
{
    public int employee_id;
    public int ssn;
}

public class ScheduleInfo
{
    public ScheduleInfo()
    {
        times = new Dictionary<string, string>();
        jobs = new Dictionary<string, string>();
        theatres = new Dictionary<string, string>();
        times.Add("m", null);
        times.Add("t", null);
        times.Add("w", null);
        times.Add("r", null);
        times.Add("f", null);
        times.Add("s", null);
        jobs.Add("m", null);
        jobs.Add("t", null);
        jobs.Add("w", null);
        jobs.Add("r", null);
        jobs.Add("f", null);
        jobs.Add("s", null);
        jobs.Add("u", null);
        theatres.Add("m", null);
        theatres.Add("t", null);
        theatres.Add("w", null);
        theatres.Add("r", null);
        theatres.Add("f", null);
        theatres.Add("s", null);
        theatres.Add("u", null);
    }
    public Dictionary<string, string> times;
    public Dictionary<string, string> jobs;
    public Dictionary<string, string> theatres;

}

public class Schedule
{
    public String name;
    public String job;
    public String time;
}

public class TheatreInfo
{
    public int theatre_id;
    public string name;
    public string address;
    public int ticketsales;
    public int movies;
}

public class ThreadPost
{
    public int post_id { get; set; }
    public string content { get; set; }
    public int rating;
    public string date;
    public int user_id;
    public int thread_id { get; set; }
}

public class Thread
{
    public int thread_id { get; set; }
    public String threadname { get; set; }
    public int hits { get; set; }
    public int comments { get; set; }
}
public class MovieRoomPlaying
{
    public int room_id;
    public int movie_id { get; set; }
    public string name { get; set; }
    public string time;
}

public enum AccessLevel
{
    guest = 0,
    normal,
    employee,
    manager,
    owner,
    manager_withaccess
}

enum HomeUIMode
{
    prompt,
    login,
    loggedin,
    register
}

enum ThreadType
{
    theatre,
    movie
}
